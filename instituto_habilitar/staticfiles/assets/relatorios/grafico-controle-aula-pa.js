var graficos_pa = function() {

    setTimeout(function() {

        var regressao_inicial_sistolica = regressaoArray(medidas_inicial_sistolica);
        var regressao_final_sistolica = regressaoArray(medidas_final_sistolica);
        var regressao_inicial_diastolica = regressaoArray(medidas_inicial_diastolica);
        var regressao_final_diastolica = regressaoArray(medidas_final_diastolica);


        new Chart(document.getElementById("chart-pa-inicial-sistolica"), {
            "type": 'line',
            "data": {
                "labels": datas_aulas,
                "datasets": [{
                        "label": "Sistólica",
                        "data": medidas_inicial_sistolica,
                        "fill": false,
                        "borderColor": "rgb(0, 0, 0)",
                        "showLine": false
                    },
                    {
                        "label": "Linha Tendência",
                        "data": regressao_inicial_sistolica,
                        "borderColor": "rgb(0, 0, 0)",
                        "fill": false,
                        "type": "line"
                    }
                ],

            },
            "options": {}

        });

        new Chart(document.getElementById("chart-pa-inicial-diastolica"), {
            "type": 'line',
            "data": {
                "labels": datas_aulas,
                "datasets": [
                    {
                        "label": 'Diastólica',
                        "data": medidas_inicial_diastolica,
                        "fill": false,
                        "borderColor": "rgb(0, 0, 0)",
                        "type": 'line',
                        "showLine": false
                    },
                    {
                        "label": "Linha Tendência",
                        "data": regressao_inicial_diastolica,
                        "borderColor": "rgb(0, 0, 0)",
                        "fill": false,
                        "type": "line"
                    }
                ],

            },
            "options": {}

        });


        new Chart(document.getElementById("chart-pa-final-sistolica"), {
            "type": "line",
            "data": {
                "labels": datas_aulas,
                "datasets": [{
                        "label": "Sistólica",
                        "data": medidas_final_sistolica,
                        "fill": false,
                        "borderColor": "rgb(0, 0, 0)",
                        "showLine": false
                    },
                    {
                        "label": "Linha Tendência",
                        "data": regressao_final_sistolica,
                        "borderColor": "rgb(0, 0, 0)",
                        "fill": false,
                        "type": "line"
                    }
                ]
            },
            "options": {}
        });


        new Chart(document.getElementById("chart-pa-final-diastolica"), {
            "type": "line",
            "data": {
                "labels": datas_aulas,
                "datasets": [
                    {
                        "label": 'Diastólica',
                        "data": medidas_final_diastolica,
                        "fill": false,
                        "borderColor": "rgb(0, 0, 0)",
                        "type": 'line',
                        "showLine": false
                    },
                    {
                        "label": "Linha Tendência",
                        "data": regressao_final_diastolica,
                        "borderColor": "rgb(0, 0, 0)",
                        "fill": false,
                        "type": "line"
                    }
                ]
            },
            "options": {}
        });


    }, 100)
}
