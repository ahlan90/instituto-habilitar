
/* DATA TABLES DE avaliacao-postural */
$(document).ready(function() {
       
       var table = $('#avaliacao-postural-table').DataTable( {
           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
          "autoWidth":false,
          "columns": [
                  { 
                      "width" : "5%"
                  },
                  { 
                      "width" : "10%",
                      "order" : false,
                      
                  },
                  null,
                  {
                      "width" : "5%",
                      "order" : false,

                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  {
                      "width" : "5%",
                      "order" : false,
                  }
          ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $(".preloader").css('display','block');
        $("#modal-avaliacao-postural .modal-content").html("");
        $("#modal-avaliacao-postural").modal("show");
      },
      success: function (data) {
        $(".preloader").css('display','none');
        $("#modal-avaliacao-postural .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#avaliacao-postural-table tbody").html(data.html_avaliacao_postural_list);
          $("#modal-avaliacao-postural").modal("hide");
        }
        else {
          $("#modal-avaliacao-postural .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create avaliacao-postural
  $(".js-create-avaliacao-postural").click(loadForm);
  $("#modal-avaliacao-postural").on("submit", ".js-avaliacao-postural-create-form", saveForm);

  // Update avaliacao-postural
  $("#avaliacao-postural-table").on("click", ".js-update-avaliacao-postural", loadForm);
  $("#modal-avaliacao-postural").on("submit", ".js-avaliacao-postural-update-form", saveForm);

  // Delete avaliacao-postural
  $("#avaliacao-postural-table").on("click", ".js-delete-avaliacao-postural", loadForm);
  $("#modal-avaliacao-postural").on("submit", ".js-avaliacao-postural-delete-form", saveForm);

});



   
     