
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
       
       var table = $('#empresa-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-empresa .modal-content").html("");
        $("#modal-empresa").modal("show");
      },
      success: function (data) {
        $("#modal-empresa .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#empresa-table tbody").html(data.html_empresa_list);
          $("#modal-empresa").modal("hide");
        }
        else {
          $("#modal-empresa .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create empresa
  $(".js-create-empresa").click(loadForm);
  $("#modal-empresa").on("submit", ".js-empresa-create-form", saveForm);

  // Update empresa
  $("#empresa-table").on("click", ".js-update-empresa", loadForm);
  $("#modal-empresa").on("submit", ".js-empresa-update-form", saveForm);

  // Delete empresa
  $("#empresa-table").on("click", ".js-delete-empresa", loadForm);
  $("#modal-empresa").on("submit", ".js-empresa-delete-form", saveForm);

});



   
     