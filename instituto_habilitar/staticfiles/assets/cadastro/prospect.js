
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
       
       var table = $('#prospect-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-prospect .modal-content").html("");
        $("#modal-prospect").modal("show");
      },
      success: function (data) {
        $("#modal-prospect .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#prospect-table tbody").html(data.html_prospect_list);
          $("#modal-prospect").modal("hide");
        }
        else {
          $("#modal-prospect .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create prospect
  $(".js-create-prospect").click(loadForm);
  $("#modal-prospect").on("submit", ".js-prospect-create-form", saveForm);

  // Update prospect
  $("#prospect-table").on("click", ".js-update-prospect", loadForm);
  $("#modal-prospect").on("submit", ".js-prospect-update-form", saveForm);

  // Delete prospect
  $("#prospect-table").on("click", ".js-delete-prospect", loadForm);
  $("#modal-prospect").on("submit", ".js-prospect-delete-form", saveForm);

});



   
     