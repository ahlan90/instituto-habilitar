
/* DATA TABLES DE prereavaliacao */
$(document).ready(function() {
       
       var table = $('#prereavaliacao-table').DataTable( {
           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
          "autoWidth":false,
          "columns": [
                  { 
                      "width" : "5%",
                      
                  },
                  {
                      "width" : "10%",

                  },
                  null,
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  {
                      "width" : "5%",
                      "order" : false,
                  }
          ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $(".preloader").css('display','block');
        $("#modal-prereavaliacao .modal-content").html("");
        $("#modal-prereavaliacao").modal("show");
      },
      success: function (data) {
        $(".preloader").css('display','none');
        $("#modal-prereavaliacao .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#prereavaliacao-table tbody").html(data.html_prereavaliacao_list);
          $("#modal-prereavaliacao").modal("hide");
        }
        else {
          $("#modal-prereavaliacao .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create prereavaliacao
  $(".js-create-prereavaliacao").click(loadForm);
  $("#modal-prereavaliacao").on("submit", ".js-prereavaliacao-create-form", saveForm);

  // Update prereavaliacao
  $("#prereavaliacao-table").on("click", ".js-update-prereavaliacao", loadForm);
  $("#modal-prereavaliacao").on("submit", ".js-prereavaliacao-update-form", saveForm);

  // Delete prereavaliacao
  $("#prereavaliacao-table").on("click", ".js-delete-prereavaliacao", loadForm);
  $("#modal-prereavaliacao").on("submit", ".js-prereavaliacao-delete-form", saveForm);

});



   
     