
/* DATA TABLES DE avaliacao-clinica */
$(document).ready(function() {
       
       var table = $('#avaliacao-clinica-table').DataTable( {
           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
          "autoWidth":false,
          "columns": [
                  { 
                      "width" : "5%"
                  },
                  {
                    "width" : "5%"
                  },
                  null,
                  {
                      "width" : "5%",
                      "order" : false,

                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  {
                      "width" : "5%",
                      "order" : false,
                  }
          ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $(".preloader").css('display','block');
        $("#modal-avaliacao-clinica .modal-content").html("");
        $("#modal-avaliacao-clinica").modal("show");
      },
      success: function (data) {
        $(".preloader").css('display','none');
        $("#modal-avaliacao-clinica .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#avaliacao-clinica-table tbody").html(data.html_avaliacao_clinica_list);
          $("#modal-avaliacao-clinica").modal("hide");
        }
        else {
          $("#modal-avaliacao-clinica .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create avaliacao-clinica
  $(".js-create-avaliacao-clinica").click(loadForm);
  $("#modal-avaliacao-clinica").on("submit", ".js-avaliacao-clinica-create-form", saveForm);

  // Update avaliacao-clinica
  $("#avaliacao-clinica-table").on("click", ".js-update-avaliacao-clinica", loadForm);
  $("#modal-avaliacao-clinica").on("submit", ".js-avaliacao-clinica-update-form", saveForm);

  // Delete avaliacao-clinica
  $("#avaliacao-clinica-table").on("click", ".js-delete-avaliacao-clinica", loadForm);
  $("#modal-avaliacao-clinica").on("submit", ".js-avaliacao-clinica-delete-form", saveForm);

});



   
     