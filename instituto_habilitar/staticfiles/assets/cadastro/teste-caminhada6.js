
/* DATA TABLES DE teste-caminhada6 */
$(document).ready(function() {
       
       var table = $('#teste-caminhada6-table').DataTable( {
           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
          "autoWidth":false,
          "columns": [
                  { 
                      "width" : "5%"
                  },
                  {
                    "width" : "10%"
                  },
                  null,
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  {
                      "width" : "5%",
                      "order" : false,
                  }
          ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $(".preloader").css('display','block');
        $("#modal-teste-caminhada6 .modal-content").html("");
        $("#modal-teste-caminhada6").modal("show");
      },
      success: function (data) {
        $(".preloader").css('display','none');
        $("#modal-teste-caminhada6 .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#teste-caminhada6-table tbody").html(data.html_teste_caminhada6_list);
          $("#modal-teste-caminhada6").modal("hide");
        }
        else {
          $("#modal-teste-caminhada6 .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create teste-caminhada6
  $(".js-create-teste-caminhada6").click(loadForm);
  $("#modal-teste-caminhada6").on("submit", ".js-teste-caminhada6-create-form", saveForm);

  // Update teste-caminhada6
  $("#teste-caminhada6-table").on("click", ".js-update-teste-caminhada6", loadForm);
  $("#modal-teste-caminhada6").on("submit", ".js-teste-caminhada6-update-form", saveForm);

  // Delete teste-caminhada6
  $("#teste-caminhada6-table").on("click", ".js-delete-teste-caminhada6", loadForm);
  $("#modal-teste-caminhada6").on("submit", ".js-teste-caminhada6-delete-form", saveForm);

});



   
     