
/* DATA TABLES DE professor */
$(document).ready(function() {
       
       var table = $('#professor-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  null,
                  null,
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-professor .modal-content").html("");
        $("#modal-professor").modal("show");
      },
      success: function (data) {
        $("#modal-professor .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#professor-table tbody").html(data.html_professor_list);
          $("#modal-professor").modal("hide");
        }
        else {
          $("#modal-professor .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create professor
  $(".js-create-professor").click(loadForm);
  $("#modal-professor").on("submit", ".js-professor-create-form", saveForm);

  // Update professor
  $("#professor-table").on("click", ".js-update-professor", loadForm);
  $("#modal-professor").on("submit", ".js-professor-update-form", saveForm);

  // Delete professor
  $("#professor-table").on("click", ".js-delete-professor", loadForm);
  $("#modal-professor").on("submit", ".js-professor-delete-form", saveForm);

});



   
     