
/* DATA TABLES DE medico */
$(document).ready(function() {
       
       var table = $('#medico-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  null,
                  null,
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-medico .modal-content").html("");
        $("#modal-medico").modal("show");
      },
      success: function (data) {
        $("#modal-medico .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#medico-table tbody").html(data.html_medico_list);
          $("#modal-medico").modal("hide");
        }
        else {
          $("#modal-medico .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create medico
  $(".js-create-medico").click(loadForm);
  $("#modal-medico").on("submit", ".js-medico-create-form", saveForm);

  // Update medico
  $("#medico-table").on("click", ".js-update-medico", loadForm);
  $("#modal-medico").on("submit", ".js-medico-update-form", saveForm);

  // Delete medico
  $("#medico-table").on("click", ".js-delete-medico", loadForm);
  $("#modal-medico").on("submit", ".js-medico-delete-form", saveForm);

});



   
     