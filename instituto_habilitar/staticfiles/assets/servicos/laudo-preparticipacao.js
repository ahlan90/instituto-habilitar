
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
       
       var table = $('#laudo-preparticipacao-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  null,
                  { 
                      "width" : "22%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-laudo-preparticipacao .modal-content").html("");
        $("#modal-laudo-preparticipacao").modal("show");
      },
      success: function (data) {
        $("#modal-laudo-preparticipacao .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#laudo-preparticipacao-table tbody").html(data.html_laudo_preparticipacao_list);
          $("#modal-laudo-preparticipacao").modal("hide");
        }
        else {
          $("#modal-laudo-preparticipacao .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create laudo_preparticipacao
  $(".js-create-laudo-preparticipacao").click(loadForm);
  $("#modal-laudo-preparticipacao").on("submit", ".js-laudo-preparticipacao-create-form", saveForm);

  // Update laudo_preparticipacao
  $("#laudo-preparticipacao-table").on("click", ".js-update-laudo-preparticipacao", loadForm);
  $("#modal-laudo-preparticipacao").on("submit", ".js-laudo-preparticipacao-update-form", saveForm);

  // Delete laudo_preparticipacao
  $("#laudo-preparticipacao-table").on("click", ".js-delete-laudo-preparticipacao", loadForm);
  $("#modal-laudo-preparticipacao").on("submit", ".js-laudo-preparticipacao-delete-form", saveForm);

});



   
     