var graficos_spo2 = function() {

    setTimeout(function() {

    var regressao_medidas1_spo2 = regressaoArray(medidas1_spo2);
    var regressao_medidas2_spo2 = regressaoArray(medidas2_spo2);
    var regressao_medidas3_spo2 = regressaoArray(medidas3_spo2);
    var regressao_medidas4_spo2 = regressaoArray(medidas4_spo2);
    var regressao_medidas5_spo2 = regressaoArray(medidas5_spo2);
    var regressao_medidas_media_spo2 = regressaoArray(medidas_media_spo2);


    new Chart(document.getElementById("chart-spo21"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 1",
                    "data": medidas1_spo2,
                    "fill": false,
                    "borderColor": "rgb(38, 198, 21)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas1_spo2,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-spo22"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 2",
                    "data": medidas2_spo2,
                    "fill": false,
                    "borderColor": "rgb(38, 19, 218)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas2_spo2,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-spo23"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 3",
                    "data": medidas3_spo2,
                    "fill": false,
                    "borderColor": "rgb(3, 198, 218)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas4_spo2,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });

    new Chart(document.getElementById("chart-spo24"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 4",
                    "data": medidas4_spo2,
                    "fill": false,
                    "borderColor": "rgb(38, 19, 21)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas4_spo2,
                    type: 'line',
                    "fill": false,
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-spo25"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 5",
                    "data": medidas5_spo2,
                    "fill": false,
                    "borderColor": "rgb(8, 19, 218)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas5_spo2,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });
    
    new Chart(document.getElementById("chart-spo2-media"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 5",
                    "data": medidas_media_spo2,
                    "fill": false,
                    "borderColor": "rgb(8, 19, 218)",
                    "showLine" : false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas_media_spo2,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });
    
    }, 300);
}