/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
  
      
       
       var table = $('#aula-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "order": [
             [ 0, "none"],
            ],
           "autoWidth": false
 
       } );
      
      
       
});


$(function () {

  /* Functions */
  var loadForm = function () {

    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-controle-aula .modal-content").html("");
        $("#modal-controle-aula").modal("show");
      },
      success: function (data) {
        $("#modal-controle-aula .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    
    $('.preloader').css('display','block');
    
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#modal-controle-aula").modal("hide");
          $('.preloader').css('display','none');
        }
        else {
          $("#modal-controle-aula .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create controle-aula
  $(".js-create-controle-aula").click(loadForm);
  $("#modal-controle-aula").on("submit", ".js-controle-aula-create-form", saveForm);

  // Update controle-aula
  $("#controle-aula-table").on("click", ".js-update-controle-aula", loadForm);
  $("#modal-controle-aula").on("submit", ".js-controle-aula-update-form", saveForm);

  // Delete controle-aula
  $("#controle-aula-table").on("click", ".js-delete-controle-aula", loadForm);
  $("#modal-controle-aula").on("submit", ".js-controle-aula-delete-form", saveForm);
  

});



   
     