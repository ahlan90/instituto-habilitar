from .base import *
# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar',
]


MIDDLEWARE_CLASSES += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'institutohabilitar',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}


INTERNAL_IPS = ('127.0.0.1','https://institutohabilitar-ahlan90.c9users.io/')

def show_toolbar(request):
    return True
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
}