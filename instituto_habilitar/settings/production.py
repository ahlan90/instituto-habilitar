from .base import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DEBUG = False

DATABASES = { 
     'default': dj_database_url.config(
        default=config('HEROKU_POSTGRESQL_AQUA_URL')
    )
}

# settings.py
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'mysite.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'DEBUG',
        },
        'instituto_habilitar': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}