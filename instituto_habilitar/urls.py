from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from cadastro.forms import LoginForm
from django.contrib.auth import views as auth_view


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_view.login, {'template_name': 'login/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', auth_view.logout, {'next_page': '/login'}, name='logout'),
    url(r'', include('cadastro.urls')),
    url(r'', include('aula.urls')),
    url(r'', include('avaliacao_fisica.urls')),
    url(r'', include('notificacoes.urls')),
    url(r'', include('relatorios.urls')),
    url(r'', include('avaliacoes.urls')),
    url(r'', include('servicos.urls')),
    url(r'^select2/', include('django_select2.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

