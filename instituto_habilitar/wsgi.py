import os
from dj_static import Cling

from django.core.wsgi import get_wsgi_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "instituto_habilitar.settings.base")

# Descomentar producao
application = Cling(get_wsgi_application())


# Descomentar c9
#application = get_wsgi_application()
