from django import forms
from django.forms import ModelForm
from .models import *

class NotificacaoForm(ModelForm):
    
    class Meta:
        model = NotificacaoAvaliacao
        fields = ['data_agendamento']
        widgets = {
            'data_agendamento' : forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput', 'id' : 'data_clinica'}),
        }