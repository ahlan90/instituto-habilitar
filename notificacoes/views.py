from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.template.loader import render_to_string
from django.http import JsonResponse
import datetime
from .models import *
from cadastro.models import ParametrosGerencial
from avaliacoes.models import AvaliacaoClinica
from avaliacao_fisica.models import *
from .forms import *

# Create your views here.
@login_required
def notificacao_delete(request, pk):
    
    notificacao = get_object_or_404(NotificacaoAvaliacao, pk=pk)
    
    data = dict()
    
    if request.method == 'POST':
        notificacao.delete()
        data['form_is_valid'] = True
        notificacoes = NotificacaoAvaliacao.objects.all()
        data['html_notificacao_list'] = render_to_string('dashboard/lista_notificacao_parcial.html', {
            'notificacoes': notificacoes
        })
    else:
        context = {'notificacao': notificacao}
        data['html_form'] = render_to_string('dashboard/notificacao_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def forcar_notificacao(request):
    
    pacientes = Paciente.objects.all()
    
    parametro = ParametrosGerencial.objects.filter().first()
    
    for paciente in pacientes:
        
        
        # Notificacao para CONTRATO
        if(paciente.data_final_contrato != None):
            data_notificacao = paciente.data_final_contrato - datetime.timedelta(days=parametro.dias_aviso_contrato)
            notificacao = NotificacaoAvaliacao(data_agendamento=paciente.data_final_contrato + datetime.timedelta(days=parametro.dias_aviso_contrato),descricao_modelo='CON',paciente=paciente)
            notificacao.save()
        
        
        # Notificacao para AVALIACAO FISICA
        avaliacao_fisica = None
        
        try:
            avaliacao_fisica = AvaliacaoFisica.objects.filter(paciente=paciente).order_by('data').first()
        except:
            pass
    
        if(avaliacao_fisica != None):
            data_notificacao = avaliacao_fisica.data - datetime.timedelta(days=parametro.dias_aviso_avaliacao_fisica)
            notificacao = NotificacaoAvaliacao(data_agendamento=avaliacao_fisica.data+ datetime.timedelta(days=parametro.dias_aviso_avaliacao_fisica),descricao_modelo='AVF',paciente=paciente)
            notificacao.save()
        
        
        # Notificacao para AVALIACAO CLINICA
        avaliacao_clinica = None
        
        try:
            avaliacao_clinica = AvaliacaoClinica.objects.filter(paciente=paciente).order_by('data').first()
        except:
            pass
    
        if(avaliacao_clinica != None):
            
            if(avaliacao_clinica.data != None):
                notificacao = NotificacaoAvaliacao(data_agendamento=avaliacao_clinica.data + datetime.timedelta(days=parametro.dias_aviso_avaliacao_clinica),descricao_modelo='AVC',paciente=paciente)
                notificacao.save()
            
            if(avaliacao_clinica.data_entrega != None):
                notificacao = NotificacaoAvaliacao(data_agendamento=avaliacao_clinica.data_entrega + datetime.timedelta(days=parametro.dias_aviso_exames),descricao_modelo='EXA',paciente=paciente)
                notificacao.save()
            
            
    return redirect('dashboard')


@login_required
def save_notificacao_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            notificacoes = NotificacaoAvaliacao.objects.all()
            data['html_notificacao_list'] = render_to_string('dashboard/lista_notificacao_parcial.html', {
                'notificacoes': notificacoes
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def notificacao_update(request, pk):
    notificacao = get_object_or_404(NotificacaoAvaliacao, pk=pk)
    if request.method == 'POST':
        form = NotificacaoForm(request.POST, instance=notificacao)
    else:
        form = NotificacaoForm(instance=notificacao)
    return save_notificacao_form(request, form, 'dashboard/notificacao_update.html')