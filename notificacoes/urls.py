from django.shortcuts import render

# Create your views here.
from django.conf.urls import url, include
from django.contrib import admin
from notificacoes import views


urlpatterns = [
    url(r'^notificacao/delete/(?P<pk>\d+)$', views.notificacao_delete, name='notificacao_delete'),
    url(r'^notificacao/atualizar/(?P<pk>\d+)$', views.notificacao_update, name='notificacao_update'),
    url(r'^notificacao/forcar$', views.forcar_notificacao, name='forcar_notificacao'),
]