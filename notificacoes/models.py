from django.db import models
from cadastro.models import Paciente

# Create your models here.

class NotificacaoAvaliacao(models.Model):
    
    OPCOES_NOTIFICACAO = (
        ('AVF', 'Avaliação Clínica'),
        ('AVC', 'Avaliação Física'),
        ('CON', 'Vencimento de Contrato'),
        ('EXA', 'Exames')
    )
    
    data_agendamento = models.DateField()
    descricao_modelo = models.CharField(max_length=50, choices=OPCOES_NOTIFICACAO)
    paciente = models.ForeignKey(Paciente)