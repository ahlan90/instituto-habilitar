# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm, inlineformset_factory, Form
from aula.models import Medidas, ControleAula, Aula
from django_select2.forms import Select2MultipleWidget, Select2Widget
from cadastro.models import Paciente

class ControleAulaForm(ModelForm):
    
    class Meta:
        model = ControleAula
        exclude = ['paciente', 'aula']
        widgets = {
            'observacoes': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),
            'data': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
            'fct': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pa_inicial': forms.TextInput(attrs={'class': 'form-control textinput textInput pressao-arterial'}),
            'pa_final': forms.TextInput(attrs={'class': 'form-control textinput textInput pressao-arterial'}),
            'peso': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'tmr': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'resistencia': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'repeticoes': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'glicemia_pre': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'glicemia_pos': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'presente': forms.HiddenInput(),
        }


class MedidaControleAulaForm(ModelForm):
    
    class Meta:
        model = Medidas
        exclude = ()
        widgets = {
            'spo2': forms.NumberInput(attrs={'class': 'form-control textinput', 'placeholder' : 'SPO2'}),
            'borg': forms.NumberInput(attrs={'class': 'form-control textinput', 'placeholder' : 'BORG'}),
            'fc': forms.NumberInput(attrs={'class': 'form-control textinput', 'placeholder' : 'F.C.'}),
        }



class AulaForm(ModelForm):
    
    data = forms.DateField(widget=forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}))
    hora = forms.TimeField(widget=forms.TextInput(attrs={'type': 'time', 'class': 'form-control textinput textInput'}))
    
    class Meta:
        model = Aula
        fields = '__all__'
        widgets = {
            'alunos': Select2MultipleWidget(attrs={'class': 'form-control textinput textInput'}),
            'professor': forms.Select(attrs={'class': 'form-control textinput textInput'})
        }
        labels = {
            'professor': 'Professor Responsável',
        }



class UploadAulaForm(Form):
    
    paciente = forms.ModelMultipleChoiceField(queryset=Paciente.objects.all(), widget=Select2MultipleWidget)
    arquivo = forms.FileField()