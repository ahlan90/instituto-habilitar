# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from cadastro.models import Paciente
from datetime import datetime
import json


class Aula(models.Model):

    data = models.DateTimeField()
    hora = models.TimeField()
    professor = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True)
    
    alunos = models.ManyToManyField(Paciente)
    
    def __str__(self):
        return str(self.data)

    class Meta:
        ordering = ['-data', '-hora']



class ControleAula(models.Model):
    
    paciente = models.ForeignKey(Paciente)

    aula = models.ForeignKey(Aula, null=True)
    
    data = models.DateTimeField(blank=True, null=True)
    fct = models.CharField(max_length=200, blank=True)
    pa_inicial = models.CharField(max_length=200, blank=True)
    pa_final = models.CharField(max_length=200, blank=True)
    peso = models.CharField(max_length=200, blank=True)
    tmr = models.CharField(max_length=200, blank=True)
    resistencia = models.CharField(max_length=200, blank=True)
    repeticoes = models.CharField(max_length=200, blank=True)
    observacoes = models.CharField(max_length=200, blank=True)
    glicemia_pre = models.CharField(max_length=7, null=True, blank=True)
    glicemia_pos = models.CharField(max_length=7, null=True, blank=True)

    presente = models.BooleanField(default=False)
    

    # Metodo que marca presenca no objeto aula
    def save(self, *args, **kwargs):

        if not self.presente and self.pa_inicial != "":
            self.presente = True

        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.data)


class Medidas(models.Model):
    
    controle_aula = models.ForeignKey(ControleAula)

    spo2 = models.IntegerField(blank=True, default=0)
    fc = models.IntegerField(blank=True, default=0)
    borg = models.IntegerField(blank=True, default=0)

    # Metodo que marca presenca no objeto aula
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        controle_aula = self.controle_aula
        if not controle_aula.presente and (self.spo2 != 0 or self.fc != 0 or self.borg != 0):
            controle_aula.presente = True
            controle_aula.save()

    
    def toJSON(self):
         return {
            "spo2": self.spo2,
            "fc": self.fc,
            "borg": self.borg
            }