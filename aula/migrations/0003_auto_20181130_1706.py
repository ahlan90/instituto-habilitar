# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-11-30 19:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aula', '0002_auto_20181130_1706'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medidas',
            name='borg',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='medidas',
            name='fc',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='medidas',
            name='spo2',
            field=models.IntegerField(blank=True, default=0),
        ),
    ]
