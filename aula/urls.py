from django.conf.urls import url

from . import views

urlpatterns = [
     # URLs controle
    # url(r'^controleaula$', views.controle_aula_list, name='controle_aula_list'),
    url(r'^controleaula/create/(?P<aulaid>\d+)/(?P<pacienteid>\d+)$', views.controle_aula_create, name='controle_aula_create'),
    url(r'^controleaula/update/(?P<aulaid>\d+)/(?P<pacienteid>\d+)$', views.controle_aula_update, name='controle_aula_update'),
    url(r'^controleaula/delete/(?P<pk>\d+)$', views.controle_aula_delete, name='controle_aula_delete'),
    url(r'^controleaula/dashboard/(?P<pk>\d+)$', views.controle_aula_dashboard, name='controle_aula_dashboard'),
    url(r'^controleaula/import/update/(?P<pk>\d+)$', views.import_controleaula_update, name='import_controleaula_update'),
    
     # URLs aula
    url(r'^aula$', views.aula_list, name='aula_list'),
    url(r'^aula/create/$', views.aula_create, name='aula_create'),
    url(r'^aula/update/(?P<pk>\d+)$', views.aula_update, name='aula_update'),
    url(r'^aula/delete/(?P<pk>\d+)$', views.aula_delete, name='aula_delete'),
    url(r'^aula/alunos/(?P<pk>\d+)$', views.aula_alunos, name='aula_alunos'),
    url(r'^aula/controle/(?P<pk>\d+)$', views.paciente_controle_aula, name='paciente_controle_aula'),
    
    url(r'^planilha$', views.adicao_planilha, name='adicao_planilha'),
]