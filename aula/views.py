# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from aula.models import ControleAula, Medidas, Aula, Paciente
from aula.forms import ControleAulaForm, AulaForm, MedidaControleAulaForm, UploadAulaForm
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.core import serializers
from django.http import HttpResponse, request
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.forms import ModelForm, inlineformset_factory
from avaliacoes.models import TesteCaminhada6, AvaliacaoClinica
from avaliacao_fisica.models import AvaliacaoFisica
from decimal import Decimal
import datetime
from django.utils import formats
from openpyxl import load_workbook
from django.core.exceptions import ValidationError
import json


import time

"""
        VIEWS PARA PRE REAVALIACAO
"""

@login_required
def controle_aula_delete(request, pk):
    controle_aula = get_object_or_404(ControleAula, pk=pk)
    data = dict()
    if request.method == 'POST':
        controle_aula.delete()
        data['form_is_valid'] = True
        controles_aula = controle_aula.objects.all()
        data['html_controle_aula_list'] = render_to_string('controleaula/lista_controle_aula_parcial.html', {
            'controles_aula': controles_aula
        })
    else:
        context = {'controle_aula': controle_aula}
        data['html_form'] = render_to_string('controleaula/controle_aula_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def controle_aula_create(request, aulaid, pacienteid, template_name='controleaula/cria_controle_aula_modal.html'):

    aula = get_object_or_404(Aula, pk=aulaid)
    
    paciente = get_object_or_404(Paciente, pk=pacienteid)
    
    MedidaControleAulaFormSet = inlineformset_factory(ControleAula, Medidas, form=MedidaControleAulaForm, extra=5, max_num=5, can_delete=False)
    
    formset = MedidaControleAulaFormSet()
    
    if request.method == 'POST':
        
        form = ControleAulaForm(request.POST)
        if form.is_valid():
            controle_aula = form.save(commit=False)
            controle_aula.paciente = paciente
            controle_aula.aula = aula
            controle_aula.save()
            return redirect('aula_alunos', pk=aula.pk)
        else:
            print('Os erros: ' + str(form.errors.as_data()))
    else:
        form = ControleAulaForm(initial={'paciente': paciente, 'aula' : aula})
    
    return save_controle_aula_form(request, form, formset, paciente, aula, template_name)


@login_required
def save_controle_aula_form(request, form, formset, paciente, aula, template_name):
    
    data = dict()
    
    if request.method == 'POST':
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            data['form_is_valid'] = True
            redirect('aula_alunos', pk=aula.pk)
        else:
            for forms in formset:
                print('Os erros: ' + str(forms.errors.as_data()))
            data['form_is_valid'] = False
    
    context = { 
        'form': form, 
        'formset' : formset, 
        'paciente' : paciente, 
        'aula' : aula
    }
    
    data['html_form'] = render_to_string(template_name, context, request=request)
    
    return JsonResponse(data)


@login_required
def controle_aula_update(request, aulaid, pacienteid, template_name='controleaula/atualiza_controle_aula_modal.html'):
    
    aula = get_object_or_404(Aula, pk=aulaid)
    aula.data = formats.date_format(aula.data, "Y-m-d")
    paciente = get_object_or_404(Paciente, pk=pacienteid)
    
    MedidaControleAulaFormSet = inlineformset_factory(ControleAula, Medidas, form=MedidaControleAulaForm, extra=5, max_num=5, can_delete = False)

    
    try:
        controle_aula = ControleAula.objects.get(aula=aula, paciente=paciente)
    except ControleAula.DoesNotExist:
        controle_aula = None

    
    data = dict()
    
    if request.method == 'POST':
        
        form = ControleAulaForm(request.POST or None,instance=controle_aula)
        
        formset = MedidaControleAulaFormSet(request.POST, request.FILES, instance=controle_aula)
        
        if form.is_valid():
            controle_aula = form.save(commit=False)
            controle_aula.paciente = paciente
            controle_aula.aula = aula
            controle_aula.data = aula.data
            controle_aula.save()
            if formset.is_valid():
                for fmform in formset:
                    fm = fmform.save(commit=False)
                    fm.controle_aula = controle_aula
                    fm.save()
                data['form_is_valid'] = True
        else:
            for forms in formset:
                print ('Os erros: ' + str(forms.errors.as_data()))
            data['form_is_valid'] = False
    
    else:
        form = ControleAulaForm(instance=controle_aula)
        formset = MedidaControleAulaFormSet(instance=controle_aula)

    
    context = { 
        'form': form, 
        'formset' : formset, 
        'paciente' : paciente, 
        'aula' : aula
    }
    
    data['html_form'] = render_to_string(template_name, context, request=request)
    
    return JsonResponse(data)


@login_required
def import_controleaula_update(request, pk, template_name='controleaula/atualiza_import_controle_aula.html'):

    controle_aula = get_object_or_404(ControleAula, pk=pk)
    
    MedidaControleAulaFormSet = inlineformset_factory(ControleAula, Medidas, form=MedidaControleAulaForm, extra=5, max_num=5)

    data = dict()
    
    if request.method == 'POST':
        
        form = ControleAulaForm(request.POST or None,instance=controle_aula)
        
        formset = MedidaControleAulaFormSet(request.POST, request.FILES, instance=controle_aula)
        
        if form.is_valid():
            form.save()
            if formset.is_valid():
                formset.save()
                data['form_is_valid'] = True
                teste = []
                controles_aula = ControleAula.objects.filter(paciente=controle_aula.paciente).order_by('-data')
                
                for controle in controles_aula:
                    medidas = Medidas.objects.filter(controle_aula=controle)
                    teste.append((controle, medidas))
                    
                data['html_controle_aula_list'] = render_to_string('controleaula/lista_controle_aula_parcial.html', {
                    'controles_aula': teste,
                    "paciente": controle_aula.paciente
                })
        else:
            data['form_is_valid'] = False
    
    else:
        form = ControleAulaForm(instance=controle_aula)
        formset = MedidaControleAulaFormSet(instance=controle_aula)

    
    context = { 
        'form': form, 
        'formset': formset
    }
    
    data['html_form'] = render_to_string(template_name, context, request=request)
    
    return JsonResponse(data)
    

@login_required
def paciente_controle_aula(request, pk, template_name='controleaula/lista_controle_aula.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    teste = []
    controles_aula = ControleAula.objects.filter(paciente=paciente).order_by('-data')
    
    for controle in controles_aula:
        medidas = Medidas.objects.filter(controle_aula=controle).order_by('id')
        teste.append((controle, medidas))

    return render(request, template_name, {"controles_aula": teste, "paciente": paciente})





"""

     VIEWS PARA AULA

"""

@login_required
def aula_list(request, template_name='aula/lista_aula.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    aulas = Aula.objects.all()

    data = {}
    data['aulas'] = aulas
    
    return render(request, template_name, data)


@login_required
def aula_delete(request, pk):
    aula = get_object_or_404(Aula, pk=pk)
    data = dict()
    if request.method == 'POST':
        aula.delete()
        data['form_is_valid'] = True
        aulas = Aula.objects.all()
        data['html_aula_list'] = render_to_string('aula/lista_aula_parcial.html', {
            'aulas': aulas
        })
    else:
        context = {'aula': aula}
        data['html_form'] = render_to_string('aula/aula_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def aula_create(request, template_name='aula/cria_aula_form.html'):
    if request.method == 'POST':
        form = AulaForm(request.POST)
        if form.is_valid():
            aula = form.save()
            return HttpResponseRedirect(reverse(aula_alunos, args=(aula.pk,)))
    else:
        form = AulaForm()
    return render(request, template_name, {'form':form})


@login_required
def aula_update(request, pk, template_name='aula/cria_aula_form.html'):
    aula = get_object_or_404(Aula, pk=pk)
    form = AulaForm(request.POST or None, instance=aula)
    if form.is_valid():
        form.save()
        return redirect('aula_list')
    return render(request, template_name, {'form':form})


@login_required
def aula_alunos(request, pk, template_name='aula/lista_aula_paciente.html'):

    aula = get_object_or_404(Aula, pk=pk)
    aula.data = formats.date_format(aula.data, "Y-m-d")
    pacientes = aula.alunos.all()
    form = AulaForm(request.POST or None, instance=aula)
    
    for paciente in pacientes:
        
        avaliacao_fisica = AvaliacaoFisica.objects.filter(paciente=paciente).order_by('-id')
        avaliacao_clinica = AvaliacaoClinica.objects.filter(paciente=paciente).order_by('-id')
        teste_caminhada = TesteCaminhada6.objects.filter(paciente=paciente).order_by('-id')
        
        if len(avaliacao_clinica) > 0:
            paciente.avaliacao_clinica = avaliacao_clinica[0]
            
        
        if len(avaliacao_fisica) > 0:
            paciente.avaliacao_fisica = avaliacao_fisica[0]
            if paciente.avaliacao_fisica.pi_max_media != None:
                paciente.avaliacao_fisica.pi_max_media = paciente.avaliacao_fisica.pi_max_media * Decimal('0.6')
        
        if len(teste_caminhada) > 0:
            paciente.teste_caminhada = teste_caminhada[0]
            paciente.teste_caminhada.fc_pos_esforco = paciente.teste_caminhada.fc_pos_esforco
            
        
        
    if form.is_valid():
        form.save()
        return redirect('aula_alunos', pk=aula.id)

    data = {}
    data['pacientes'] = pacientes
    data['aula'] = aula
    data['form'] = form
    
    return render(request, template_name, data)


def adicao_planilha(request, template_name='aula/upload_aula_form.html'):

    if request.method == 'POST':
        
        form = UploadAulaForm(request.POST, request.FILES)
        if form.is_valid():
            converte_modelo(request.FILES['arquivo'], request.POST.get("paciente", ""))
            return redirect('paciente_controle_aula', pk=request.POST.get("paciente", ""))
    else:
        form = UploadAulaForm()
    
    return render(request, template_name, {'form':form})
    
    
def converte_modelo(arquivo, paciente_id):

    paciente = Paciente.objects.get(pk=paciente_id)
    
    wb = load_workbook(arquivo)
    
    ws = wb['FREQUÊNCIA CARDÍACA']
    
    """ Percorre a aba FREQUENCIA CARDIACA, nas colunas A -> F 
         a partir da 3 linha até a penultima
    """
    frequencias = []
    for row in ws['A{}:F{}'.format((ws.min_row + 3),(ws.max_row - 1))]:
        aula = []
        if row[0].value != None:
            for cell in row:
               aula.append(cell.value)
            frequencias.append(aula)

    ws = wb['SATURAÇÃO DE O2']
    
    saturacoes = []
    for row in ws['A{}:F{}'.format((ws.min_row + 3),(ws.max_row - 1))]:
        aula = []
        if row[0].value != None:
            for cell in row:
              aula.append(cell.value)
            saturacoes.append(aula)
    
    
    ws = wb['ESCALA DE BORG']
    escalas_borg = []
    for row in ws['A{}:F{}'.format((ws.min_row + 3),(ws.max_row - 1))]:
        aula = []
        if row[0].value != None:
            for cell in row:
              aula.append(cell.value)
            escalas_borg.append(aula)
        
    
    ws = wb['PAS']
    pas = []
    for row in ws['A{}:C{}'.format((ws.min_row + 1),ws.max_row)]:
        aula = []
        if row[0].value != None:
            for cell in row:
              aula.append(cell.value)
            pas.append(aula)
            print(aula)
        
    
    ws = wb['PAD']
    
    pad = []
    for row in ws['A{}:C{}'.format((ws.min_row + 1),ws.max_row)]:
        aula = []
        if row[0].value != None:
            for cell in row:
              aula.append(cell.value)
            pad.append(aula)
    
    
    # Criando o controle aula, concatenando todas planilhas
    controles_aula = concatena_tabelas(frequencias, saturacoes)
    controles_aula = concatena_tabelas(controles_aula, escalas_borg)
    controles_aula = concatena_tabelas(controles_aula, pas)
    controles_aula = concatena_tabelas(controles_aula, pad)

    # Criando e salvando todos controle aula
    for obj in controles_aula:
        
        controle_aula = ControleAula()
        controle_aula.paciente = paciente
        controle_aula.data = obj[0]
        controle_aula.pa_inicial = str(obj[16]) + "x" + str(obj[18])
        controle_aula.pa_final = str(obj[17]) + "x" + str(obj[19])
        controle_aula.save()
   
        medidas_aula2 = Medidas()
        medidas_aula2.controle_aula = controle_aula
        medidas_aula2.spo2 = str(obj[6])
        medidas_aula2.fc = str(obj[1])
        medidas_aula2.borg = str(obj[11])
        medidas_aula2.save()
        
        medidas_aula3 = Medidas()
        medidas_aula3.controle_aula = controle_aula
        medidas_aula3.spo2 = str(obj[7])
        medidas_aula3.fc = str(obj[2])
        medidas_aula3.borg = str(obj[12])
        medidas_aula3.save()
        
        medidas_aula4 = Medidas()
        medidas_aula4.controle_aula = controle_aula
        medidas_aula4.spo2 = str(obj[8])
        medidas_aula4.fc = str(obj[3])
        medidas_aula4.borg = str(obj[13])
        medidas_aula4.save()
        
        medidas_aula5 = Medidas()
        medidas_aula5.controle_aula = controle_aula
        medidas_aula5.spo2 = str(obj[9])
        medidas_aula5.fc = str(obj[4])
        medidas_aula5.borg = str(obj[14])
        medidas_aula5.save()
        
        medidas_aula6 = Medidas()
        medidas_aula6.controle_aula = controle_aula
        medidas_aula6.spo2 = str(obj[10])
        medidas_aula6.fc = str(obj[5])
        medidas_aula6.borg = str(obj[15])
        medidas_aula6.save()


@login_required
def controle_aula_dashboard(request, pk, template_name='controleaula/grafico_controle_aula.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    aulas = ControleAula.objects.filter(paciente=paciente).order_by('data')
    
    datas_aulas = []

    medidas_aulas = []
    
    medidas_inicial_diastolica = []
    medidas_inicial_sistolica = []
    
    medidas_final_diastolica = []
    medidas_final_sistolica = []

    for aula in aulas:
        
        # Criando array com as datas
        
        data_aula = str(aula.data.strftime('%d/%m/%y'))
        datas_aulas.append(data_aula)

        if(aula.pa_inicial != None or aula.pa_inicial != ""):
            if(len(aula.pa_inicial.split('x')) == 2):
                medidas_inicial_sistolica.append(aula.pa_inicial.split('x')[0])
                medidas_inicial_diastolica.append(aula.pa_inicial.split('x')[1])
        
        if(aula.pa_final != None or aula.pa_final != ""):
            if(len(aula.pa_final.split('x')) == 2):
                medidas_final_sistolica.append(aula.pa_final.split('x')[0])
                medidas_final_diastolica.append(aula.pa_final.split('x')[1])
        
        for index, elem in enumerate(datas_aulas):
            if index != 0 and index != len(datas_aulas)-1:
                datas_aulas[index] = ''
                
        
        # # #Data em que foi modificado o controle de aulas
        data_mudanca = datetime.date(2018, 12, 3)
        # Recuperando as medidas
        if(aula.data < data_mudanca):
            medidas = Medidas.objects.filter(controle_aula=aula)
        else:
            medidas = Medidas.objects.filter(controle_aula=aula).order_by('id')
        
        medida_retorno = {}
        
        medida_retorno['data'] = data_aula
        
        med = []
        #print('########################')
        for medida in medidas:
            #print(str(medida.toJSON()))
            med.append(medida.toJSON())
        
        medida_retorno['medidas'] = med
        
        medidas_aulas.append(medida_retorno)
    
    context = { 
        'paciente': paciente,
        'datas_aulas': json.dumps(datas_aulas),
        'medidas_aulas': json.dumps(medidas_aulas),
        'medidas_inicial_sistolica': json.dumps(medidas_inicial_sistolica),
        'medidas_inicial_diastolica': json.dumps(medidas_inicial_diastolica),
        'medidas_final_sistolica': json.dumps(medidas_final_sistolica),
        'medidas_final_diastolica': json.dumps(medidas_final_diastolica)
    }
    
    
    return render(request, template_name, context)


"""

    Concatena as tabelas mantendo a data que é comum a todas
    
"""
def concatena_tabelas(tabela1, tabela2):
    
    tabela3 = []
    
    for linha1 in tabela1:
        linha3 = []
        for linha2 in tabela2:
            if(linha1[0] == linha2[0]):
                linha3.extend(linha1)
                linha3.extend(linha2[1:])
                tabela3.append(linha3)
    
    return tabela3



def separa_medidas2(medidas, medidasfc, medidasborg, medidasspo2, i, medias):
    
    if medidas[i] != None:
        
        if medidas[i].fc != 'None' and medidas[i].fc != '':
            medidasfc.append(medidas[i].fc)
            medias[0] = medias[0] + int(medidas[i].fc)



def separa_medidas(medidas, medidasfc, medidasborg, medidasspo2, i, medias):

    medidasfc.append(medidas[i].fc)
    medias[0] = medias[0] + medidas[i].fc
    
    medidasborg.append(medidas[i].borg)
    medias[1] = medias[1] + medidas[i].borg

    medidasspo2.append(medidas[i].spo2)
    medias[2] = medias[2] + medidas[i].spo2
