from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from cadastro.models import Paciente
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import HttpResponse, request
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect
from datetime import datetime
from easy_pdf.rendering import render_to_pdf_response



"""
     VIEWS PARA CLIENTES
"""
@login_required
def cliente_servico_list(request, template_name='servicos/cliente_servico/lista_cliente_servico.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    cliente_servicos = Paciente.objects.filter(cliente_servico=True)
    
    return render(request, template_name, {'cliente_servicos' : cliente_servicos, 'menu-cliente-servico' : 'active'})


@login_required
def cliente_servico_delete(request, pk):
    cliente_servico = get_object_or_404(Paciente, pk=pk)
    data = dict()
    if request.method == 'POST':
        cliente_servico.delete()
        data['form_is_valid'] = True
        cliente_servicos = Paciente.objects.filter(cliente_servico=True)
        data['html_cliente_servico_list'] = render_to_string('servicos/cliente_servico/lista_cliente_servico_parcial.html', {
            'cliente_servicos': cliente_servicos
        })
    else:
        context = {'cliente_servico': cliente_servico}
        data['html_form'] = render_to_string('servicos/cliente_servico/cliente_servico_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_cliente_servico_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            cliente_servicos = Paciente.objects.all()
            data['html_paciente_list'] = render_to_string('paciente/lista_paciente_parcial.html', {
                'pacientes': cliente_servicos
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def cliente_servico_create(request):
    if request.method == 'POST':
        form = ClienteServicoForm(request.POST)
    else:
        form = ClienteServicoForm(initial={'cliente_servico': 'True'})
    return save_cliente_servico_form(request, form, 'servicos/cliente_servico/cria_cliente_servico_modal.html')


@login_required
def cliente_servico_update(request, pk):
    cliente_servico = get_object_or_404(Paciente, pk=pk)
    if request.method == 'POST':
        form = ClienteServicoForm(request.POST, instance=cliente_servico)
    else:
        form = ClienteServicoForm(instance=cliente_servico)
    return save_cliente_servico_form(request, form, 'servicos/cliente_servico/atualiza_cliente_servico_modal.html')






"""

   VIEWS PARA LAUDO DE PRE PARTICIPACAO
   
"""


@login_required
def laudo_preparticipacao_criar(request, template_name='servicos/laudo_preparticipacao/laudo_preparticipacao_criar.html'):

    if request.method == "POST":
        form = LaudoPreParticipacaoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('laudo_preparticipacao_listar', pk=form.instance.paciente.pk)
    else:
        form = LaudoPreParticipacaoForm(initial={'usuario': request.user})

    return render(request, template_name, {'form': form, 'menu_laudo_preparticipacao': 'active'})


@login_required
def laudo_preparticipacao_listar(request, pk, template_name='servicos/laudo_preparticipacao/laudo_preparticipacao_lista.html'):

    paciente = get_object_or_404(Paciente, pk=pk)
    laudos = LaudoPreParticipacao.objects.filter(paciente=paciente)

    data = {}

    data['laudos'] = laudos
    data['menu_laudo_preparticipacao'] = 'active'

    return render(request, template_name, data)


@login_required
def laudo_preparticipacao_atualizar(request, pk, template_name='servicos/laudo_preparticipacao/laudo_preparticipacao_atualizar.html'):

    laudo = get_object_or_404(LaudoPreParticipacao, pk=pk)

    if request.method == 'POST':
        form = LaudoPreParticipacaoForm(request.POST, instance=laudo)
        if form.is_valid():
            form.save()
            return redirect('laudo_preparticipacao_listar', pk=form.instance.paciente.pk)
    else:
        form = LaudoPreParticipacaoForm(instance=laudo)

    return render(request, template_name, {'form': form, 'menu_laudo_preparticipacao': 'active'})


@login_required
def laudo_preparticipacao_excluir(request, pk):

    laudo_preparticipacao = get_object_or_404(LaudoPreParticipacao, pk=pk)

    data = dict()

    if request.method == 'POST':
        laudo_preparticipacao.delete()
        data['form_is_valid'] = True
        laudos = LaudoPreParticipacao.objects.filter(paciente=laudo_preparticipacao.paciente)
        data['html_laudo_preparticipacao_list'] = render_to_string('servicos/laudo_preparticipacao/laudo_preparticipacao_lista_parcial.html', {
            'laudos': laudos
        })
    else:
        context = {'laudo': laudo_preparticipacao, 'menu_laudo_preparticipacao': 'active'}
        data['html_form'] = render_to_string('servicos/laudo_preparticipacao/laudo_preparticipacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


def laudo_preparticipacao_print(request, pk):

    laudo = get_object_or_404(LaudoPreParticipacao, pk=pk)
    paciente = laudo.paciente
    laudo_form = LaudoPreParticipacaoForm(instance=laudo)

    data = {
        'paciente': paciente,
        'laudo': laudo_form,
    }

    return render_to_pdf_response(request, 'servicos/laudo_preparticipacao/laudo_preparticipacao_print.html', data)
    #return render(request, 'servicos/laudo_preparticipacao/laudo_preparticipacao_print.html', data)