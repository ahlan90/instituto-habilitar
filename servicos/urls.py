from django.conf.urls import url, include
from django.contrib import admin
from servicos import views

urlpatterns = [

    # URLs CLIENTE SERVICO
    url(r'^cliente_servico$', views.cliente_servico_list, name='cliente_servico_list'),
    url(r'^cliente_servico/create/$', views.cliente_servico_create, name='cliente_servico_create'),
    url(r'^cliente_servico/update/(?P<pk>\d+)$', views.cliente_servico_update, name='cliente_servico_update'),
    url(r'^cliente_servico/delete/(?P<pk>\d+)$', views.cliente_servico_delete, name='cliente_servico_delete'),
    
    
        # URLs LAUDO PRE PARTICIPCAO    
    url(r'^laudo_preparticipacao/(?P<pk>\d+)$', views.laudo_preparticipacao_listar, name='laudo_preparticipacao_listar'),
    url(r'^laudo_preparticipacao/create/$', views.laudo_preparticipacao_criar, name='laudo_preparticipacao_criar'),
    url(r'^laudo_preparticipacao/update/(?P<pk>\d+)$', views.laudo_preparticipacao_atualizar, name='laudo_preparticipacao_atualizar'),
    url(r'^laudo_preparticipacao/delete/(?P<pk>\d+)$', views.laudo_preparticipacao_excluir, name='laudo_preparticipacao_excluir'),
    url(r'^laudo_preparticipacao/print/(?P<pk>\d+)$', views.laudo_preparticipacao_print, name='laudo_preparticipacao_print'),
    
]
