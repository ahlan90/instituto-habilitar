from django import forms
from django.forms import ModelForm
from .models import *
from cadastro.models import Paciente


class ClienteServicoForm(ModelForm):
    
    class Meta:
        model = Paciente
        fields = ['nome', 'telefone', 'data_nascimento', 'cpf', 'cliente_servico', 'sexo']
        widgets = {
            'cliente_servico': forms.HiddenInput(),
            'data_nascimento': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
        }



class LaudoPreParticipacaoForm(ModelForm):
    
    class Meta:
        model = LaudoPreParticipacao
        fields = '__all__'
        widgets = {
            'usuario': forms.HiddenInput(),
            'objetivo': forms.Textarea(),
            'texto_avaliacao_clinica': forms.Textarea(),
            'texto_avaliacao_fisica': forms.Textarea(),
            'texto_avaliacao_nutricional': forms.Textarea(),
            'data_criacao': forms.TextInput(attrs={'type': 'date'}),
        }
        
    def __init__(self, *args, **kwargs):
        super(LaudoPreParticipacaoForm, self).__init__(*args, **kwargs)
        self.fields['paciente'].queryset = Paciente.objects.filter(cliente_servico=True)