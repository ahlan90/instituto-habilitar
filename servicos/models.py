# -*- coding: utf-8 -*-
from django.db import models
from cadastro.models import Paciente
from django.conf import settings


# Create your models here.
class LaudoPreParticipacao(models.Model):
    
    paciente = models.ForeignKey(Paciente)
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Responsável", null=True, blank=True)
    data_criacao = models.DateField("Data do laudo", null=True, blank=True)
    
    objetivo = models.CharField(max_length=1500, null=True, blank=True)
    texto_avaliacao_clinica = models.CharField("Descrição Avaliação Clínica", max_length=4000, null=True, blank=True)
    texto_avaliacao_fisica = models.CharField("Descrição Avaliação Física", max_length=4000, null=True, blank=True)
    texto_avaliacao_nutricional = models.CharField("Descrição Avaliação Nutricional", max_length=4000, null=True, blank=True)