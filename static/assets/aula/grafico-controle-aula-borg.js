var graficos_borg = function() {

    setTimeout(function (){},100);
    
    var regressao_medidas1_borg = regressaoArray(medidas1_borg);
    var regressao_medidas2_borg = regressaoArray(medidas2_borg);
    var regressao_medidas3_borg = regressaoArray(medidas3_borg);
    var regressao_medidas4_borg = regressaoArray(medidas4_borg);
    var regressao_medidas5_borg = regressaoArray(medidas5_borg);
    var regressao_medidas_media_borg = regressaoArray(medidas_media_borg);


    new Chart(document.getElementById("chart-borg1"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 1",
                    "data": medidas1_borg,
                    "fill": false,
                    "borderColor": "rgb(38, 198, 21)",
                    "showLine": false
                },
                {
                    label: 'Linha Tendência',
                    data: regressao_medidas1_borg,
                    "fill": false,
                    type: 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-borg2"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 2",
                    "data": medidas2_borg,
                    "fill": false,
                    "borderColor": "rgb(38, 19, 218)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas2_borg,
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-borg3"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 3",
                    "data": medidas3_borg,
                    "fill": false,
                    "borderColor": "rgb(3, 198, 218)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas3_borg,
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });

    new Chart(document.getElementById("chart-borg4"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 4",
                    "data": medidas4_borg,
                    "fill": false,
                    "borderColor": "rgb(38, 19, 21)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas4_borg,
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-borg5"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 5",
                    "data": medidas5_borg,
                    "fill": false,
                    "borderColor": "rgb(8, 19, 218)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas5_borg,
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });
    
    
    new Chart(document.getElementById("chart-borg-media"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Média Medidas BORG",
                    "data": medidas_media_borg,
                    "fill": false,
                    "borderColor": "rgb(8, 19, 218)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas_media_borg,
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });
}