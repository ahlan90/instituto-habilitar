
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
  
      
       
       var table = $('#aula-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "order": [
             [ 0, "none"],
            ],
           "autoWidth": false,
           "columns": [
                  { 
                      "width" : "10%",
                  },
                  { 
                      "width" : "10%",
                  },
                  { 
                      "width" : "10%",
                  },
                  { 
                      "width" : "50%",
                  },
                  { 
                      "width" : "10%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "10%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "10%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-aula .modal-content").html("");
        $("#modal-aula").modal("show");
      },
      success: function (data) {
        $("#modal-aula .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#aula-table tbody").html(data.html_aula_list);
          $("#modal-aula").modal("hide");
        }
        else {
          $("#modal-aula .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create aula
  $(".js-create-aula").click(loadForm);
  $("#modal-aula").on("submit", ".js-aula-create-form", saveForm);

  // Update aula
  $("#aula-table").on("click", ".js-update-aula", loadForm);
  $("#modal-aula").on("submit", ".js-aula-update-form", saveForm);

  // Delete aula
  $("#aula-table").on("click", ".js-delete-aula", loadForm);
  $("#modal-aula").on("submit", ".js-aula-delete-form", saveForm);

});



   
     