/**
 * 
 * 
 * DATA TABLES
 * 
 * */



$( document ).ready(function() {
  

  var data_nascimento = $('#id_nascimento').val();
  
  $('#id_idade').val(calculaIdade(data_nascimento));
  var idade = Number(calculaIdade(data_nascimento));
  var fc_maxima = 220 - idade;
  $('#id_fc_maxima').val(fc_maxima);
  



/** 
 * Calculo de IMC
**/

//Funcao que altera os campos
$( "#id_peso, #id_estatura" )
  .change(function () {
    
    var peso = $('#id_peso').val();
    var estatura = $('#id_estatura').val();
    
    if(peso != null && peso != "" && estatura != null && estatura != ""){
        var imc = calculo_imc(estatura,peso);
        
        $('#id_imc').val(imc);
        
        if(imc != null && imc != ""){
          
        classifica_imc(imc);
        
    }
        
    }
    
  })
  .change();

// Funcao que Classificao IMC
function classifica_imc(imc){

  if(imc < 18){
     $('#id_classificacao_imc').val('MAGREZA');
  }
  else{
    
    if(imc >= 18 && imc < 25){
      $('#id_classificacao_imc').val('NORMAL'); 
    }
    else{
      
      if(imc >= 25 && imc < 30){
        $('#id_classificacao_imc').val('SOBREPESO');
      }
      else{
        
        if(imc >= 30 && imc < 35){
          $('#id_classificacao_imc').val('OBESIDADE1');
        }
        else{
          
          if(imc >= 35 && imc < 40){
            $('#id_classificacao_imc').val('OBESIDADE2');
          }
          else{
            
            if(imc >= 40){
              $('#id_classificacao_imc').val('OBESIDADE3');
            }
            
          }
        }
        
      }
    }
    
  }
        
}

// Funcao que calcula o IMC
function calculo_imc(altura, peso){
    
    altura = altura/100;
    
    resultado = Number(peso/(altura*altura)).toFixed(2);
    
    return resultado;
}

/**
 * 
 * Funcao que Classifica a Gordura
 * 
 **/
$( "#id_percentual_gordura" )
  .change(function () {
  
  var sexo = $('#id_sexo').val();
  var idade = $('#id_idade').val();
  var bf = $('#id_percentual_gordura').val();
  
  if(idade < 26){
    if(sexo == 'Feminino'){
        
        if(bf > 0 && bf != "" && bf <17){
          $('#id_classificacao_gordura').val('EXCELENTE');
        }
        else{
          if(bf >= 17 && bf <20){
            $('#id_classificacao_gordura').val('BOM');
          }
          else {
            
            if(bf >= 20 && bf <23){
              $('#id_classificacao_gordura').val('ACIMAMEDIA');
            }
            else {
              
              if(bf >= 23 && bf <26){
                $('#id_classificacao_gordura').val('MEDIA');
              }
              else {
                
                if(bf >= 26 && bf <29){
                  $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                }
                else {
                  
                  if(bf >= 29 && bf <32){
                    $('#id_classificacao_gordura').val('RUIM');
                  }
                  else {
                    
                    if(bf >= 32){
                      $('#id_classificacao_gordura').val('MUITORUIM');
                    }
                    
                  }
                  
                  
                }
          
              }
              
            }
            
          }
        }
        
      }
      else{
        
        if(bf > 0 && bf != "" && bf <7){
          $('#id_classificacao_gordura').val('EXCELENTE');
        }
        else{
          if(bf >= 7 && bf <11){
            $('#id_classificacao_gordura').val('BOM');
          }
          else {
            
            if(bf >= 11 && bf < 14){
              $('#id_classificacao_gordura').val('ACIMAMEDIA');
            }
            else {
              
              if(bf >= 14 && bf <17){
                $('#id_classificacao_gordura').val('MEDIA');
              }
              else {
                
                if(bf >= 17 && bf <21){
                  $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                }
                else {
                  
                  if(bf >= 21 && bf <24){
                    $('#id_classificacao_gordura').val('RUIM');
                  }
                  else {
                    
                    if(bf >= 24){
                      $('#id_classificacao_gordura').val('MUITORUIM');
                    }
                    
                  }
                  
                  
                }
          
              }
              
            }
            
          }
        }
        
      }
  }
  else{
    if(idade >= 26 && idade < 36){
      
      if(sexo == 'Feminino'){
        
        if(bf > 0 && bf != "" && bf < 17){
          $('#id_classificacao_gordura').val('EXCELENTE');
        }
        else{
          if(bf >= 17 && bf < 21){
            $('#id_classificacao_gordura').val('BOM');
          }
          else {
            
            if(bf >= 21 && bf < 24){
              $('#id_classificacao_gordura').val('ACIMAMEDIA');
            }
            else {
              
              if(bf >= 24 && bf < 27){
                $('#id_classificacao_gordura').val('MEDIA');
              }
              else {
                
                if(bf >= 27 && bf < 30){
                  $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                }
                else {
                  
                  if(bf >= 30 && bf < 33){
                    $('#id_classificacao_gordura').val('RUIM');
                  }
                  else {
                    
                    if(bf >= 33){
                      $('#id_classificacao_gordura').val('MUITORUIM');
                    }
                    
                  }
                  
                  
                }
          
              }
              
            }
            
          }
        }
        
      }
      else{
        
        if(bf > 0 && bf != "" && bf < 12){
          $('#id_classificacao_gordura').val('EXCELENTE');
        }
        else{
          if(bf >= 12 && bf < 16){
            $('#id_classificacao_gordura').val('BOM');
          }
          else {
            
            if(bf >= 16 && bf < 19){
              $('#id_classificacao_gordura').val('ACIMAMEDIA');
            }
            else {
              
              if(bf >= 19 && bf < 22){
                $('#id_classificacao_gordura').val('MEDIA');
              }
              else {
                
                if(bf >= 22 && bf < 25){
                  $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                }
                else {
                  
                  if(bf >= 25 && bf < 29){
                    $('#id_classificacao_gordura').val('RUIM');
                  }
                  else {
                    
                    if(bf >= 29){
                      $('#id_classificacao_gordura').val('MUITORUIM');
                    }
                    
                  }
                  
                  
                }
          
              }
              
            }
            
          }
        }
        
      }
      
    }
    else{
      if(idade >= 36 && idade < 46){
        
        
         if(sexo == 'Feminino'){
        
            if(bf > 0 && bf != "" && bf < 20){
              $('#id_classificacao_gordura').val('EXCELENTE');
            }
            else{
              if(bf >= 20 && bf < 24){
                $('#id_classificacao_gordura').val('BOM');
              }
              else {
                
                if(bf >= 24 && bf < 27){
                  $('#id_classificacao_gordura').val('ACIMAMEDIA');
                }
                else {
                  
                  if(bf >= 27 && bf < 30){
                    $('#id_classificacao_gordura').val('MEDIA');
                  }
                  else {
                    
                    if(bf >= 30 && bf < 33){
                      $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                    }
                    else {
                      
                      if(bf >= 33 && bf < 37){
                        $('#id_classificacao_gordura').val('RUIM');
                      }
                      else {
                        
                        if(bf >= 37){
                          $('#id_classificacao_gordura').val('MUITORUIM');
                        }
                        
                      }
                      
                      
                    }
              
                  }
                  
                }
                
              }
            }
            
          }
          else{
            
            if(bf > 0 && bf != "" && bf < 15){
              $('#id_classificacao_gordura').val('EXCELENTE');
            }
            else{
              if(bf >= 15 && bf < 19){
                $('#id_classificacao_gordura').val('BOM');
              }
              else {
                
                if(bf >= 19 && bf < 22){
                  $('#id_classificacao_gordura').val('ACIMAMEDIA');
                }
                else {
                  
                  if(bf >= 22 && bf < 24){
                    $('#id_classificacao_gordura').val('MEDIA');
                  }
                  else {
                    
                    if(bf >= 24 && bf < 27){
                      $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                    }
                    else {
                      
                      if(bf >= 27 && bf < 30){
                        $('#id_classificacao_gordura').val('RUIM');
                      }
                      else {
                        
                        if(bf >= 30){
                          $('#id_classificacao_gordura').val('MUITORUIM');
                        }
                        
                      }
                      
                      
                    }
              
                  }
                  
                }
                
              }
            }
            
          }
  
      }
      else{
        if(idade >= 46 && idade < 56){

           if(sexo == 'Feminino'){
        
            if(bf > 0 && bf != "" && bf < 22){
              $('#id_classificacao_gordura').val('EXCELENTE');
            }
            else{
              if(bf >= 22 && bf < 26){
                $('#id_classificacao_gordura').val('BOM');
              }
              else {
                
                if(bf >= 26 && bf < 29){
                  $('#id_classificacao_gordura').val('ACIMAMEDIA');
                }
                else {
                  
                  if(bf >= 29 && bf < 32){
                    $('#id_classificacao_gordura').val('MEDIA');
                  }
                  else {
                    
                    if(bf >= 32 && bf < 35){
                      $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                    }
                    else {
                      
                      if(bf >= 35 && bf < 39){
                        $('#id_classificacao_gordura').val('RUIM');
                      }
                      else {
                        
                        if(bf >= 39){
                          $('#id_classificacao_gordura').val('MUITORUIM');
                        }
                        
                      }
                      
                      
                    }
              
                  }
                  
                }
                
              }
            }
            
          }
          else{
            
            if(bf > 0 && bf != "" && bf < 17){
              $('#id_classificacao_gordura').val('EXCELENTE');
            }
            else{
              if(bf >= 17 && bf < 21){
                $('#id_classificacao_gordura').val('BOM');
              }
              else {
                
                if(bf >= 21 && bf < 24){
                  $('#id_classificacao_gordura').val('ACIMAMEDIA');
                }
                else {
                  
                  if(bf >= 24 && bf < 26){
                    $('#id_classificacao_gordura').val('MEDIA');
                  }
                  else {
                    
                    if(bf >= 26 && bf < 28){
                      $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                    }
                    else {
                      
                      if(bf >= 28 && bf < 31){
                        $('#id_classificacao_gordura').val('RUIM');
                      }
                      else {
                        
                        if(bf >= 31){
                          $('#id_classificacao_gordura').val('MUITORUIM');
                        }
                        
                      }
                      
                      
                    }
              
                  }
                  
                }
                
              }
            }
            
          }
          
        }
        else{
          if(idade >=56){
            
             if(sexo == 'Feminino'){
        
              if(bf > 0 && bf != "" && bf < 23){
                $('#id_classificacao_gordura').val('EXCELENTE');
              }
              else{
                if(bf >= 23 && bf < 27){
                  $('#id_classificacao_gordura').val('BOM');
                }
                else {
                  
                  if(bf >= 27 && bf < 30){
                    $('#id_classificacao_gordura').val('ACIMAMEDIA');
                  }
                  else {
                    
                    if(bf >= 30 && bf < 33){
                      $('#id_classificacao_gordura').val('MEDIA');
                    }
                    else {
                      
                      if(bf >= 33 && bf < 36){
                        $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                      }
                      else {
                        
                        if(bf >= 36 && bf < 39){
                          $('#id_classificacao_gordura').val('RUIM');
                        }
                        else {
                          
                          if(bf >= 39){
                            $('#id_classificacao_gordura').val('MUITORUIM');
                          }
                          
                        }
                        
                        
                      }
                
                    }
                    
                  }
                  
                }
              }
              
            }
            else{
              
              if(bf > 0 && bf != "" && bf < 20){
                $('#id_classificacao_gordura').val('EXCELENTE');
              }
              else{
                if(bf >= 20 && bf < 23){
                  $('#id_classificacao_gordura').val('BOM');
                }
                else {
                  
                  if(bf >= 23 && bf < 25){
                    $('#id_classificacao_gordura').val('ACIMAMEDIA');
                  }
                  else {
                    
                    if(bf >= 25 && bf < 28){
                      $('#id_classificacao_gordura').val('MEDIA');
                    }
                    else {
                      
                      if(bf >= 28 && bf < 31){
                        $('#id_classificacao_gordura').val('ABAIXOMEDIA');
                      }
                      else {
                        
                        if(bf >= 31 && bf < 33){
                          $('#id_classificacao_gordura').val('RUIM');
                        }
                        else {
                          
                          if(bf >= 33){
                            $('#id_classificacao_gordura').val('MUITORUIM');
                          }
                          
                        }
                        
                        
                      }
                
                    }
                    
                  }
                  
                }
              }
              
            }
            
          }
        }
      }
    }
  }
  

})
  .change();



/**
 * 
 * Funcao que Classifica o ICQ
 * 
 **/
$( "#id_cintura, #id_quadril" )
  .change(function () {
  
    var cintura = $('#id_cintura').val();
    var quadril = $('#id_quadril').val();
    
    if(cintura != null && cintura != "" && quadril != null && quadril != ""){
        
        var icq = cintura/quadril;
        
        $('#id_icq').val(Math.round(icq * 100) / 100);
        $("#id_icq").trigger("change");
        
    }

})
  .change();



/**
 * 
 * Funcao que Classifica a Gordura
 * 
 **/
$( "#id_icq" )
  .change(function () {
  
  var sexo = $('#id_sexo').val();
  var idade = $('#id_idade').val();
  var icq = $('#id_icq').val();
  
  if(idade < 30){
    
    if(sexo == 'Feminino'){
      
      if(icq >0 && icq != "" && icq < 0.71){
        $('#id_resultado_icq').val('BAIXO');
      }
      else{
        if(icq >= 0.71 && icq <= 0.78){
          $('#id_resultado_icq').val('MODERADO');  
        }
        else{
          if(icq > 0.78 && icq <= 0.83){
            $('#id_resultado_icq').val('ALTO');
          }
          else{
            if(icq > 0.83){
              $('#id_resultado_icq').val('MUITO ALTO');
            }
          }
        }
      }
      
    }
    else{
      
      if(icq >0 && icq != "" && icq < 0.83){
        $('#id_resultado_icq').val('BAIXO');
      }
      else{
        if(icq >= 0.83 && icq <= 0.89){
          $('#id_resultado_icq').val('MODERADO');  
        }
        else{
          if(icq > 0.89 && icq <= 0.94){
            $('#id_resultado_icq').val('ALTO');
          }
          else{
            if(icq > 0.94){
              $('#id_resultado_icq').val('MUITO ALTO');
            }
          }
        }
      }
      
    }
    
  }
  else{
    if(idade >= 30 && idade < 40){
      
      if(sexo == 'Feminino'){
        
        
        if(icq >0 && icq != "" && icq < 0.72){
          $('#id_resultado_icq').val('BAIXO');
        }
        else{
          if(icq >= 0.72 && icq <= 0.79){
            $('#id_resultado_icq').val('MODERADO');  
          }
          else{
            if(icq > 0.79 && icq <= 0.84){
              $('#id_resultado_icq').val('ALTO');
            }
            else{
              if(icq > 0.84){
                $('#id_resultado_icq').val('MUITO ALTO');
              }
            }
          }
        }
        
        
      }
      else{
        
        if(icq >0 && icq != "" && icq < 0.84){
          $('#id_resultado_icq').val('BAIXO');
        }
        else{
          if(icq >= 0.84 && icq <= 0.92){
            $('#id_resultado_icq').val('MODERADO');  
          }
          else{
            if(icq > 0.92 && icq <= 0.96){
              $('#id_resultado_icq').val('ALTO');
            }
            else{
              if(icq > 0.96){
                $('#id_resultado_icq').val('MUITO ALTO');
              }
            }
          }
        }
        
      }
     
      
    }
    else{
      if(idade >= 40 && idade < 50){
        
          if(sexo == 'Feminino'){
           
            if(icq >0 && icq != "" && icq < 0.73){
              $('#id_resultado_icq').val('BAIXO');
            }
            else{
              if(icq >= 0.73 && icq <= 0.8){
                $('#id_resultado_icq').val('MODERADO');  
              }
              else{
                if(icq > 0.8 && icq <= 0.87){
                  $('#id_resultado_icq').val('ALTO');
                }
                else{
                  if(icq > 0.87){
                    $('#id_resultado_icq').val('MUITO ALTO');
                  }
                }
              }
            }
        
        
          }
          else{
            
            if(icq >0 && icq != "" && icq < 0.88){
              $('#id_resultado_icq').val('BAIXO');
            }
            else{
              if(icq >= 0.88 && icq <= 0.96){
                $('#id_resultado_icq').val('MODERADO');  
              }
              else{
                if(icq > 0.96 && icq <= 1){
                  $('#id_resultado_icq').val('ALTO');
                }
                else{
                  if(icq > 1){
                    $('#id_resultado_icq').val('MUITO ALTO');
                  }
                }
              }
            }
            
          }
         
  
      }
      else{
        if(idade >= 50 && idade < 60){

          if(sexo == 'Feminino'){
            
            if(icq >0 && icq != "" && icq < 0.74){
              $('#id_resultado_icq').val('BAIXO');
            }
            else{
              if(icq >= 0.74 && icq <= 0.81){
                $('#id_resultado_icq').val('MODERADO');  
              }
              else{
                if(icq > 0.81 && icq <= 0.88){
                  $('#id_resultado_icq').val('ALTO');
                }
                else{
                  if(icq > 0.88){
                    $('#id_resultado_icq').val('MUITO ALTO');
                  }
                }
              }
            }
        
          }
          else{
            
            if(icq >0 && icq != "" && icq < 0.90){
              $('#id_resultado_icq').val('BAIXO');
            }
            else{
              if(icq >= 0.90 && icq <= 0.96){
                $('#id_resultado_icq').val('MODERADO');  
              }
              else{
                if(icq > 0.96 && icq <= 1.02){
                  $('#id_resultado_icq').val('ALTO');
                }
                else{
                  if(icq > 1.02){
                    $('#id_resultado_icq').val('MUITO ALTO');
                  }
                }
              }
            }
            
          }
          
        }
        else{
          if(idade >= 60){
            
            if(sexo == 'Feminino'){
              
              if(icq >0 && icq != "" && icq < 0.76){
                $('#id_resultado_icq').val('BAIXO');
              }
              else{
                if(icq >= 0.76 && icq <= 0.83){
                  $('#id_resultado_icq').val('MODERADO');  
                }
                else{
                  if(icq > 0.83 && icq <= 0.90){
                    $('#id_resultado_icq').val('ALTO');
                  }
                  else{
                    if(icq > 0.90){
                      $('#id_resultado_icq').val('MUITO ALTO');
                    }
                  }
                }
              }
        
            }
            else{
              if(icq >0 && icq != "" && icq < 0.91){
                $('#id_resultado_icq').val('BAIXO');
              }
              else{
                if(icq >= 0.91 && icq <= 0.98){
                  $('#id_resultado_icq').val('MODERADO');  
                }
                else{
                  if(icq > 0.98 && icq <= 1.03){
                    $('#id_resultado_icq').val('ALTO');
                  }
                  else{
                    if(icq > 1.03){
                      $('#id_resultado_icq').val('MUITO ALTO');
                    }
                  }
                }
              }
            }
            
          }
        }
      }
    }
  }
  

})
  .change();



/**
 * 
 * FUNCAO QUE CALCULA IDADE
 * 
 **/


function calculaIdade(dateString) {
  var aniversario = +new Date(dateString);
  return ~~((Date.now() - aniversario) / (31557600000));
}


/**
 * Calculos Força de Preensão
**/

$( "#id_md_medida_1, #id_md_medida_2, #id_md_medida_3" )
  .change(function () {
    
    var sexo = $('#id_sexo').val();
    var idade = $('#id_idade').val();
    
    var medida1 = Number($('#id_md_medida_1').val());
    var medida2 = Number($('#id_md_medida_2').val());
    var medida3 = Number($('#id_md_medida_3').val());
    
    
    if(medida1 != null && medida1 != "" && medida2 != null && medida2 != "" && medida3 != null && medida3 != ""){
        
        var media = Number((medida1 + medida2 + medida3) / 3).toFixed(2);
        
        $('#id_md_media').val(media);
        
        if(idade < 55){
          
          if(sexo == 'Masculino'){
            
            if(media > 0 && media != null && media < 44.2){
              $('#id_md_resultado').val('REGULAR')
            }
            else{
              if( media >= 44.2 && media <= 56.9){
                $('#id_md_resultado').val('BOM')
              }
              else{
                if(media > 56.9){
                  $('#id_md_resultado').val('OTIMO')
                }
              }
            }
            
          }
          else{
            
            if(media > 0 && media != null && media < 26.7){
              $('#id_md_resultado').val('REGULAR')
            }
            else{
              if( media >= 26.7 && media <= 30.2){
                $('#id_md_resultado').val('BOM')
              }
              else{
                if(media > 30.2){
                  $('#id_md_resultado').val('OTIMO')
                }
              }
            }
            
          }
          
        }
        else{
          if(idade >= 55 && idade < 60){
            
            if(sexo == 'Masculino'){
              
              if(media > 0 && media != null && media < 36.7){
                $('#id_md_resultado').val('REGULAR')
              }
              else{
                if( media >= 36.7 && media <= 51.4){
                  $('#id_md_resultado').val('BOM')
                }
                else{
                  if(media > 51.4){
                    $('#id_md_resultado').val('OTIMO')
                  }
                }
              }
              
            }
            else{
              
              if(media > 0 && media != null && media < 26.4){
                $('#id_md_resultado').val('REGULAR')
              }
              else{
                if( media >= 26.4 && media <= 33.6){
                  $('#id_md_resultado').val('BOM')
                }
                else{
                  if(media > 33.6){
                    $('#id_md_resultado').val('OTIMO')
                  }
                }
              }
              
            }
            
          }
          else{
            if(idade >=60 && idade < 65){
              
              if(sexo == 'Masculino'){
                
                if(media > 0 && media != null && media < 36.8){
                  $('#id_md_resultado').val('REGULAR')
                }
                else{
                  if( media >= 38.8 && media <= 46.7){
                    $('#id_md_resultado').val('BOM')
                  }
                  else{
                    if(media > 46.7){
                      $('#id_md_resultado').val('OTIMO')
                    }
                  }
                }
                
              }
              else{
                
                if(media > 0 && media != null && media < 22.2){
                  $('#id_md_resultado').val('REGULAR')
                }
                else{
                  if( media >= 22.2 && media <= 29.6){
                    $('#id_md_resultado').val('BOM')
                  }
                  else{
                    if(media > 29.6){
                      $('#id_md_resultado').val('OTIMO')
                    }
                  }
                }
                
              }
              
            }
            else{
              if(idade >= 65 && idade < 70){
                
                if(sexo == 'Masculino'){
                  
                  
                  if(media > 0 && media != null && media < 35.4){
                    $('#id_md_resultado').val('REGULAR')
                  }
                  else{
                    if( media >= 35.4 && media <= 47.9){
                      $('#id_md_resultado').val('BOM')
                    }
                    else{
                      if(media > 47.9){
                        $('#id_md_resultado').val('OTIMO')
                      }
                    }
                  }
                  
                }
                else{
                  
                  
                  if(media > 0 && media != null && media < 22.5){
                    $('#id_md_resultado').val('REGULAR')
                  }
                  else{
                    if( media >= 22.5 && media <= 28.8){
                      $('#id_md_resultado').val('BOM')
                    }
                    else{
                      if(media > 28.8){
                        $('#id_md_resultado').val('OTIMO')
                      }
                    }
                  }
                  
                }
                
              }
              else{
                if(idade >=70 && idade < 75){
                  
                  if(sexo == 'Masculino'){
                    
                    
                    if(media > 0 && media != null && media < 32){
                      $('#id_md_resultado').val('REGULAR')
                    }
                    else{
                      if( media >= 32 && media <= 44.5){
                        $('#id_md_resultado').val('BOM')
                      }
                      else{
                        if(media > 44.5){
                          $('#id_md_resultado').val('OTIMO')
                        }
                      }
                    }
                    
                  }
                  else{
                    
                    if(media > 0 && media != null && media < 20.7){
                      $('#id_md_resultado').val('REGULAR')
                    }
                    else{
                      if( media >= 20.7 && media <= 27.8){
                        $('#id_md_resultado').val('BOM')
                      }
                      else{
                        if(media > 27.8){
                          $('#id_md_resultado').val('OTIMO')
                        }
                      }
                    }
                    
                  }
                  
                }
                else {
                  if(idade >= 75){
                    
                    if(sexo == 'Masculino'){
                      
                      if(media > 0 && media != null && media < 12.7){
                       $('#id_md_resultado').val('REGULAR')
                      }
                      else{
                        if( media >= 12.7 && media <= 31){
                          $('#id_md_resultado').val('BOM')
                        }
                        else{
                          if(media > 31){
                            $('#id_md_resultado').val('OTIMO')
                          }
                        }
                      }
                      
                    }
                    else{
                      
                      if(media > 0 && media != null && media < 16){
                       $('#id_md_resultado').val('REGULAR')
                      }
                      else{
                        if( media >= 16 && media <= 19.9){
                          $('#id_md_resultado').val('BOM')
                        }
                        else{
                          if(media > 19.9){
                            $('#id_md_resultado').val('OTIMO')
                          }
                        }
                      }
                    
                  }
                }
              }
            }
          }
        }
      }
    }
  })
  .change();


$( "#id_mnd_medida_1, #id_mnd_medida_2, #id_mnd_medida_3" )
  .change(function () {

    var sexo = $('#id_sexo').val();
    var idade = $('#id_idade').val();
    
    var medida1 = Number($('#id_mnd_medida_1').val());
    var medida2 = Number($('#id_mnd_medida_2').val());
    var medida3 = Number($('#id_mnd_medida_3').val());
    
    if(medida1 != null && medida1 != "" && medida2 != null && medida2 != "" && medida3 != null && medida3 != ""){
        
        var media = Number((medida1 + medida2 + medida3) / 3).toFixed(2);
        
        $('#id_mnd_media').val(media);
        
        if(idade < 55){
          
          if(sexo == 'Masculino'){
            
            if(media > 0 && media != null && media < 39.4){
              $('#id_mnd_resultado').val('REGULAR')
            }
            else{
              if( media >= 39.4 && media <= 51.1){
                $('#id_mnd_resultado').val('BOM')
              }
              else{
                if(media > 51.1){
                  $('#id_mnd_resultado').val('OTIMO')
                }
              }
            }
            
          }
          else{
            
            if(media > 0 && media != null && media < 24){
              $('#id_mnd_resultado').val('REGULAR')
            }
            else{
              if( media >= 24 && media <= 33.5){
                $('#id_mnd_resultado').val('BOM')
              }
              else{
                if(media > 33.5){
                  $('#id_mnd_resultado').val('OTIMO')
                }
              }
            }
            
          }
          
        }
        else{
          if(idade >= 55 && idade < 60){
            
            if(sexo == 'Masculino'){
              
              if(media > 0 && media != null && media < 33.7){
                $('#id_mnd_resultado').val('REGULAR')
              }
              else{
                if( media >= 33.7 && media <= 48.4){
                  $('#id_mnd_resultado').val('BOM')
                }
                else{
                  if(media > 48.4){
                    $('#id_mnd_resultado').val('OTIMO')
                  }
                }
              }
              
            }
            else{
              
              if(media > 0 && media != null && media < 24.6){
                $('#id_mnd_resultado').val('REGULAR')
              }
              else{
                if( media >= 24.6 && media <= 29.5){
                  $('#id_mnd_resultado').val('BOM')
                }
                else{
                  if(media > 29.5){
                    $('#id_mnd_resultado').val('OTIMO')
                  }
                }
              }
              
            }
            
          }
          else{
            if(idade >=60 && idade < 65){
              
              if(sexo == 'Masculino'){
                
                if(media > 0 && media != null && media < 33.4){
                  $('#id_mnd_resultado').val('REGULAR')
                }
                else{
                  if( media >= 33.4 && media <= 44){
                    $('#id_mnd_resultado').val('BOM')
                  }
                  else{
                    if(media > 44){
                      $('#id_mnd_resultado').val('OTIMO')
                    }
                  }
                }
                
              }
              else{
                
                if(media > 0 && media != null && media < 18.6){
                  $('#id_mnd_resultado').val('REGULAR')
                }
                else{
                  if( media >= 18.6 && media <= 27.3){
                    $('#id_mnd_resultado').val('BOM')
                  }
                  else{
                    if(media > 27.3){
                      $('#id_mnd_resultado').val('OTIMO')
                    }
                  }
                }
                
              }
              
            }
            else{
              if(idade >= 65 && idade < 70){
                
                if(sexo == 'Masculino'){
                  
                  
                  if(media > 0 && media != null && media < 32){
                    $('#id_mnd_resultado').val('REGULAR')
                  }
                  else{
                    if( media >= 32 && media <= 44.4){
                      $('#id_mnd_resultado').val('BOM')
                    }
                    else{
                      if(media > 44.4){
                        $('#id_mnd_resultado').val('OTIMO')
                      }
                    }
                  }
                  
                }
                else{
                  
                  
                  if(media > 0 && media != null && media < 19.6){
                    $('#id_mnd_resultado').val('REGULAR')
                  }
                  else{
                    if( media >= 19.6 && media <= 26.2){
                      $('#id_mnd_resultado').val('BOM')
                    }
                    else{
                      if(media > 26.2){
                        $('#id_mnd_resultado').val('OTIMO')
                      }
                    }
                  }
                  
                }
                
              }
              else{
                if(idade >=70 && idade < 75){
                  
                  if(sexo == 'Masculino'){
                    
                    
                    if(media > 0 && media != null && media < 30.3){
                      $('#id_mnd_resultado').val('REGULAR')
                    }
                    else{
                      if( media >= 30.3 && media <= 42.1){
                        $('#id_mnd_resultado').val('BOM')
                      }
                      else{
                        if(media > 42.1){
                          $('#id_mnd_resultado').val('OTIMO')
                        }
                      }
                    }
                    
                  }
                  else{
                    
                    if(media > 0 && media != null && media < 19.1){
                      $('#id_mnd_resultado').val('REGULAR')
                    }
                    else{
                      if( media >= 19.1 && media <= 25.8){
                        $('#id_mnd_resultado').val('BOM')
                      }
                      else{
                        if(media > 25.8){
                          $('#id_mnd_resultado').val('OTIMO')
                        }
                      }
                    }
                    
                  }
                  
                }
                else {
                  if(idade >= 75){
                    
                    if(sexo == 'Masculino'){
                      
                      if(media > 0 && media != null && media < 24.8){
                       $('#id_mnd_resultado').val('REGULAR')
                      }
                      else{
                        if( media >= 24.8 && media <= 34.7){
                          $('#id_mnd_resultado').val('BOM')
                        }
                        else{
                          if(media > 34.7){
                            $('#id_mnd_resultado').val('OTIMO')
                          }
                        }
                      }
                      
                    }
                    else{
                      
                      if(media > 0 && media != null && media < 14.7){
                       $('#id_mnd_resultado').val('REGULAR')
                      }
                      else{
                        if( media >= 14.7 && media <= 18.1){
                          $('#id_mnd_resultado').val('BOM')
                        }
                        else{
                          if(media > 18.1){
                            $('#id_mnd_resultado').val('OTIMO')
                          }
                        }
                      }
                    
                  }
                }
              }
            }
          }
        }
      }
        
    }
    
  })
  .change();



/**
 * Calculos Manovacuometria
**/

$( "#id_pi_max_medida_1, #id_pi_max_medida_2, #id_pi_max_medida_3" )
  .change(function () {
    
    
    var medida1 = Number($('#id_pi_max_medida_1').val());
    var medida2 = Number($('#id_pi_max_medida_2').val());
    var medida3 = Number($('#id_pi_max_medida_3').val());
    
    var maiorValor = 0;
    
    if(medida1 <= medida2 && medida1 <= medida3){
      maiorValor = medida1;
    }
    else{
      if(medida2 <= medida3){
        maiorValor = medida2;
      }
      else{
        maiorValor = medida3;
      }
    }
    
    if(maiorValor != null && maiorValor != "" && maiorValor != 0){
        
        
        $('#id_pi_max_media').val(maiorValor)
        
        if(maiorValor <= -75){
          $('#id_pi_max_resultado').val('NORMAL')
        }
        else{
          if(maiorValor < -40 && maiorValor > -75){
            $('#id_pi_max_resultado').val('FRAQUEZA')
          }
          else{
            if(maiorValor >= -40 && maiorValor < -20 ){
              $('#id_pi_max_resultado').val('FADIGA')  
            }
            else{
              if(maiorValor >= -20){
                $('#id_pi_max_resultado').val('FALENCIA')
              }
            }
          }
        }
        
    }
    
  })
  .change();


$( "#id_pe_max_medida_1, #id_pe_max_medida_2, #id_pe_max_medida_3" )
  .change(function () {
    
    
    var medida1 = Number($('#id_pe_max_medida_1').val());
    var medida2 = Number($('#id_pe_max_medida_2').val());
    var medida3 = Number($('#id_pe_max_medida_3').val());
    
    
    var maiorValor = 0;
    
    if(medida1 >= medida2 && medida1 >= medida3){
      maiorValor = medida1;
    }
    else{
      if(medida2 >= medida3){
        maiorValor = medida2;
      }
      else{
        maiorValor = medida3;
      }
    }
    
    if(maiorValor != null && maiorValor != "" && maiorValor != 0){
        
        $('#id_pe_max_media').val(maiorValor)
        
        if(maiorValor <= 90){
          $('#id_pe_max_resultado').val('INSUFICIENTE')
        }
        else{
          if(maiorValor > 90){
            $('#id_pe_max_resultado').val('NORMAL')
          }
        }
    }
    
  })
  .change();


/**
 * Calculos Cintura
**/

$( "#id_cintura" )
  .change(function () {
    
    
    var cintura = Number($('#id_cintura').val());
    var sexo = $('#id_sexo').val();
    
    if(cintura != null && cintura != ""){
      
      if(sexo == 'Feminino'){
        if(cintura < 80){
          $('#id_res_circunferencia_cintura').val('BAIXO')
        }
        else{
          if(cintura >= 80 && cintura < 88){
            $('#id_res_circunferencia_cintura').val('ELEVADO')
          }
          else{
            if(cintura >= 88){
              $('#id_res_circunferencia_cintura').val('MUITO ELEVADO')
            }
          }
        }
      }
      else{
        
         if(cintura < 94){
            $('#id_res_circunferencia_cintura').val('BAIXO')
          }
          else{
            if(cintura >= 94 && cintura < 102){
              $('#id_res_circunferencia_cintura').val('ELEVADO')
            }
            else{
              if(cintura >= 102){
                $('#id_res_circunferencia_cintura').val('MUITO ELEVADO')
              }
            }
          }
        
      }
        
    }
    
  })
  .change();




/**
 * Calculos EXAMES
**/

$( "#id_frequencia_cardiaca" )
  .change(function () {
    
    
    var fc = Number($('#id_frequencia_cardiaca').val());
    
    var fc_max = Number($('#id_fc_maxima').val())
    
    
    if(fc != null && fc != ""){
      
        $('#id_fc_basal').val(fc);
    
        $('#id_fc_reserva').val(fc_max - fc);
        
        if(fc < 60){
          $('#id_av_frequencia_cardiaca').val('BRADICARDIA');
        }
        else{
          if(fc >= 60 && fc < 80){
            $('#id_av_frequencia_cardiaca').val('NORMAL');  
          }
          else{
           if(fc >= 80 && fc < 100){
              $('#id_av_frequencia_cardiaca').val('NORMALALTA');  
            }
            else{
              if(fc >= 100){
                $('#id_av_frequencia_cardiaca').val('TAQUICARDIA');  
              }
            }
          }
        }
        
    }
    
  })
  .change();


$( "#id_pa_sistolica" )
  .change(function () {
    
    var sistolica = Number($('#id_pa_sistolica').val());
    
    if(sistolica != null && sistolica != ""){
        
        if(sistolica < 130){
          $('#id_av_pa_sistolica').val('NORMAL');
        }
        else{
          if(sistolica >= 130 && sistolica < 140){
            $('#id_av_pa_sistolica').val('LIMITROFE');  
          }
          else{
           if(sistolica >= 140 && sistolica < 160){
              $('#id_av_pa_sistolica').val('LEVE');  
            }
            else{
              if(sistolica >= 160 && sistolica < 180){
                $('#id_av_pa_sistolica').val('MODERADA');  
              }
              else{
                if(sistolica >= 180){
                  $('#id_av_pa_sistolica').val('GRAVE');  
                } 
              }
            }
          }
        }
        
    }
    
  })
  .change();


$( "#id_pa_diastolica" )
  .change(function () {
    
    
    var diastolica = Number($('#id_pa_diastolica').val());
    
    if(diastolica != null && diastolica != ""){
      
      if(diastolica < 85){
          $('#id_av_pa_diastolica').val('NORMAL');
        }
        else{
          if(diastolica >= 85 && diastolica < 90){
            $('#id_av_pa_diastolica').val('LIMITROFE');  
          }
          else{
           if(diastolica >= 90 && diastolica < 100){
              $('#id_av_pa_diastolica').val('LEVE');  
            }
            else{
              if(diastolica >= 100 && diastolica < 110){
                $('#id_av_pa_diastolica').val('MODERADA');  
              }
              else{
                if(diastolica >= 110){
                  $('#id_av_pa_diastolica').val('GRAVE');  
                } 
              }
            }
          }
        }   
    }
    
  })
  .change();


$( "#id_colesterol" )
  .change(function () {
    
    
    var colesterol = Number($('#id_colesterol').val());
    
    if(colesterol != null && colesterol != ""){
      
      if(colesterol < 200){
        $('#id_av_colesterol').val('DESEJAVEL');
      }
      else{
        if(colesterol >= 200 && colesterol < 240){
          $('#id_av_colesterol').val('LIMITROFE');
        }else{
          if(colesterol >= 240){
            $('#id_av_colesterol').val('RISCOS');
          }
        }
      }
        
    }
    
  })
  .change();


$( "#id_oximetria_pulso" )
  .change(function () {
    
    
    var oximetria = Number($('#id_oximetria_pulso').val());
    
    if(oximetria != null && oximetria != ""){
      
      if(oximetria < 90){
        $('#id_av_oximetria_pulso').val('MUITO BAIXO');
      }
      else{
        if(oximetria >= 90 && oximetria < 92){
          $('#id_av_oximetria_pulso').val('BAIXO');
        }else{
          if(oximetria >= 92 && oximetria < 94){
            $('#id_av_oximetria_pulso').val('ACEITAVEL');
          }
          else{
            if(oximetria >= 95 && oximetria <= 100){
              $('#id_av_oximetria_pulso').val('NORMAL');
            }
          }
        }
      }
        
    }
    
  })
  .change();


$( "#id_triglicerideos" )
  .change(function () {
    
    
    var triglicerideos = Number($('#id_triglicerideos').val());
    
    if(triglicerideos != null && triglicerideos != ""){
      
      if(triglicerideos < 150){
        $('#id_av_triglicerideos').val('DESEJAVEL');
      }
      else{
        if(triglicerideos >= 150 && triglicerideos < 200){
          $('#id_av_triglicerideos').val('LIMITROFE');
        }else{
          if(triglicerideos >= 200){
            $('#id_av_triglicerideos').val('RISCOS');
          }
        }
      }
        
    }
    
  })
  .change();
  

$( "#id_hdl" )
  .change(function () {
    
    
    var hdl = Number($('#id_hdl').val());
    
    if(hdl != null && hdl != ""){
      
      if(hdl > 40){
        $('#id_av_hdl').val('DESEJAVEL');
      }
      else{
        if(hdl >= 35 && hdl <= 40){
          $('#id_av_hdl').val('LIMITROFE');
        }else{
          if(hdl < 35){
            $('#id_av_hdl').val('RISCOS');
          }
        }
      }
        
    }
    
  })
  .change();


$( "#id_glicose" )
  .change(function () {
    
    
    var glicose = Number($('#id_glicose').val());
    
    if(glicose != null && glicose != ""){
      
      if(glicose < 100){
        $('#id_av_glicose').val('DESEJAVEL');
      }
      else{
        if(glicose >= 100 && glicose <= 126){
          $('#id_av_glicose').val('LIMITROFE');
        }else{
          if(glicose > 126){
            $('#id_av_glicose').val('RISCOS');
          }
        }
      }
        
    }
    
  })
   .change();
  



/**
 * 
 * CALCULO DE METABOLISMO
 * 
 * */
 
 $( "#id_nivel_atividade_fisica" )
  .change(function () {
    
    var sexo = $('#id_sexo').val();
    var peso = Number($('#id_peso').val());
    var idade = Number($('#id_idade').val());
    var altura = Number($('#id_estatura').val());
    var nivel = $('#id_nivel_atividade_fisica').val();
    
    if(nivel != null && nivel != "" && peso != null && peso != "" && altura != null && altura != ""){
      
      var fator_atividade = 0;
      
      if(sexo == 'Masculino'){
        
        switch(nivel) {
          
            case 'ATLETA':
                fator_atividade = 2.4;
                break;
            case 'MUITOATIVO':
                fator_atividade = 2.1;
                break;
            case 'ATIVO':
                fator_atividade = 1.7;
                break;
            case 'POUCOATIVO':
                fator_atividade = 1.6;
                break;
            case 'SEDENTARIO':
                fator_atividade = 1.3;
                break;
                
        }
        
        var taxa_basal = 66 + (13.7 * peso) + (5 * altura) - (6.8 * idade);
        
        $('#id_metabolismo_basal').val(Math.round(taxa_basal));
        
        var gasto_energetico = taxa_basal * fator_atividade;
        
        $('#id_gasto_energetico').val(Math.round(gasto_energetico));
        
      }
      else{
        
        switch(nivel) {
          
            case 'ATLETA':
                fator_atividade = 2.2;
                break;
            case 'MUITOATIVO':
                fator_atividade = 1.9;
                break;
            case 'ATIVO':
                fator_atividade = 1.6;
                break;
            case 'POUCOATIVO':
                fator_atividade = 1.5;
                break;
            case 'SEDENTARIO':
                fator_atividade = 1.3;
                break;
                
        }
        
        var taxa_basal = 665 + (9.6 * peso) + (1.8 * altura) - (4.7 * idade);
        
        $('#id_metabolismo_basal').val(Math.round(taxa_basal));
        
        var gasto_energetico = taxa_basal * fator_atividade;
        
        $('#id_gasto_energetico').val(Math.round(gasto_energetico));
        
      }
      
    }
    
  })
   .change();
 


$( "#id_fc_maxima" )
  .change(function () {
    
    
    var fc_maxima = Number($('#id_fc_maxima').val());
    
  })
   .change();
  


$( "#id_intensidade_treino" )
  .change(function () {
    
    
    var intensidade_treino = $('#id_intensidade_treino').val();
    
    var fc_basal = Number($('#id_fc_basal').val());
    var fc_reserva = Number($('#id_fc_reserva').val());
    
    var perc_inferior;
    var perc_superior;
    
    var valor_inferior;
    var valor_superior;
    
    if(intensidade_treino == 'MUITOBAIXA'){
      
      perc_inferior = 0;
      perc_superior = 28;
      
      $('#id_fc_reserva_inferior').val(perc_inferior);
      $('#id_fc_reserva_superior').val(perc_superior);
      
      valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
      valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
      
      $('#id_fc_reserva_inferior_num').val(valor_inferior);
      $('#id_fc_reserva_superior_num').val(valor_superior);
      
      
    }else{
      if(intensidade_treino == 'BAIXA'){
        
        perc_inferior = 28;
        perc_superior = 42;
        
        $('#id_fc_reserva_inferior').val(perc_inferior);
        $('#id_fc_reserva_superior').val(perc_superior);
        
        valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
        valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
        
        $('#id_fc_reserva_inferior_num').val(valor_inferior);
        $('#id_fc_reserva_superior_num').val(valor_superior);
        
      }
      else{
        if(intensidade_treino == 'MODERADA'){
        
          perc_inferior = 42;
          perc_superior = 56;
          
          $('#id_fc_reserva_inferior').val(perc_inferior);
          $('#id_fc_reserva_superior').val(perc_superior);
          
        valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
        valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
          
          $('#id_fc_reserva_inferior_num').val(valor_inferior);
          $('#id_fc_reserva_superior_num').val(valor_superior);
      
        }
        else{
          if(intensidade_treino == 'MODERADAALTA'){
            
            perc_inferior = 56;
            perc_superior = 70;
            
            $('#id_fc_reserva_inferior').val(perc_inferior);
            $('#id_fc_reserva_superior').val(perc_superior);
            
            valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
            valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
            
            $('#id_fc_reserva_inferior_num').val(valor_inferior);
            $('#id_fc_reserva_superior_num').val(valor_superior);
      
          }
          else{
            if(intensidade_treino == 'ALTA'){
              
              perc_inferior = 70;
              perc_superior = 83;
              
              $('#id_fc_reserva_inferior').val(perc_inferior);
              $('#id_fc_reserva_superior').val(perc_superior);
              
              valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
              valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
              
              $('#id_fc_reserva_inferior_num').val(valor_inferior);
              $('#id_fc_reserva_superior_num').val(valor_superior);
              
      
            }
            else{
              if(intensidade_treino == 'MUITOALTA'){
                
                perc_inferior = 83;
                perc_superior = 100;
                
                $('#id_fc_reserva_inferior').val(perc_inferior);
                $('#id_fc_reserva_superior').val(perc_superior);

                valor_inferior = Math.round((fc_reserva * perc_inferior / 100) + fc_basal);
                valor_superior = Math.round((fc_reserva * perc_superior / 100) + fc_basal);
                
                $('#id_fc_reserva_inferior_num').val(valor_inferior);
                $('#id_fc_reserva_superior_num').val(valor_superior);
                
              }
            }
          }
        }
      }
    }
  })
   .change();
  
  
    
});