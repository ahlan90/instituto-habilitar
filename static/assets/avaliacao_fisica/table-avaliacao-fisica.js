
/* DATA TABLES DE AVALIACAOFISICA */
$(document).ready(function() {
       
       var table = $('#avaliacao-fisica-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth":false,
           "columns": [
                  { 
                      "width" : "5%"
                  },
                  {
                      "width" : "10%"
                  },
                  null,
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $(".preloader").css('display','block');
        $("#modal-avaliacao-fisica .modal-content").html("");
        $("#modal-avaliacao-fisica").modal("show");
      },
      success: function (data) {
        $(".preloader").css('display','none');
        $("#modal-avaliacao-fisica .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#avaliacao-fisica-table tbody").html(data.html_avaliacao_fisica_list);
          $("#modal-avaliacao-fisica").modal("hide");
        }
        else {
          $("#modal-avaliacao-fisica .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create avaliacao-fisica
  $(".js-create-avaliacao-fisica").click(loadForm);
  $("#modal-avaliacao-fisica").on("submit", ".js-avaliacao-fisica-create-form", saveForm);

  // Update avaliacao-fisica
  $("#avaliacao-fisica-table").on("click", ".js-update-avaliacao-fisica", loadForm);
  $("#modal-avaliacao-fisica").on("submit", ".js-avaliacao-fisica-update-form", saveForm);

  // Delete avaliacao-fisica
  $("#avaliacao-fisica-table").on("click", ".js-delete-avaliacao-fisica", loadForm);
  $("#modal-avaliacao-fisica").on("submit", ".js-avaliacao-fisica-delete-form", saveForm);

});



   
     