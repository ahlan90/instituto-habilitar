
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
  
      
       
       var table = $('#notificacao-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  { 
                      "width" : "10%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "10%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "10%",
                  },
                  { 
                      "width" : "30%",
                  },
                  { 
                      "width" : "40%",
                  }
                  
            ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-notificacao .modal-content").html("");
        $("#modal-notificacao").modal("show");
      },
      success: function (data) {
        $("#modal-notificacao .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#notificacao-table tbody").html(data.html_notificacao_list);
          $("#modal-notificacao").modal("hide");
        }
        else {
          $("#modal-notificacao .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create notificacao
  $(".js-create-notificacao").click(loadForm);
  $("#modal-notificacao").on("submit", ".js-notificacao-create-form", saveForm);

  // Update notificacao
  $("#notificacao-table").on("click", ".js-update-notificacao", loadForm);
  $("#modal-notificacao").on("submit", ".js-notificacao-update-form", saveForm);

  // Delete notificacao
  $("#notificacao-table").on("click", ".js-delete-notificacao", loadForm);
  $("#modal-notificacao").on("submit", ".js-notificacao-delete-form", saveForm);

});



   
     