var medidas1_fc = new Array();
var medidas2_fc = new Array();
var medidas3_fc = new Array();
var medidas4_fc = new Array();
var medidas5_fc = new Array();
var medidas_media_fc = new Array();

var medidas1_spo2 = new Array();
var medidas2_spo2 = new Array();
var medidas3_spo2 = new Array();
var medidas4_spo2 = new Array();
var medidas5_spo2 = new Array();
var medidas_media_spo2 = new Array();

var medidas1_borg = new Array();
var medidas2_borg = new Array();
var medidas3_borg = new Array();
var medidas4_borg = new Array();
var medidas5_borg = new Array();
var medidas_media_borg = new Array();


medidas_aulas.forEach(function(aula){

    medidas1_fc.push(aula.medidas[0].fc);
    medidas2_fc.push(aula.medidas[1].fc);
    medidas3_fc.push(aula.medidas[2].fc);
    medidas4_fc.push(aula.medidas[3].fc);
    medidas5_fc.push(aula.medidas[4].fc);
    
    medidas_media_fc.push(Math.round((aula.medidas[0].fc + aula.medidas[1].fc + aula.medidas[2].fc + aula.medidas[3].fc + aula.medidas[4].fc)/5))
    
    
    medidas1_spo2.push(aula.medidas[0].spo2);
    medidas2_spo2.push(aula.medidas[1].spo2);
    medidas3_spo2.push(aula.medidas[2].spo2);
    medidas4_spo2.push(aula.medidas[3].spo2);
    medidas5_spo2.push(aula.medidas[4].spo2);
    
    medidas_media_spo2.push(Math.round((aula.medidas[0].spo2 + aula.medidas[1].spo2 + aula.medidas[2].spo2 + aula.medidas[3].spo2 + aula.medidas[4].spo2)/5))
    
    
    medidas1_borg.push(aula.medidas[0].borg);
    medidas2_borg.push(aula.medidas[1].borg);
    medidas3_borg.push(aula.medidas[2].borg);
    medidas4_borg.push(aula.medidas[3].borg);
    medidas5_borg.push(aula.medidas[4].borg);
    
    medidas_media_borg.push(Math.round((aula.medidas[0].borg + aula.medidas[1].borg + aula.medidas[2].borg + aula.medidas[3].borg + aula.medidas[4].borg)/5))
    

});