setTimeout(function() {

    var regressao_medidas1_fc = regressaoArray(medidas1_fc);
    var regressao_medidas2_fc = regressaoArray(medidas2_fc);
    var regressao_medidas3_fc = regressaoArray(medidas3_fc);
    var regressao_medidas4_fc = regressaoArray(medidas4_fc);
    var regressao_medidas5_fc = regressaoArray(medidas5_fc);
    var regressao_medidas_media_fc = regressaoArray(medidas_media_fc);

    new Chart(document.getElementById("chart-fc1"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 1",
                    "data": medidas1_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": "Linha Tendência",
                    "data": regressao_medidas1_fc,
                    "fill": false,
                    "type": "line",
                    "borderColor": "rgb(0, 0, 0)",
                }
            ],

        },
        "options": {
            "responsive" : true,
         },
        "trendlineLinear": {
            style: "rgba(255,105,180, .8)",
            width: 2
        }

    });


    new Chart(document.getElementById("chart-fc2"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 2",
                    "data": medidas2_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas2_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-fc3"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 3",
                    "data": medidas3_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas3_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });

    new Chart(document.getElementById("chart-fc4"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 4",
                    "data": medidas4_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas4_fc,
                    "borderColor": "rgb(0, 0, 0)",
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-fc5"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Medida 5",
                    "data": medidas5_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas5_fc,
                    "borderColor": "rgb(0, 0, 0)",
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });


    new Chart(document.getElementById("chart-fc-media"), {
        "type": "line",
        "data": {
            "labels": datas_aulas,
            "datasets": [{
                    "label": "Média Medidas F.C.",
                    "data": medidas_media_fc,
                    "fill": false,
                    "borderColor": "rgb(0, 0, 0)",
                    "showLine": false
                },
                {
                    "label": 'Linha Tendência',
                    "data": regressao_medidas_media_fc,
                    "borderColor": "rgb(0, 0, 0)",
                    "fill": false,
                    "type": 'line'
                }
            ]
        },
        "options": {}
    });
    
}, 300);