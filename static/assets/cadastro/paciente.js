
/* DATA TABLES DE paciente */
$(document).ready(function() {
       
       var table = $('#paciente-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  { 
                      "width" : "10%",
                  },
                  { 
                      "width" : "15%",
                      "sortable" : false,
                      "order" : false
                  },
                  { 
                      "width" : "5%",
                      "sortable" : false,
                      "order" : false
                  },
                  { 
                      "width" : "5%",
                      "sortable" : false,
                      "order" : false
                  }
           ],
           "initComplete": function () {
                this.api().column(2).every( function () {
                    var column = this;
                    var select = $('<select class="form-control"  style="font-weight: bold;"><option value="" selected style="font-weight: bold;">Todos</option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }

       } );

       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-paciente .modal-content").html("");
        $("#modal-paciente").modal("show");
      },
      success: function (data) {
        $("#modal-paciente .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#paciente-table tbody").html(data.html_paciente_list);
          $("#modal-paciente").modal("hide");
        }
        else {
          $("#modal-paciente .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create paciente
  $(".js-create-paciente").click(loadForm);
  $("#modal-paciente").on("submit", ".js-paciente-create-form", saveForm);

  // Update paciente
  $("#paciente-table").on("click", ".js-update-paciente", loadForm);
  $("#modal-paciente").on("submit", ".js-paciente-update-form", saveForm);

  // Delete paciente
  $("#paciente-table").on("click", ".js-delete-paciente", loadForm);
  $("#modal-paciente").on("submit", ".js-paciente-delete-form", saveForm);

    // Create cliente_servico
  $(".js-create-cliente-servico").click(loadForm);
  $("#modal-paciente").on("submit", ".js-cliente-servico-create-form", saveForm);

    $(".js-create-prospect").click(loadForm);
  $("#modal-paciente").on("submit", ".js-prospect-create-form", saveForm);

});



   
     