
/* DATA TABLES DE hospital */
$(document).ready(function() {
       
       var table = $('#hospital-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      "sortable" : false,
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-hospital .modal-content").html("");
        $("#modal-hospital").modal("show");
      },
      success: function (data) {
        $("#modal-hospital .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#hospital-table tbody").html(data.html_hospital_list);
          $("#modal-hospital").modal("hide");
        }
        else {
          $("#modal-hospital .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create hospital
  $(".js-create-hospital").click(loadForm);
  $("#modal-hospital").on("submit", ".js-hospital-create-form", saveForm);

  // Update hospital
  $("#hospital-table").on("click", ".js-update-hospital", loadForm);
  $("#modal-hospital").on("submit", ".js-hospital-update-form", saveForm);

  // Delete hospital
  $("#hospital-table").on("click", ".js-delete-hospital", loadForm);
  $("#modal-hospital").on("submit", ".js-hospital-delete-form", saveForm);

});



   
     