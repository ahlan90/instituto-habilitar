
/* DATA TABLES DE plano */
$(document).ready(function() {
       
       var table = $('#plano-table').DataTable( {
           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
          "columns": [
                  { 
                  },
                  { 
                      "width" : "5%",
                      "order" : false,
                      
                  },
                  {
                      "width" : "5%",
                      "order" : false,
                  }
          ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-plano .modal-content").html("");
        $("#modal-plano").modal("show");
      },
      success: function (data) {
        $("#modal-plano .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#plano-table tbody").html(data.html_plano_list);
          $("#modal-plano").modal("hide");
        }
        else {
          $("#modal-plano .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create plano
  $(".js-create-plano").click(loadForm);
  $("#modal-plano").on("submit", ".js-plano-create-form", saveForm);

  // Update plano
  $("#plano-table").on("click", ".js-update-plano", loadForm);
  $("#modal-plano").on("submit", ".js-plano-update-form", saveForm);

  // Delete plano
  $("#plano-table").on("click", ".js-delete-plano", loadForm);
  $("#modal-plano").on("submit", ".js-plano-delete-form", saveForm);

});



   
     