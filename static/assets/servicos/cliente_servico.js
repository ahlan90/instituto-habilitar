
/* DATA TABLES DE CONVENIO */
$(document).ready(function() {
       
       var table = $('#cliente-servico-table').DataTable( {

           "language": {
              "sEmptyTable": "Nenhum registro encontrado",
              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
              "sInfoPostFix": "",
              "sInfoThousands": ".",
              "sLengthMenu": "_MENU_ Resultados por página",
              "sLoadingRecords": "Carregando...",
              "sProcessing": "Processando...",
              "sZeroRecords": "Nenhum registro encontrado",
              "sSearch": "Pesquisar: ",
              "oPaginate": {
                  "sNext": "Próximo",
                  "sPrevious": "Anterior",
                  "sFirst": "Primeiro",
                  "sLast": "Último"
              },
              "oAria": {
                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                  "sSortDescending": ": Ordenar colunas de forma descendente"
              }
           },
           "autoWidth": false,
           "columns": [
                  null,
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  },
                  { 
                      "width" : "5%",
                      "orderable": false,
                      "sortable" : false
                  }
           ]
       } );
      
      
       
});


$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-cliente-servico .modal-content").html("");
        $("#modal-cliente-servico").modal("show");
      },
      success: function (data) {
        $("#modal-cliente-servico .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#paciente-table tbody").html(data.html_paciente_list);
          $("#modal-cliente-servico").modal("hide");
        }
        else {
          $("#modal-cliente-servico .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create cliente_servico
  $(".js-create-cliente-servico").click(loadForm);
  $("#modal-cliente-servico").on("submit", ".js-cliente-servico-create-form", saveForm);

  // Update cliente_servico
  $("#cliente-servico-table").on("click", ".js-update-cliente-servico", loadForm);
  $("#modal-cliente-servico").on("submit", ".js-cliente-servico-update-form", saveForm);

  // Delete cliente_servico
  $("#cliente-servico-table").on("click", ".js-delete-cliente-servico", loadForm);
  $("#modal-cliente-servico").on("submit", ".js-cliente-servico-delete-form", saveForm);

});



   
     