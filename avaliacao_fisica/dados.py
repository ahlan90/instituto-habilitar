# -*- coding: utf-8 -*-

mulher_gordura = {
    {
        {'faixa_etaria' : (18,25)},
        {
            {'bf' : (13,16), 'valor' : 'Excelente'},
            {'bf' : (17,19), 'valor' : 'Bom'},
            {'bf' : (20,22), 'valor' : 'Acima da Média'},
            {'bf' : (23,25), 'valor' : 'Média'},
            {'bf' : (26,28), 'valor' : 'Abaixo da Média'},
            {'bf' : (29,31), 'valor' : 'Ruim'},
            {'bf' : (32,43), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (26,35)},
        {
            {'bf' : (14,16), 'valor' : 'Excelente'},
            {'bf' : (17,20), 'valor' : 'Bom'},
            {'bf' : (21,23), 'valor' : 'Acima da Média'},
            {'bf' : (24,26), 'valor' : 'Média'},
            {'bf' : (27,29), 'valor' : 'Abaixo da Média'},
            {'bf' : (30,33), 'valor' : 'Ruim'},
            {'bf' : (36,49), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (36,45)},
        {
            {'bf' : (16,19), 'valor' : 'Excelente'},
            {'bf' : (20,23), 'valor' : 'Bom'},
            {'bf' : (24,26), 'valor' : 'Acima da Média'},
            {'bf' : (27,29), 'valor' : 'Média'},
            {'bf' : (30,32), 'valor' : 'Abaixo da Média'},
            {'bf' : (33,36), 'valor' : 'Ruim'},
            {'bf' : (37,48), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (46,55)},
        {
            { 'bf' : (17,21), 'valor' : 'Excelente'},
            { 'bf' : (22,25), 'valor' : 'Bom'},
            { 'bf' : (26,28), 'valor' : 'Acima da Média'},
            { 'bf' : (29,31), 'valor' : 'Média'},
            { 'bf' : (32,34), 'valor' : 'Abaixo da Média'},
            { 'bf' : (35,38), 'valor' : 'Ruim'},
            { 'bf' : (39,50), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (56,65)},
        {
            {'bf' : (18,22), 'valor' : 'Excelente'},
            {'bf' : (23,26), 'valor' : 'Bom'},
            {'bf' : (17,29), 'valor' : 'Acima da Média'},
            {'bf' : (30,32), 'valor' : 'Média'},
            {'bf' : (33,35), 'valor' : 'Abaixo da Média'},
            {'bf' : (36,38), 'valor' : 'Ruim'},
            {'bf' : (39,49), 'valor' : 'Muito Ruim'}
        }
    }
}


homem_gordura = {
    {
        {'faixa_etaria' : (18,25)},
        {
            {'bf' : (4,6), 'valor' : 'Excelente'},
            {'bf' : (7,10), 'valor' : 'Bom'},
            {'bf' : (11,13), 'valor' : 'Acima da Média'},
            {'bf' : (14,16), 'valor' : 'Média'},
            {'bf' : (17,20), 'valor' : 'Abaixo da Média'},
            {'bf' : (21,24), 'valor' : 'Ruim'},
            {'bf' : (26,36), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (26,35)},
        {
            {'bf' : (8,11), 'valor' : 'Excelente'},
            {'bf' : (12,15), 'valor' : 'Bom'},
            {'bf' : (16,18), 'valor' : 'Acima da Média'},
            {'bf' : (19,21), 'valor' : 'Média'},
            {'bf' : (22,24), 'valor' : 'Abaixo da Média'},
            {'bf' : (25,28), 'valor' : 'Ruim'},
            {'bf' : (29,36), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (36,45)},
        {
            {'bf' : (10,14), 'valor' : 'Excelente'},
            {'bf' : (15,18), 'valor' : 'Bom'},
            {'bf' : (19,21), 'valor' : 'Acima da Média'},
            {'bf' : (22,23), 'valor' : 'Média'},
            {'bf' : (24,26), 'valor' : 'Abaixo da Média'},
            {'bf' : (27,29), 'valor' : 'Ruim'},
            {'bf' : (30,39), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (46,55)},
        {
            { 'bf' : (12,16), 'valor' : 'Excelente'},
            { 'bf' : (17,20), 'valor' : 'Bom'},
            { 'bf' : (21,23), 'valor' : 'Acima da Média'},
            { 'bf' : (24,25), 'valor' : 'Média'},
            { 'bf' : (26,27), 'valor' : 'Abaixo da Média'},
            { 'bf' : (28,30), 'valor' : 'Ruim'},
            { 'bf' : (31,38), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (56,65)},
        {
            {'bf' : (13,19), 'valor' : 'Excelente'},
            {'bf' : (20,22), 'valor' : 'Bom'},
            {'bf' : (23,29), 'valor' : 'Acima da Média'},
            {'bf' : (25,27), 'valor' : 'Média'},
            {'bf' : (28,30), 'valor' : 'Abaixo da Média'},
            {'bf' : (31,32), 'valor' : 'Ruim'},
            {'bf' : (33,38), 'valor' : 'Muito Ruim'}
        }
    }
}






mulher_icq = {
    {
        {'faixa_etaria' : (20,29)},
        {
            {'indice' : (0.83,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.78,0.83), 'valor' : 'Alto'},
            {'indice' : (0.71,0.78), 'valor' : 'Moderado'},
            {'indice' : (0,0.71), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (30,39)},
        {
            {'indice' : (0.84,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.79,0.84), 'valor' : 'Alto'},
            {'indice' : (0.72,0.79), 'valor' : 'Moderado'},
            {'indice' : (0,0.72), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (40,49)},
        {
            {'indice' : (0.87,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.80,0.87), 'valor' : 'Alto'},
            {'indice' : (0.73,0.80), 'valor' : 'Moderado'},
            {'indice' : (0,0.73), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (50,59)},
        {
           {'indice' : (0.88,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.82,0.88), 'valor' : 'Alto'},
            {'indice' : (0.74,0.82), 'valor' : 'Moderado'},
            {'indice' : (0,0.74), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (60,69)},
        {
            {'indice' : (0.9,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.84,0.9), 'valor' : 'Alto'},
            {'indice' : (0.76,0.84), 'valor' : 'Moderado'},
            {'indice' : (0,0.76), 'valor' : 'Baixo'}
        }
    }
}


homem_icq = {
    {
        {'faixa_etaria' : (20,29)},
        {
            {'indice' : (0.94,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.88,0.94), 'valor' : 'Alto'},
            {'indice' : (0.83,0.88), 'valor' : 'Moderado'},
            {'indice' : (0,0.83), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (30,39)},
        {
            {'indice' : (0.96,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.91,0.96), 'valor' : 'Alto'},
            {'indice' : (0.84,0.91), 'valor' : 'Moderado'},
            {'indice' : (0,0.84), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (40,49)},
        {
            {'indice' : (1,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.95,1), 'valor' : 'Alto'},
            {'indice' : (0.88,0.95), 'valor' : 'Moderado'},
            {'indice' : (0,0.88), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (50,59)},
        {
            {'indice' : (1.02,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.97,1.02), 'valor' : 'Alto'},
            {'indice' : (0.9,0.97), 'valor' : 'Moderado'},
            {'indice' : (0,0.9), 'valor' : 'Baixo'}
        }
    },
    {
        {'faixa_etaria' : (60,69)},
        {
            {'indice' : (1.03,3), 'valor' : 'Muito Alto'},
            {'indice' : (0.99,1.03), 'valor' : 'Alto'},
            {'indice' : (0.91,0.99), 'valor' : 'Moderado'},
            {'indice' : (0,0.91), 'valor' : 'Baixo'}
        }
    }
}





imc = {
    { 'valor' : (0,18) , 'classificacao' : 'Magreza' },
    { 'valor' : (18,25) , 'classificacao' : 'Normal' },
    { 'valor' : (25,30) , 'classificacao' : 'Sobrepeso' },
    { 'valor' : (30,35) , 'classificacao' : 'Obesidade Grau 1' },
    { 'valor' : (35,40) , 'classificacao' : 'Obesidade Grau 2' },
    { 'valor' : (40,1000) , 'classificacao' : 'Obesidade Grau 3' }
}


pressao = {
    { 'sistolica' : (0,130) , 'diastolica' : (0,85) , 'classificacao' : 'Normal' },
    { 'sistolica' : (130,139) , 'diastolica' : (85,89) , 'classificacao' : 'Normal - Limítrofe' },
    { 'sistolica' : (140,159) , 'diastolica' : (90,99) , 'classificacao' : 'Hipertensão Leve' },
    { 'sistolica' : (160,179) , 'diastolica' : (100,109) , 'classificacao' : 'Hipertensão Moderada' },
    { 'sistolica' : (180,500) , 'diastolica' : (110,300) , 'classificacao' : 'Hipertensão Grave' },
}


circunferencia_cintura_homem = {
    { 'valor' : (0,94) , 'classificacao' : 'Risco Baixo' },
    { 'valor' : (94,102) , 'classificacao' : 'Risco Elevado' },
    { 'valor' : (102,500) , 'classificacao' : 'Muito Elevado' }
}


circunferencia_cintura_mulher = {
    { 'valor' : (0,80) , 'classificacao' : 'Risco Baixo' },
    { 'valor' : (80,88) , 'classificacao' : 'Risco Elevado' },
    { 'valor' : (88,500) , 'classificacao' : 'Muito Elevado' }
}


referencia_pi_max = {
    { 'valor' : (-120,-75) , 'classificacao' : 'Normal'},
    { 'valor' : (-75,-40) , 'classificacao' : 'Fraqueza Muscular'},
    { 'valor' : (-40,-20) , 'classificacao' : 'Fadiga Muscular'},
    { 'valor' : (-20,0) , 'classificacao' : 'Falência Muscular'}
}



oximetria = {
    { 'valor' : (0,90) , 'classificacao' : 'Muito Baixo' },
    { 'valor' : (90,91) , 'classificacao' : 'Baixo' },
    { 'valor' : (92,94) , 'classificacao' : 'Aceitavel' },
    { 'valor' : (95,99) , 'classificacao' : 'Muito Baixo' }
}



colesterol = {
    { 'valor' : (0,200) , 'classificacao' : 'Desejável' },
    { 'valor' : (201,240) , 'classificacao' : 'Limítrofe' },
    { 'valor' : (240,999) , 'classificacao' : 'Riscos' },
}

triglicerideos = {
    { 'valor' : (0,150) , 'classificacao' : 'Desejável' },
    { 'valor' : (151,200) , 'classificacao' : 'Limítrofe' },
    { 'valor' : (200,999) , 'classificacao' : 'Riscos' },
}

hdl = {
    { 'valor' : (40,999) , 'classificacao' : 'Desejável' },
    { 'valor' : (35,40) , 'classificacao' : 'Limítrofe' },
    { 'valor' : (0,35) , 'classificacao' : 'Riscos' },
}

triglicerideos = {
    { 'valor' : (0,100) , 'classificacao' : 'Desejável' },
    { 'valor' : (101,126) , 'classificacao' : 'Limítrofe' },
    { 'valor' : (126,999) , 'classificacao' : 'Riscos' },
}





homem_dinamometria = {
    {
        {'faixa_etaria' : (50,54)},
        {
            {'bf' : (4,6), 'valor' : 'Excelente'},
            {'bf' : (7,10), 'valor' : 'Bom'},
            {'bf' : (11,13), 'valor' : 'Acima da Média'},
            {'bf' : (14,16), 'valor' : 'Média'},
            {'bf' : (17,20), 'valor' : 'Abaixo da Média'},
            {'bf' : (21,24), 'valor' : 'Ruim'},
            {'bf' : (26,36), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (55,59)},
        {
            {'bf' : (8,11), 'valor' : 'Excelente'},
            {'bf' : (12,15), 'valor' : 'Bom'},
            {'bf' : (16,18), 'valor' : 'Acima da Média'},
            {'bf' : (19,21), 'valor' : 'Média'},
            {'bf' : (22,24), 'valor' : 'Abaixo da Média'},
            {'bf' : (25,28), 'valor' : 'Ruim'},
            {'bf' : (29,36), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (60,64)},
        {
            {'bf' : (10,14), 'valor' : 'Excelente'},
            {'bf' : (15,18), 'valor' : 'Bom'},
            {'bf' : (19,21), 'valor' : 'Acima da Média'},
            {'bf' : (22,23), 'valor' : 'Média'},
            {'bf' : (24,26), 'valor' : 'Abaixo da Média'},
            {'bf' : (27,29), 'valor' : 'Ruim'},
            {'bf' : (30,39), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (65,69)},
        {
            { 'bf' : (12,16), 'valor' : 'Excelente'},
            { 'bf' : (17,20), 'valor' : 'Bom'},
            { 'bf' : (21,23), 'valor' : 'Acima da Média'},
            { 'bf' : (24,25), 'valor' : 'Média'},
            { 'bf' : (26,27), 'valor' : 'Abaixo da Média'},
            { 'bf' : (28,30), 'valor' : 'Ruim'},
            { 'bf' : (31,38), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (70,74)},
        {
            {'bf' : (13,19), 'valor' : 'Excelente'},
            {'bf' : (20,22), 'valor' : 'Bom'},
            {'bf' : (23,29), 'valor' : 'Acima da Média'},
            {'bf' : (25,27), 'valor' : 'Média'},
            {'bf' : (28,30), 'valor' : 'Abaixo da Média'},
            {'bf' : (31,32), 'valor' : 'Ruim'},
            {'bf' : (33,38), 'valor' : 'Muito Ruim'}
        }
    },
    {
        {'faixa_etaria' : (75,200)},
        {
            {'bf' : (13,19), 'valor' : 'Excelente'},
            {'bf' : (20,22), 'valor' : 'Bom'},
            {'bf' : (23,29), 'valor' : 'Acima da Média'},
            {'bf' : (25,27), 'valor' : 'Média'},
            {'bf' : (28,30), 'valor' : 'Abaixo da Média'},
            {'bf' : (31,32), 'valor' : 'Ruim'},
            {'bf' : (33,38), 'valor' : 'Muito Ruim'}
        }
    }
}