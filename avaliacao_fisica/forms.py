# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from .models import AvaliacaoFisica
from django_select2.forms import Select2MultipleWidget, Select2Widget


class AvaliacaoFisicaForm(ModelForm):

    class Meta:
        model = AvaliacaoFisica
        fields = '__all__'
        widgets = {
            
            #Exibir campos, Data Nascimento, CPF, Idade, Raça, Sexo
            'paciente': Select2Widget(attrs={'class': 'form-control textinput textInput'}),
            'data': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),

            'usuario': Select2Widget(attrs={'class': 'form-control textinput textInput'}),
            
            # COMPOSICAO CORPORAL
            
            'peso': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'type': 'number', 'min':20,'max': '300'}),
            'estatura': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'min':90,'max': '300'}),
            'imc': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'percentual_gordura': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'percentual_massa_magra': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'percentual_agua': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'classificacao_gordura': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'classificacao_imc': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            
            
            
            # METABOLISMO
            
            'nivel_atividade_fisica': forms.Select(attrs={'class': 'form-control textinput textInput'}),
            'gasto_energetico': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly' }),
            'metabolismo_basal': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly' }),
            
            
            
            # PERIMETRIA
            
            'pescoco': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'cintura': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pescoco': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'abdominal': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'quadril': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'braco_direito': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'braco_esquerdo': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'braco_contraido_direito': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'braco_contraido_esquerdo': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'coxa_direita': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'coxa_esquerda': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'panturrilha_esquerda': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'panturrilha_direita': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'icq': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'resultado_icq': forms.Select(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'res_circunferencia_cintura': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            
            
            
            
            # FORCA PREENSAO MANUAL
            
            # MD = Mao Dominante    
            'md_medida_1': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'md_medida_2': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'md_medida_3': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'md_media': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'md_resultado': forms.Select(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            
            # MND = Mao Nao Dominante
            'mnd_medida_1': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'mnd_medida_2': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'mnd_medida_3': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'mnd_media': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'mnd_resultado': forms.Select(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            
            
            

            #Manovacuometria

            # PI MAX    
            'pi_max_medida_1': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pi_max_medida_2': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pi_max_medida_3': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pi_max_media': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'pi_max_resultado': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            
            # PE MAX
            'pe_max_medida_1': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pe_max_medida_2': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pe_max_medida_3': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'pe_max_media': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'pe_max_resultado': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            
            
            
            
            # PARAMETROS REPOUSO
            
            'frequencia_cardiaca': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_frequencia_cardiaca': forms.Select(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'pa_sistolica': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_pa_sistolica': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'pa_diastolica': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_pa_diastolica': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'oximetria_pulso': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_oximetria_pulso': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'colesterol': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_colesterol': forms.Select(attrs={'class': 'form-control textinput textInput', 'readonly':'readonly'}),
            'triglicerideos': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_triglicerideos': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'hdl': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_hdl': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            'glicose': forms.NumberInput(attrs={'class': 'form-control textinput textInput'}),
            'av_glicose': forms.Select(attrs={'class': 'form-control textinput textInput','readonly':'readonly' }),
            
            
            
            
            #FREQUENCIA DE TREINO
            
            'fc_maxima': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_basal': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_reserva_inferior_num': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_reserva_superior_num': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_reserva': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_reserva_inferior': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'fc_reserva_superior': forms.NumberInput(attrs={'class': 'form-control textinput textInput','readonly':'readonly'}),
            'intensidade_treino': forms.Select(attrs={'class': 'form-control textinput textInput'}),

 
        }