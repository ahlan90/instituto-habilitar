from django.conf.urls import url

from . import views

urlpatterns = [
    # URLs Avaliacao Clinica
    url(r'^avaliacaofisica/(?P<pk>\d+)$', views.avaliacao_fisica_list, name='avaliacao_fisica_list'),
    url(r'^avaliacaofisica/criar/(?P<pk>\d+)$', views.avaliacao_fisica_create, name='avaliacao_fisica_create'),
    url(r'^avaliacaofisica/atualizar/(?P<pk>\d+)$', views.avaliacao_fisica_update, name='avaliacao_fisica_update'),
    url(r'^avaliacaofisica/deletar/(?P<pk>\d+)$', views.avaliacao_fisica_delete, name='avaliacao_fisica_delete'),
    url(r'^avaliacaofisica/visualizar/(?P<pk>\d+)$', views.avaliacao_fisica_visualizar, name='avaliacao_fisica_visualizar'),
]


