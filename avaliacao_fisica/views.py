# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.http import HttpResponse, request
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import AvaliacaoFisica
from .forms import AvaliacaoFisicaForm
from cadastro.models import Paciente
from cadastro.views import *
from avaliacoes.models import *
# Create your views here.
import json




"""
  VIEWS PARA AVALIACAO CLINICA
"""

@login_required
def avaliacao_fisica_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_fisica = AvaliacaoFisicaForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_fisica': form_fisica,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'fisica',
        'subtab': 'lista-avaliacaofisica',
    }

    return render(request, 'avaliacaofisica/cria_avaliacao_fisica.html', context)
   # return save_avaliacao_fisica_form(request, context, 'avaliacaofisica/cria_avaliacao_fisica.html')


@login_required
def avaliacao_fisica_delete(request, pk):
    avaliacao_fisica = get_object_or_404(AvaliacaoFisica, pk=pk)
    paciente = avaliacao_fisica.paciente
    data = dict()
    if request.method == 'POST':
        avaliacao_fisica.delete()
        data['form_is_valid'] = True
        #paciente_geral(request, avaliacao_fisica.paciente.id, 'fisica', 'formulario-avaliacaofisica', None, form_fisica, None, None, template_name)
        avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
        data['html_avaliacao_fisica_list'] = render_to_string('avaliacaofisica/lista_avaliacao_fisica_parcial.html', {
             'avaliacoes_fisica': avaliacoes_fisica
        })
    else:
        context = {'avaliacao_fisica': avaliacao_fisica}
        data['html_form'] = render_to_string('avaliacaofisica/avaliacao_fisica_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def avaliacao_fisica_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    if request.method == 'POST':
        form_fisica = AvaliacaoFisicaForm(request.POST)
        if form_fisica.is_valid():
            avaliacao_fisica = form_fisica.save(commit=False)
            avaliacao_fisica.paciente = paciente
            avaliacao_fisica.save()
            return avaliacao_fisica_list(request, pk)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_fisica = AvaliacaoFisicaForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_fisica': form_fisica,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'fisica',
        'subtab': 'formulario-avaliacaofisica',
    }

    #return render(request, 'avaliacaofisica/cria_avaliacao_fisica.html', context)
    return save_avaliacao_fisica_form(request, form_fisica, 'avaliacaofisica/cria_avaliacao_fisica.html')

@login_required
def save_avaliacao_fisica_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form_fisica': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
def avaliacao_fisica_update(request, pk):

    avaliacao_fisica = get_object_or_404(AvaliacaoFisica, pk=pk)
    paciente = avaliacao_fisica.paciente

    if request.method == 'POST':
        form_fisica = AvaliacaoFisicaForm(request.POST, instance=avaliacao_fisica)
        if form_fisica.is_valid():
            avaliacao_fisica = form_fisica.save(commit=False)
            avaliacao_fisica.paciente = paciente
            avaliacao_fisica.save()
            return avaliacao_fisica_list(request, paciente.id)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_fisica = AvaliacaoFisicaForm(instance=avaliacao_fisica)

    context = {
        'paciente': paciente,
        'form_fisica': form_fisica,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'fisica',
        'subtab': 'formulario-avaliacaofisica',
        'update': 'True'
    }

    return render(request, 'avaliacaofisica/cria_avaliacao_fisica.html', context)
    #return save_avaliacao_fisica_form(request, form_fisica, 'avaliacaofisica/cria_avaliacao_fisica.html')


@login_required
def avaliacao_fisica_visualizar(request, pk, template_name='avaliacaofisica/visualiza_avaliacao_fisica.html'):
    avaliacao_fisica = get_object_or_404(AvaliacaoFisica, pk=pk)
    form_fisica = AvaliacaoFisicaForm(instance=avaliacao_fisica)
    data = dict()
    context = {'form_fisica': form_fisica}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


