# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from cadastro.models import Paciente
from django.conf import settings
from decimal import Decimal

# Create your models here.

class AvaliacaoFisica(models.Model):
    
    #Exibir campos, Data Nascimento, CPF, Idade, Raça, Sexo
    paciente = models.ForeignKey(Paciente)
    data = models.DateField()
    
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    
    """
    COMPOSICAO CORPORAL
    """
    
    CLASSIFICAO_IMC = (
        ('MAGREZA', 'Magreza'),
        ('NORMAL', 'Normal'),
        ('SOBREPESO', 'Sobrepeso'),
        ('OBESIDADE1', 'Obesidade Grau 1'),
        ('OBESIDADE2', 'Obesidade Grau 2'),
        ('OBESIDADE3', 'Obesidade Grau 3'),
    )
    
    peso = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    estatura = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    imc = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    percentual_gordura = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    percentual_massa_magra = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    percentual_agua = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    
    CLASSIFICAO_GORDURA = (
        ('EXCELENTE', 'Excelente'),
        ('BOM', 'Bom'),
        ('ACIMAMEDIA', 'Acima da Média'),
        ('MEDIA', 'Média'),
        ('ABAIXOMEDIA', 'Abaixo da Média'),
        ('RUIM', 'Ruim'),
        ('MUITORUIM', 'Muito Ruim'),
    )
    classificacao_gordura = models.CharField(max_length=30, choices=CLASSIFICAO_GORDURA, blank=True, null=True)
    classificacao_imc = models.CharField(max_length=30, choices=CLASSIFICAO_IMC, blank=True, null=True)
    
    
    
    """
    METABOLISMO
    """
    
    NIVEL_ATIVIDADE = (
        ('SEDENTARIO', 'Sedentário'),
        ('POUCOATIVO', 'Pouco Ativo'),
        ('ATIVO', 'Ativo'),
        ('MUITOATIVO', 'Muito Ativo'),
        ('ATLETA', 'Atleta'),
    )
    
    nivel_atividade_fisica = models.CharField(max_length=100, choices=NIVEL_ATIVIDADE, blank=True, null=True)
    gasto_energetico = models.IntegerField(blank=True, null=True)
    metabolismo_basal = models.IntegerField(blank=True, null=True)
    
    
    
    """
    PERIMETRIA
    """
    
    
    pescoco = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    cintura = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    abdominal = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    quadril = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    braco_direito = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    braco_esquerdo = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    braco_contraido_direito = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    braco_contraido_esquerdo = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    coxa_direita = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    coxa_esquerda = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    panturrilha_esquerda = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    panturrilha_direita = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    icq = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    
    AV_ICQ = (
        ('BAIXO', 'Risco Baixo'),
        ('MODERADO', 'Risco Moderado'),
        ('ALTO', 'Risco Alto'),
        ('MUITO ALTO', 'Risco Muito Alto'),
    )
    resultado_icq = models.CharField(max_length=100, choices=AV_ICQ, blank=True, null=True)
    
    AV_CINTURA = (
        ('BAIXO', 'Risco Baixo'),
        ('ELEVADO', 'Risco Elevado'),
        ('MUITO ELEVADO', 'Risco Muito Elevado'),
    )
    res_circunferencia_cintura = models.CharField(max_length=100, choices=AV_CINTURA, blank=True, null=True)
    
    
    
    """
    FORCA PREENSAO MANUAL
    """
    
    AV_PREENSAO = (
        ('REGULAR', 'Regular'),
        ('BOM', 'Bom'),
        ('OTIMO', 'Ótimo'),
    )
    
    # MD = Mao Dominante    
    md_medida_1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    md_medida_2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    md_medida_3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    md_media = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    md_resultado = models.CharField(max_length=30, choices=AV_PREENSAO, blank=True, null=True)
    
    # MND = Mao Nao Dominante
    mnd_medida_1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    mnd_medida_2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    mnd_medida_3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    mnd_media = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    mnd_resultado = models.CharField(max_length=30, choices=AV_PREENSAO, blank=True, null=True)
    
    
    
    """
    Manovacuometria
    """
    # PI MAX    
    pi_max_medida_1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pi_max_medida_2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pi_max_medida_3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pi_max_media = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    
    PI_MAXIMO = (
        ('NORMAL', 'Normal'),
        ('FRAQUEZA', 'Fraqueza Muscular'),
        ('FADIGA', 'Fadiga Muscular'),
        ('FALENCIA', 'Falência Muscular'),
    )
    pi_max_resultado = models.CharField(max_length=100, choices=PI_MAXIMO, blank=True, null=True)
    
    
    
    # PE MAX
    pe_max_medida_1 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pe_max_medida_2 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pe_max_medida_3 = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    pe_max_media = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    
    PE_MAXIMO = (
        ('NORMAL', 'Normal'),
        ('INSUFICIENTE', 'Insuficiente'),
    )
    pe_max_resultado = models.CharField(max_length=100, choices=PE_MAXIMO, blank=True, null=True)
    
    
    
    
    """
    PARAMETROS REPOUSO
    """
    
    AV_FC = (
        ('BRADICARDIA', 'Bradicardia'),
        ('NORMAL', 'Normal'),
        ('NORMALALTA', 'Normal Alta'),
        ('TAQUICARDIA', 'Taquicardia'),
    )
    frequencia_cardiaca = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_frequencia_cardiaca = models.CharField(max_length=50, choices=AV_FC, blank=True, null=True)
    
  
    AV_PRESSAO = (
        ('NORMAL', 'Normal'),
        ('LIMITROFE', 'Normal - Limítrofe'),
        ('LEVE', 'Hipertensão Leve'),
        ('MODERADA', 'Hipertensão Moderada'),
        ('GRAVE', 'Hipertensão Grave'),
    )
    pa_sistolica = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_pa_sistolica = models.CharField(max_length=50, choices=AV_PRESSAO, blank=True, null=True)
    pa_diastolica = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_pa_diastolica = models.CharField(max_length=50, choices=AV_PRESSAO, blank=True, null=True)
    
    
    AV_OXIMETRIA = (
        ('NORMAL', 'Normal'),
        ('ACEITAVEL', 'Aceitável'),
        ('BAIXO', 'Baixo'),
        ('MUITO BAIXO', 'Muito Baixo'),
    )
    oximetria_pulso = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_oximetria_pulso = models.CharField(max_length=50, choices=AV_OXIMETRIA, blank=True, null=True)
    

    AV_EXAMES = (
        ('DESEJAVEL', 'Desejável'),
        ('LIMITROFE', 'Limitrofe'),
        ('RISCOS', 'Riscos'),
    )
    colesterol = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_colesterol = models.CharField(max_length=50, choices=AV_EXAMES, blank=True, null=True)
    triglicerideos = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_triglicerideos = models.CharField(max_length=50, choices=AV_EXAMES, blank=True, null=True)
    hdl = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_hdl = models.CharField(max_length=50, choices=AV_EXAMES, blank=True, null=True)
    glicose = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    av_glicose = models.CharField(max_length=50, choices=AV_EXAMES, blank=True, null=True)
    
    
    """
    FREQUENCIA DE TREINO
    """
    
    INTENSIDADE = (
        ('MUITOBAIXA', 'Muito Baixa'),
        ('BAIXA', 'Baixa'),
        ('MODERADA', 'Moderada'),
        ('MODERADAALTA', 'Moderada Alta'),
        ('ALTA', 'Alta'),
        ('MUITOALTA', 'Muito Alta'),
    )
    
    fc_maxima = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    fc_basal = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    fc_reserva = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    intensidade_treino = models.CharField(max_length=50, choices=INTENSIDADE, blank=True, null=True)
    fc_reserva_inferior = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    fc_reserva_superior = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    fc_reserva_inferior_num = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    fc_reserva_superior_num = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)