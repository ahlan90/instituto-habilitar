# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-04-12 20:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cadastro', '0011_paciente_cliente_servico'),
        ('relatorios', '0003_templatelaudo'),
    ]

    operations = [
        migrations.AddField(
            model_name='laudo',
            name='paciente',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Paciente'),
            preserve_default=False,
        ),
    ]
