from django import forms
from django.forms import ModelForm
from .models import *
from ckeditor.widgets import CKEditorWidget
#from django_select2.forms import Select2MultipleWidget, Select2Widget

class RelatorioTesteCaminhadaForm(ModelForm):
    
    class Meta:
        model = RelatorioTesteCaminhada
        fields = ['texto']


class TemplateLaudoForm(ModelForm):
    class Meta:
        model = TemplateLaudo
        fields = ['texto']


class NovoLaudoForm(ModelForm):
    class Meta:
        model = Laudo
        fields = ['texto','paciente']
        widgets = {
            'paciente': forms.HiddenInput()
        }