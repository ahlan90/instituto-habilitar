# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField
from cadastro.models import Paciente

class RelatorioTesteCaminhada(models.Model):

    texto = RichTextField(null=True, blank=True)


class TemplateLaudo(models.Model):

    texto = RichTextField(null=True, blank=True)


class Laudo(models.Model):

    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    texto = RichTextField(null=True, blank=True)