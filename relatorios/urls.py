from django.conf.urls import url
from . import views

urlpatterns = [
     
     # URLs Avaliacao Clinica
    url(r'^relatorio/menu$', views.menu_relatorios, name='menu_relatorios'),
    url(r'^relatorio/teste_caminhada/editar/(?P<pk>\d+)$', views.template_teste_caminhada, name='template_teste_caminhada'),
    url(r'^relatorio/teste_caminhada/visualizar/(?P<pk>\d+)$', views.view_teste_caminhada_rel, name='view_teste_caminhada_rel'),
    
    url(r'^laudo/(?P<pk>\d+)/(?P<laudo>\d+)$', views.laudo_geral_print, name='laudo_geral_print'),
    url(r'^laudo/gerar/(?P<pk>\d+)$', views.gerar_laudo, name='gerar_laudo'),
    url(r'^laudo/editar/(?P<pk>\d+)$', views.editar_laudo, name='editar_laudo'),
    url(r'^laudo/template$', views.template_laudo, name='template_laudo'),

    url(r'^hello$', views.helloPDF, name='hello_pdf')

]



