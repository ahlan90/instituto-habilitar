from easy_pdf.rendering import render_to_pdf_response
from easy_pdf.views import PDFTemplateView
from aula.models import Medidas, ControleAula
from avaliacao_fisica.models import AvaliacaoFisica
from cadastro.models import Paciente
from avaliacoes.models import TesteCaminhada6, PreReavaliacao, AvaliacaoClinica, AvaliacaoPostural
from avaliacoes.forms import TesteCaminhada6Form, AvaliacaoClinicaForm, TesteErgometricoForm, TesteCardiopulmonarForm, \
    ExamesBioquimicosForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from .forms import *
import time


# Create your views here.
@login_required
def menu_relatorios(request, template_name='relatorios/menu_pacientes_relatorio.html'):

    return render(request, template_name)


@login_required
def view_teste_caminhada_rel(request, pk, template_name='relatorio/relatorio_teste_caminhada.html'):

    teste_caminhada = get_object_or_404(TesteCaminhada6, pk=pk)
    
    form_caminhada6 = TesteCaminhada6Form(instance=teste_caminhada)
    
    relatorio = RelatorioTesteCaminhada.objects.last()
    
    texto = replace_teste_caminhada(relatorio.texto, teste_caminhada)

    data = {}
    data['teste_caminhada'] = teste_caminhada
    data['form_caminhada6'] = form_caminhada6
    data['texto'] = texto
    
    return render(request, template_name, data)
    

@login_required
def template_teste_caminhada(request, pk, template_name='relatorio/form_teste_caminhada.html'):

    teste_caminhada = get_object_or_404(TesteCaminhada6, pk=pk)
    
    form_recuperado = None

    try:
        form_recuperado = RelatorioTesteCaminhada.objects.last()
    except:
        pass

    if form_recuperado == None:
        if request.method == 'POST':
            form = RelatorioTesteCaminhadaForm(request.POST or None)
            if form.is_valid():
                form.save()
                return redirect('view_teste_caminhada_rel', pk=pk)
        else:
            form = RelatorioTesteCaminhadaForm()
    else:
        if request.method == 'POST':
            form = RelatorioTesteCaminhadaForm(request.POST or None, instance=form_recuperado)
            if form.is_valid():
                form.save()
                return redirect('view_teste_caminhada_rel', pk=pk)
        else:
            form = RelatorioTesteCaminhadaForm(instance=form_recuperado)

    data = {}
    data['teste_caminhada'] = teste_caminhada
    data['form'] = form
    
    return render(request, template_name, data)


def replace_teste_caminhada(texto, teste_caminhada):
    
    retorno = texto.replace("##data##", teste_caminhada.data.strftime("%d/%m/%Y"))
    retorno = retorno.replace("##voltas_total##", str(teste_caminhada.total_voltas))
    retorno = retorno.replace("##paciente##", str(teste_caminhada.paciente))
    retorno = retorno.replace("##distancia_percorrida##", str(teste_caminhada.distancia_percorrida))
    
    return retorno



@login_required
def laudo_geral_print(request, pk, template_name='laudo/laudo_print.html'):

    paciente = get_object_or_404(Paciente, pk=pk)

    laudo = Laudo.objects.last()

    if not laudo:
        return redirect('template_laudo', pk=pk)

    aulas = ControleAula.objects.filter(paciente=paciente).order_by('data')

    datas_aulas = []

    medidas_aulas = []

    medidas_inicial_diastolica = []
    medidas_inicial_sistolica = []

    medidas_final_diastolica = []
    medidas_final_sistolica = []

    start_time = time.time()

    for aula in aulas:

        # Criando array com as datas

        data_aula = str(aula.data.strftime('%d/%m/%y'))
        datas_aulas.append(data_aula)

        if (aula.pa_inicial != None or aula.pa_inicial != ""):
            if (len(aula.pa_inicial.split('x')) == 2):
                medidas_inicial_sistolica.append(aula.pa_inicial.split('x')[0])
                medidas_inicial_diastolica.append(aula.pa_inicial.split('x')[1])

        if (aula.pa_final != None or aula.pa_final != ""):
            if (len(aula.pa_final.split('x')) == 2):
                medidas_final_sistolica.append(aula.pa_final.split('x')[0])
                medidas_final_diastolica.append(aula.pa_final.split('x')[1])

        for index, elem in enumerate(datas_aulas):
            if index != 0 and index != len(datas_aulas) - 1:
                datas_aulas[index] = ''

        # Recuperando as medidas
        medidas = Medidas.objects.filter(controle_aula=aula)

        medida_retorno = {}

        medida_retorno['data'] = data_aula

        med = []
        for medida in medidas:
            med.append(medida.toJSON())

        medida_retorno['medidas'] = med

        medidas_aulas.append(medida_retorno)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'datas_aulas': json.dumps(datas_aulas),
        'medidas_aulas': json.dumps(medidas_aulas),
        'medidas_inicial_sistolica': json.dumps(medidas_inicial_sistolica),
        'medidas_inicial_diastolica': json.dumps(medidas_inicial_diastolica),
        'medidas_final_sistolica': json.dumps(medidas_final_sistolica),
        'medidas_final_diastolica': json.dumps(medidas_final_diastolica),
        'texto': laudo.texto,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
    }

    return render(request, template_name, context)


@login_required
def template_laudo(request, pk, template_name='laudo/laudo_form.html'):

   #laudo = get_object_or_404(Laudo, pk=pk)

    form_recuperado = None

    try:
        form_recuperado = Laudo.objects.last()
    except:
        pass

    if form_recuperado == None:
        if request.method == 'POST':
            form = NovoLaudoForm(request.POST or None)
            if form.is_valid():
                form.save()
                return redirect('laudo_geral_print', pk=pk)
        else:
            form = NovoLaudoForm()
    else:
        if request.method == 'POST':
            form = NovoLaudoForm(request.POST or None, instance=form_recuperado)
            if form.is_valid():
                form.save()
                return redirect('laudo_geral_print', pk=pk)
        else:
            form = NovoLaudoForm(instance=form_recuperado)

    data = {}
    #data['laudo'] = laudo
    data['form'] = form

    return render(request, template_name, data)


@login_required
def laudo_print(request, pk, template_name='relatorio/relatorio_teste_caminhada.html'):
    teste_caminhada = get_object_or_404(TesteCaminhada6, pk=pk)

    form_caminhada6 = TesteCaminhada6Form(instance=teste_caminhada)

    relatorio = RelatorioTesteCaminhada.objects.last()

    texto = replace_teste_caminhada(relatorio.texto, teste_caminhada)

    data = {}
    data['teste_caminhada'] = teste_caminhada
    data['form_caminhada6'] = form_caminhada6
    data['texto'] = texto

    return render(request, template_name, data)



def helloPDF(request):

    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=6)
    paciente = avaliacao_clinica.paciente

    form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
    form_ergometrico = TesteErgometricoForm(instance=avaliacao_clinica.teste_ergometrico)
    form_cardiopulmonar = TesteCardiopulmonarForm(instance=avaliacao_clinica.teste_cardiopulmonar)
    form_bioquimicos = ExamesBioquimicosForm(instance=avaliacao_clinica.bioquimicos)

    data = {
        'paciente': paciente,
        'form_clinica': form_clinica,
        'form_ergometrico': form_ergometrico,
        'form_cardiopulmonar': form_cardiopulmonar,
        'form_bioquimicos': form_bioquimicos
    }

    return render_to_pdf_response(request, 'print/avaliacao_clinica_print.html', data)

# class HelloPDFView(PDFTemplateView):
#
#
#     avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=229)
#     paciente = avaliacao_clinica.paciente
#
#     form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
#     form_ergometrico = TesteErgometricoForm(instance=avaliacao_clinica.teste_ergometrico)
#     form_cardiopulmonar = TesteCardiopulmonarForm(instance=avaliacao_clinica.teste_cardiopulmonar)
#     form_bioquimicos = ExamesBioquimicosForm(instance=avaliacao_clinica.bioquimicos)
#
#     context = { 'paciente' : paciente }
#
#     template_name = 'print/avaliacao_clinica_print.html'