from django.contrib import admin
from .models import Plano, MedicoAssistente, Paciente, ConvenioMedico, Hospital

# Register your models here.
admin.site.register(Plano)
admin.site.register(MedicoAssistente)
admin.site.register(Paciente)
admin.site.register(ConvenioMedico)
admin.site.register(Hospital)