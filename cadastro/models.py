# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from decimal import Decimal
from datetime import date

class ParametrosGerencial(models.Model):
    
    dias_aviso_contrato = models.IntegerField(blank=True, null=True)
    dias_aviso_avaliacao_fisica = models.IntegerField(blank=True, null=True)
    dias_aviso_avaliacao_clinica = models.IntegerField(blank=True, null=True)
    dias_aviso_exames = models.IntegerField(blank=True, null=True)
    dias_aviso_falta = models.IntegerField(blank=True, null=True)
    tamanho_volta_teste_caminhada = models.DecimalField(max_digits=5, decimal_places=2,null=True,blank=True, default=Decimal('0.00'))


class Plano(models.Model):

    nome = models.CharField(max_length=200, unique=True, error_messages={'unique':"Já existe um Plano com este nome cadastrado."})

    frequencia = models.IntegerField("Frequência",blank=True, null=True)
    periodo_total = models.IntegerField("Período total")
    suspensao = models.IntegerField("Suspensão de até")
    periodo_avaliacao_fisica = models.IntegerField("Av. Física(Periodicidade)")
    periodo_avaliacao_clinica = models.IntegerField("Av. Clínica(Periodicidade)")
    
    def __str__(self):
        return self.nome


class Empresa(models.Model):

    nome = models.CharField(max_length=200, unique=True, error_messages={'unique':"Já existe uma Empresa com este nome cadastrado."})
    
    def __str__(self):
        return self.nome


class Hospital(models.Model):

    nome = models.CharField(max_length=200, unique=True, error_messages={'unique':"Já existe um Hospital com este nome cadastrado."})
    
    def __str__(self):
        return self.nome


class ConvenioMedico(models.Model):

    nome = models.CharField(max_length=200, unique=True, error_messages={'unique':"Já existe um Convênio com este nome cadastrado."})
    
    def __str__(self):
        return self.nome


class MedicoAssistente(models.Model):
    
    nome = models.CharField(max_length=200, unique=True, error_messages={'unique':"Já existe um Médico com este nome cadastrado."})
    telefone = models.CharField(max_length=200)
    email = models.EmailField(blank=True)

    def __str__(self):
        return self.nome

    class Meta:
        ordering = ('nome',)


class Professor(models.Model):
    
    nome = models.CharField(max_length=200)
    telefone = models.CharField(max_length=200)
    email = models.EmailField(blank=True)
    
    def __str__(self):
        return self.nome

    class Meta:
        ordering = ('nome',)
    

class Paciente(models.Model):

    nome = models.CharField(max_length=200)
    telefone = models.CharField(max_length=200)
    data_nascimento = models.DateTimeField(blank=True, null=True)
    cpf = models.CharField(max_length=20)
    rg = models.CharField(max_length=20,blank=True, null=True)
    plano = models.ManyToManyField(Plano, blank=True)
    hospital = models.ManyToManyField(Hospital, blank=True)
    telefone_emergencia = models.CharField(max_length=200)
    convenio_medico = models.ManyToManyField(ConvenioMedico)
    medico_assistente = models.ManyToManyField(MedicoAssistente)
    
    matricula = models.CharField(max_length=20, blank=True, null=True)
    #data_vencimento = models.DateTimeField(blank=True, null=True)
    data_inicial_contrato = models.DateTimeField(blank=True, null=True)
    data_final_contrato = models.DateTimeField(blank=True, null=True)
    
    OPCAO_RACIAL = (
        ('ASIATICO', 'Asiatico'),
        ('BRANCO', 'Branco'),
        ('AFROAMERICANO', 'Afro-Americano'),
    )
    
    raca = models.CharField(max_length=70, choices=OPCAO_RACIAL)
    
    OPCAO_SEXO = (
        ('M', 'Masculino'),
        ('F', 'Feminino'),
    )
    
    sexo = models.CharField(max_length=25, choices=OPCAO_SEXO)
    
    ativo = models.BooleanField(default=True)
    prospect = models.BooleanField(default=True)
    cliente_servico = models.BooleanField("Cliente de serviços", default=False)
    
    def __str__(self):
        return self.nome
    
    def idade(self):
        today = date.today()
        return today.year - self.data_nascimento.year - ((today.month, today.day) < (self.data_nascimento.month, self.data_nascimento.day))

    class Meta:
        ordering = ['nome']