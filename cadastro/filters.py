from .models import Paciente
import django_filters


class PacienteFilter(django_filters.FilterSet):
    
    nome = django_filters.CharFilter(lookup_expr='icontains')
    
    class Meta:
        model = Paciente
        fields = ['nome']