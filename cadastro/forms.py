from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from .models import *
from django_select2.forms import Select2MultipleWidget, Select2Widget



#Login#
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username', 'placeholder': 'Usuario'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password', 'placeholder':'Senha'}))


class ParametrosGerencialForm(ModelForm):
    
    class Meta:
        model = ParametrosGerencial
        fields = '__all__'


class MedicoAssistenteForm(ModelForm):
    
    class Meta:
        model = MedicoAssistente
        fields = '__all__'


class ProfessorForm(ModelForm):
    
    class Meta:
        model = Professor
        fields = '__all__'


class PacienteForm(ModelForm):
    
    nome = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    telefone = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    telefone_emergencia = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    rg = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    matricula = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    cpf = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    ativo = forms.BooleanField(required=False, initial=True, widget=forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}))
    
    plano = forms.ModelMultipleChoiceField(queryset=Plano.objects.all(), widget=Select2MultipleWidget, required=False)
    hospital = forms.ModelMultipleChoiceField(queryset=Hospital.objects.all(), widget=Select2MultipleWidget)
    convenio_medico = forms.ModelMultipleChoiceField(queryset=ConvenioMedico.objects.all(), widget=Select2MultipleWidget)
    medico_assistente = forms.ModelMultipleChoiceField(queryset=MedicoAssistente.objects.all(), widget=Select2MultipleWidget)
    
    class Meta:
        model = Paciente
        fields = '__all__'
        widgets = {
            'ativo': forms.CheckboxInput(attrs={'class' : 'filled-in chk-col-orange'}),
            'data_nascimento': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
            'data_vencimento': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
            'data_inicial_contrato': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
            'data_final_contrato': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
        }


class ProspectForm(ModelForm):
    
    medico_assistente = forms.ModelMultipleChoiceField(queryset=MedicoAssistente.objects.all(), widget=Select2MultipleWidget(attrs={'class' : 'form-control'}))
    
    class Meta:
        model = Paciente
        fields = ['nome','telefone','medico_assistente']


class PlanoForm(ModelForm):
    
    class Meta:
        model = Plano
        fields = '__all__'


class EmpresaForm(ModelForm):
    
    class Meta:
        model = Empresa
        fields = '__all__'


class ConvenioMedicoForm(ModelForm):
    
    class Meta:
        model = ConvenioMedico
        fields = '__all__'


class HospitalForm(ModelForm):
    
    class Meta:
        model = Hospital
        fields = '__all__'