from django.shortcuts import render
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.core import serializers
from django.http import HttpResponse, request
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .filters import PacienteFilter
from avaliacao_fisica.models import *
from avaliacao_fisica.forms import *
from cadastro.utils import calculate_age
from django.http import HttpResponseRedirect
import logging
from notificacoes.models import *
import datetime

"""

     VIEWS PARA GERAIS

"""

@login_required
def home(request):
    
    usuario = request.user
    
    return paciente_list_card(request,'paciente/lista_paciente_card.html')


@login_required
def dashboard(request, template_name='dashboard/dashboard.html'):
    
    parametro = ParametrosGerencial.objects.filter().first()

    
    """ Lista das solucoes ja cadastradas ao problema """
    pacientes_ativos = Paciente.objects.filter(ativo=True);
    prospects = Paciente.objects.filter(prospect=True);
    avaliacoes_fisica = AvaliacaoFisica.objects.all();
    avaliacoes_clinica = AvaliacaoClinica.objects.all();
    prereavaliacoes = PreReavaliacao.objects.all();
    posturais = AvaliacaoPostural.objects.all();
    #notificacoes = NotificacaoAvaliacao.objects.exclude(data_aviso__gte=datetime.datetime.now());
    #data_notificacao = data_inicial - datetime.timedelta(days=dias_aviso)
    notificacoes = NotificacaoAvaliacao.objects.filter(descricao_modelo='AVC',data_agendamento__lte=datetime.datetime.now() +  datetime.timedelta(days=parametro.dias_aviso_avaliacao_clinica))


    data = {}
    data['pacientes_ativos'] = len(pacientes_ativos)
    data['prospects'] = len(prospects)
    data['avaliacoes_fisica'] = len(avaliacoes_fisica)
    data['avaliacoes_clinica'] = len(avaliacoes_clinica)
    data['prereavaliacoes'] = len(prereavaliacoes)
    data['posturais'] = len(posturais)
    data['notificacoes'] = notificacoes
    
    
    return render(request,template_name, data)


def logout_view(request):
    logout(request)
    return render(request, template_name='index.html')


"""

     VIEWS PARA CONVENIO

"""

@login_required
def convenio_list(request, template_name='convenio/lista_convenio.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    convenios = ConvenioMedico.objects.all();

    data = {}
    data['convenios'] = convenios
    
    return render(request, template_name, data)


@login_required
def convenio_delete(request, pk):
    convenio = get_object_or_404(ConvenioMedico, pk=pk)
    data = dict()
    if request.method == 'POST':
        convenio.delete()
        data['form_is_valid'] = True
        convenios = ConvenioMedico.objects.all()
        data['html_convenio_list'] = render_to_string('convenio/lista_convenio_parcial.html', {
            'convenios': convenios
        })
    else:
        context = {'convenio': convenio}
        data['html_form'] = render_to_string('convenio/convenio_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_convenio_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            convenios = ConvenioMedico.objects.all()
            data['html_convenio_list'] = render_to_string('convenio/lista_convenio_parcial.html', {
                'convenios': convenios
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def convenio_create(request):
    if request.method == 'POST':
        form = ConvenioMedicoForm(request.POST)
    else:
        form = ConvenioMedicoForm()
    return save_convenio_form(request, form, 'convenio/cria_convenio_modal.html')


@login_required
def convenio_update(request, pk):
    convenio = get_object_or_404(ConvenioMedico, pk=pk)
    if request.method == 'POST':
        form = ConvenioMedicoForm(request.POST, instance=convenio)
    else:
        form = ConvenioMedicoForm(instance=convenio)
    return save_convenio_form(request, form, 'convenio/atualiza_convenio_modal.html')




"""

     VIEWS PARA EMPRESA

"""

@login_required
def empresa_list(request, template_name='empresa/lista_empresa.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    empresas = Empresa.objects.all();

    data = {}
    data['empresas'] = empresas
    
    return render(request, template_name, data)


@login_required
def empresa_delete(request, pk):
    empresa = get_object_or_404(Empresa, pk=pk)
    data = dict()
    if request.method == 'POST':
        empresa.delete()
        data['form_is_valid'] = True
        empresas = Empresa.objects.all()
        data['html_empresa_list'] = render_to_string('empresa/lista_empresa_parcial.html', {
            'empresas': empresas
        })
    else:
        context = {'empresa': empresa}
        data['html_form'] = render_to_string('empresa/empresa_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_empresa_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            empresas = Empresa.objects.all()
            data['html_empresa_list'] = render_to_string('empresa/lista_empresa_parcial.html', {
                'empresas': empresas
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def empresa_create(request):
    if request.method == 'POST':
        form = EmpresaForm(request.POST)
    else:
        form = EmpresaForm()
    return save_empresa_form(request, form, 'empresa/cria_empresa_modal.html')


@login_required
def empresa_update(request, pk):
    empresa = get_object_or_404(Empresa, pk=pk)
    if request.method == 'POST':
        form = EmpresaForm(request.POST, instance=empresa)
    else:
        form = EmpresaForm(instance=empresa)
    return save_empresa_form(request, form, 'empresa/atualiza_empresa_modal.html')





"""

     VIEWS PARA MEDICO

"""

@login_required
def medico_list(request, template_name='medico/lista_medico.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    medicos = MedicoAssistente.objects.all();

    data = {}
    data['medicos'] = medicos
    
    return render(request, template_name, data)


@login_required
def medico_delete(request, pk):
    medico = get_object_or_404(MedicoAssistente, pk=pk)
    data = dict()
    if request.method == 'POST':
        medico.delete()
        data['form_is_valid'] = True
        medicos = MedicoAssistente.objects.all()
        data['html_medico_list'] = render_to_string('medico/lista_medico_parcial.html', {
            'medicos': medicos
        })
    else:
        context = {'medico': medico}
        data['html_form'] = render_to_string('medico/medico_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_medico_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            medicos = MedicoAssistente.objects.all()
            data['html_medico_list'] = render_to_string('medico/lista_medico_parcial.html', {
                'medicos': medicos
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def medico_create(request):
    if request.method == 'POST':
        form = MedicoAssistenteForm(request.POST)
    else:
        form = MedicoAssistenteForm()
    return save_medico_form(request, form, 'medico/cria_medico_modal.html')


@login_required
def medico_update(request, pk):
    medico = get_object_or_404(MedicoAssistente, pk=pk)
    if request.method == 'POST':
        form = MedicoAssistenteForm(request.POST, instance=medico)
    else:
        form = MedicoAssistenteForm(instance=medico)
    return save_medico_form(request, form, 'medico/atualiza_medico_modal.html')




"""

     VIEWS PARA PROFESSOR

"""

@login_required
def professor_list(request, template_name='professor/lista_professor.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    professors = Professor.objects.all();

    data = {}
    data['professors'] = professors
    
    return render(request, template_name, data)


@login_required
def professor_delete(request, pk):
    professor = get_object_or_404(Professor, pk=pk)
    data = dict()
    if request.method == 'POST':
        professor.delete()
        data['form_is_valid'] = True
        professors = Professor.objects.all()
        data['html_professor_list'] = render_to_string('professor/lista_professor_parcial.html', {
            'professors': professors
        })
    else:
        context = {'professor': professor}
        data['html_form'] = render_to_string('professor/professor_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_professor_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            professors = Professor.objects.all()
            data['html_professor_list'] = render_to_string('professor/lista_professor_parcial.html', {
                'professors': professors
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def professor_create(request):
    if request.method == 'POST':
        form = ProfessorForm(request.POST)
    else:
        form = ProfessorForm()
    return save_professor_form(request, form, 'professor/cria_professor_modal.html')


@login_required
def professor_update(request, pk):
    professor = get_object_or_404(Professor, pk=pk)
    if request.method == 'POST':
        form = ProfessorForm(request.POST, instance=professor)
    else:
        form = ProfessorForm(instance=professor)
    return save_professor_form(request, form, 'professor/atualiza_professor_modal.html')









"""

     VIEWS PARA PLANO

"""

@login_required
def plano_list(request, template_name='plano/lista_plano.html'):

    print('Entrou aqui')
    """ Lista das solucoes ja cadastradas ao problema """
    planos = Plano.objects.all();
    
    return render(request, template_name, {'planos' : planos})


@login_required
def plano_delete(request, pk):
    plano = get_object_or_404(Plano, pk=pk)
    data = dict()
    if request.method == 'POST':
        plano.delete()
        data['form_is_valid'] = True
        planos = Plano.objects.all()
        data['html_plano_list'] = render_to_string('plano/lista_plano_parcial.html', {
            'planos': planos
        })
    else:
        context = {'plano': plano}
        data['html_form'] = render_to_string('plano/plano_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_plano_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            planos = Plano.objects.all()
            data['html_plano_list'] = render_to_string('plano/lista_plano_parcial.html', {
                'planos': planos
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def plano_create(request):
    if request.method == 'POST':
        form = PlanoForm(request.POST)
    else:
        form = PlanoForm()
    return save_plano_form(request, form, 'plano/cria_plano_modal.html')


@login_required
def plano_update(request, pk):
    plano = get_object_or_404(Plano, pk=pk)
    if request.method == 'POST':
        form = PlanoForm(request.POST, instance=plano)
    else:
        form = PlanoForm(instance=plano)
    return save_plano_form(request, form, 'plano/atualiza_plano_modal.html')




"""

     VIEWS PARA PROSPECT

"""

@login_required
def prospect_list(request, template_name='prospect/lista_prospect.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    prospects = Paciente.objects.filter(prospect=True);

    data = {}
    data['prospects'] = prospects
    
    return render(request, template_name, data)


@login_required
def prospect_delete(request, pk):
    prospect = get_object_or_404(Paciente, pk=pk)
    data = dict()
    if request.method == 'POST':
        prospect.delete()
        data['form_is_valid'] = True
        prospects = Paciente.objects.all()
        data['html_prospect_list'] = render_to_string('prospect/lista_prospect_parcial.html', {
            'prospects': prospects
        })
    else:
        context = {'prospect': prospect}
        data['html_form'] = render_to_string('prospect/prospect_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def prospect_create(request, template_name='prospect/cria_prospect_form.html'):
    
    if request.method == 'POST':
        form = ProspectForm(request.POST)
        if form.is_valid():
            prospect = form.save()
            prospect.prospect = True
            prospect.ativo = True
            prospect.save()
            return redirect('prospect_list')
        else:
            print ('Os erros: ' + str(form.errors.as_data()))
    else:
        form = ProspectForm()
    return render(request, template_name, {'form':form})


@login_required
def prospect_update(request, pk, template_name='prospect/cria_prospect_form.html'):
    paciente = get_object_or_404(Paciente, pk=pk)
    form = ProspectForm(request.POST or None, instance=paciente)
    if form.is_valid():
        form.save()
        return redirect('prospect_list')
    return render(request, template_name, {'form':form})





"""

     VIEWS PARA PACIENTE

"""

@login_required
def paciente_list(request, template_name='paciente/lista_paciente.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    pacientes = Paciente.objects.all();

    data = {}
    data['pacientes'] = pacientes
    
    return render(request, template_name, data)


@login_required
def paciente_delete(request, pk):
    paciente = get_object_or_404(Paciente, pk=pk)
    data = dict()
    if request.method == 'POST':
        paciente.delete()
        data['form_is_valid'] = True
        pacientes = Paciente.objects.all()
        data['html_paciente_list'] = render_to_string('paciente/lista_paciente_parcial.html', {
            'pacientes': pacientes
        })
    else:
        context = {'paciente': paciente}
        data['html_form'] = render_to_string('paciente/paciente_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def paciente_create(request, template_name='paciente/cria_paciente_form.html'):
    if request.method == 'POST':
        form = PacienteForm(request.POST)
        if form.is_valid():
            paciente = form.save()
            paciente.prospect = False
            paciente.save()
            return redirect('paciente_list')
    else:
        form = PacienteForm()
    return render(request, template_name, {'form':form})


@login_required
def paciente_update(request, pk, template_name='paciente/cria_paciente_form.html'):
    paciente = get_object_or_404(Paciente, pk=pk)
    form = PacienteForm(request.POST or None, instance=paciente)
    
    
    if form.is_valid():
        paciente = form.save()
        paciente.prospect = False
        paciente.save()
        return redirect('paciente_list')
    return render(request, template_name, {'form':form})


@login_required
def paciente_menu(request, pk, template_name='paciente/paciente_informacoes.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    data = {}
    data['paciente'] = paciente
    
    return render(request, template_name, data)
    

@login_required
def paciente_list_card(request, template_name='paciente/lista_paciente_card.html'):
    

    # Inicio da paginacao
    #paciente_list = Paciente.objects.filter(prospect=False)
    
    paciente_list = Paciente.objects.all()
    
    pacientes_filter = PacienteFilter(request.GET, queryset=paciente_list)

    paginator = Paginator(pacientes_filter.qs, 12) # Show 25 contacts per page
    
    page = request.GET.get('page',1)

    try:
        pacientes = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pacientes = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pacientes = paginator.page(paginator.num_pages)
    
    return render(request, template_name, {'pacientes': pacientes, 'filter' : pacientes_filter})


@login_required
def paciente_geral(request, pk, tab=None, subtab=None , formulario=None, formulario_fisica=None, formulario_clinica=None, formulario_postural=None ,template_name='paciente/paciente_geral.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    
    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente);
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente);
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente);
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente);
        
        
    # Formularios
    if formulario == None:
        form = PreReavaliacaoForm(initial={'paciente': paciente})
    else:
        form = formulario
    
    if formulario_fisica == None:
        form_fisica = AvaliacaoFisicaForm(initial={'paciente': paciente})
    else:
        form_fisica = formulario_fisica
    
    if formulario_clinica == None:
        form_clinica = AvaliacaoClinicaForm(initial={'paciente': paciente, 'usuario' : request.user })
    else:
       form_clinica = formulario_clinica


    if formulario_postural == None:
        form_postural = AvaliacaoPosturalForm(initial={'paciente': paciente})
    else:
       form_postural = formulario_postural
       
    # Navegacao Default
    if tab == None: 
        tab = 'info'
    
    if subtab == None:
        subtab = 'lista'
    
    
    
    context = { 
        'paciente' : paciente , 
        'form_fisica' : form_fisica, 
        'form_clinica' : form_clinica, 
        'form_postural' : form_postural, 
        'form' : form , 
        'prereavaliacoes' : prereavaliacoes, 
        'avaliacoes_fisica' : avaliacoes_fisica, 
        'avaliacoes_clinica' : avaliacoes_clinica , 
        'avaliacoes_postural' : avaliacoes_postural,
        'tab' : tab,
        'subtab' : subtab,
    }
    
    return render(request, template_name, context)
    

@login_required
def paciente_dashboard(request, pk, template_name='paciente_dashboard/paciente_dashboard.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    
    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente);
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente);
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente);
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente);
        
     
    
    context = { 
        'paciente' : paciente,
        'prereavaliacoes' : prereavaliacoes, 
        'avaliacoes_fisica' : avaliacoes_fisica, 
        'avaliacoes_clinica' : avaliacoes_clinica , 
        'avaliacoes_postural' : avaliacoes_postural,
    }
    
    return render(request, template_name, context)
    
"""

     VIEWS PARA HOSPITAL

"""

@login_required
def hospital_list(request, template_name='hospital/lista_hospital.html'):

    """ Lista das solucoes ja cadastradas ao problema """
    hospitais = Hospital.objects.all();

    data = {}
    data['hospitais'] = hospitais
    
    return render(request, template_name, data)


@login_required
def hospital_delete(request, pk):
    hospital = get_object_or_404(Hospital, pk=pk)
    data = dict()
    if request.method == 'POST':
        hospital.delete()
        data['form_is_valid'] = True
        hospitais = Hospital.objects.all()
        data['html_hospital_list'] = render_to_string('hospital/lista_hospital_parcial.html', {
            'hospitais': hospitais
        })
    else:
        context = {'hospital': hospital}
        data['html_form'] = render_to_string('hospital/hospital_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def save_hospital_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            hospitais = Hospital.objects.all()
            data['html_hospital_list'] = render_to_string('hospital/lista_hospital_parcial.html', {
                'hospitais': hospitais
            })
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def hospital_create(request):
    if request.method == 'POST':
        form = HospitalForm(request.POST)
    else:
        form = HospitalForm()
    return save_hospital_form(request, form, 'hospital/cria_hospital_modal.html')


@login_required
def hospital_update(request, pk):
    hospital = get_object_or_404(Hospital, pk=pk)
    if request.method == 'POST':
        form = HospitalForm(request.POST, instance=hospital)
    else:
        form = HospitalForm(instance=hospital)
    return save_hospital_form(request, form, 'hospital/atualiza_hospital_modal.html')



"""
        VIEWS PARA PRE REAVALIACAO
"""

@login_required
def prereavaliacao_list(request, pk, template_name='prereavaliacao/lista_prereavaliacao.html'):

    paciente = get_object_or_404(Paciente, pk=pk)
    
    if(paciente == None):
        return redirect('paciente_list_card')
    
    """ Lista das solucoes ja cadastradas ao problema """
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente);

    data = {}
    data['prereavaliacoes'] = prereavaliacoes
    data['paciente'] = paciente
    
    return render(request, template_name, data)


@login_required
def prereavaliacao_delete(request, pk):
    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)
    data = dict()
    print('Delete pre')
    if request.method == 'POST':
        prereavaliacao.delete()
        data['form_is_valid'] = True
        prereavaliacoes = PreReavaliacao.objects.all()
        #return paciente_geral(request, pk, 'prereavaliacao', 'paciente/paciente_geral.html')
        data['html_prereavaliacao_list'] = render_to_string('prereavaliacao/lista_prereavaliacao_parcial.html', {
            'prereavaliacoes': prereavaliacoes
        })
    else:
        print('Delete pre 2')
        context = {'prereavaliacao': prereavaliacao}
        data['html_form'] = render_to_string('prereavaliacao/prereavaliacao_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def prereavaliacao_create(request, pk, template_name='paciente/paciente_geral.html'):
    
    paciente = get_object_or_404(Paciente, pk=pk)
    
    if request.method == 'POST':

        form = PreReavaliacaoForm(request.POST)
        
        if form.is_valid():
            preavaliacao = form.save(commit=False)
            preavaliacao.usuario = request.user
            preavaliacao.paciente = paciente
            preavaliacao.save()
            return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk': pk , 'tab' : 'prereavaliacao'}) )
    
    return paciente_geral(request, paciente.id, 'prereavaliacao', 'lista', form, None, None, None, template_name)


@login_required
def prereavaliacao_update(request, pk, template_name='paciente/paciente_geral.html'):
    
    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)
    
    formulario = PreReavaliacaoForm(request.POST or None, instance=prereavaliacao)
    
    if formulario.is_valid():
        formulario.save()
        return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk':  prereavaliacao.paciente.id , 'tab' : 'prereavaliacao'}) )
        #return paciente_geral(request, prereavaliacao.paciente.id, 'prereavaliacao', 'lista-prereavaliacao', None, None, None, template_name)
    
    return paciente_geral(request, prereavaliacao.paciente.id, 'prereavaliacao', 'formulario-prereavaliacao', formulario, None, None, None, template_name)


@login_required
def prereavaliacao_visualizar(request, pk, template_name='prereavaliacao/visualiza_prereavaliacao.html'):
    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)
    form_fisica = PreReavaliacaoForm(instance=prereavaliacao)
    data = dict()
    context = {'form_fisica': form_fisica}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)




"""
  VIEWS PARA AVALIACAO CLINICA
"""

@login_required
def avaliacao_clinica_list(request, pk, template_name='avaliacaoclinica/lista_avaliacao_clinica.html'):

    paciente = get_object_or_404(Paciente, pk=pk)
    
    if(paciente == None):
        return redirect('paciente_list_card')
    
    """ Lista das solucoes ja cadastradas ao problema """
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente);

    data = {}
    data['avaliacoes_clinica'] = avaliacoes_clinica
    data['paciente'] = paciente
    
    return render(request, template_name, data)


@login_required
def avaliacao_clinica_delete(request, pk):
    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    data = dict()
    if request.method == 'POST':
        avaliacao_clinica.delete()
        data['form_is_valid'] = True
        avaliacoes_clinica = AvaliacaoClinica.objects.all()
        data['html_avaliacao_clinica_list'] = render_to_string('avaliacaoclinica/lista_avaliacao_clinica_parcial.html', {
            'avaliacoes_clinica': avaliacoes_clinica
        })
    else:
        context = {'avaliacao_clinica': avaliacao_clinica}
        data['html_form'] = render_to_string('avaliacaoclinica/avaliacao_clinica_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def avaliacao_clinica_create(request, pk, template_name='paciente/paciente_geral.html'):
  
    paciente = get_object_or_404(Paciente, pk=pk)
    
    if request.method == 'POST':
        form_clinica = AvaliacaoClinicaForm(request.POST)
        if form_clinica.is_valid():
            
            avaliacao_clinica = form_clinica.save(commit=False)
            data_inicial = avaliacao_clinica.data
            form_clinica.save()
            
            inserir_notificacao(paciente, avaliacao_clinica.data, 'AVC')
            inserir_notificacao(paciente, avaliacao_clinica.data_entrega, 'EXA')

                
            return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk': pk , 'tab' : 'clinica'}) )
            
        else:
            print ('Os erros: ' + str(form_clinica.errors.as_data()))
        
    return paciente_geral(request, paciente.id, 'clinica', 'formulario-avaliacaoclinica', None, None, form_clinica, None , template_name)
    

@login_required
def avaliacao_clinica_update(request, pk, template_name='paciente/paciente_geral.html'):
    
    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    
    form_clinica = AvaliacaoClinicaForm(request.POST or None, instance=avaliacao_clinica)
    

    if form_clinica.is_valid():
        avaliacao_clinica = form_clinica.save(commit=False)
        data_inicial = avaliacao_clinica.data
        form_clinica.save()
        
            
        return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk': avaliacao_clinica.paciente.id , 'tab' : 'clinica'}) )
    else:
        print ('Os erros clinica update: ' + str(form_clinica.errors.as_data()))
        #return paciente_geral(request, avaliacao_clinica.paciente.id, 'clinica', 'lista-avaliacaoclinica', None, None, None, template_name)
        
    return paciente_geral(request, avaliacao_clinica.paciente.id, 'clinica', 'formulario-avaliacaoclinica', None, None, form_clinica, None, template_name)


@login_required
def avaliacao_clinica_visualizar(request, pk, template_name='avaliacaoclinica/visualiza_avaliacao_clinica.html'):
    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
    data = dict()
    context = {'form_clinica': form_clinica}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)





"""
  VIEWS PARA AVALIACAO POSTURAL
"""

@login_required
def avaliacao_postural_list(request, pk, template_name='avaliacaopostural/lista_avaliacao_postural.html'):

    paciente = get_object_or_404(Paciente, pk=pk)
    
    if(paciente == None):
        return redirect('paciente_list_card')
    
    """ Lista das solucoes ja cadastradas ao problema """
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente);

    data = {}
    data['avaliacoes_postural'] = avaliacoes_postural
    data['paciente'] = paciente
    
    return render(request, template_name, data)


@login_required
def avaliacao_postural_delete(request, pk):
    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)
    data = dict()
    if request.method == 'POST':
        avaliacao_postural.delete()
        data['form_is_valid'] = True
        avaliacoes_postural = AvaliacaoPostural.objects.all()
        data['html_avaliacao_postural_list'] = render_to_string('avaliacaopostural/lista_avaliacao_postural_parcial.html', {
            'avaliacoes_postural': avaliacoes_postural
        })
    else:
        context = {'avaliacao_postural': avaliacao_postural}
        data['html_form'] = render_to_string('avaliacaopostural/avaliacao_postural_confirma_delete.html', context, request=request)
    return JsonResponse(data)


@login_required
def avaliacao_postural_create(request, pk, template_name='paciente/paciente_geral.html'):
    
    print('Ava Clinica')
    paciente = get_object_or_404(Paciente, pk=pk)
    
    print('Paciente: ' + str(paciente))
    
    """ Lista das solucoes ja cadastradas ao problema """
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente);

    if request.method == 'POST':
        form_postural = AvaliacaoPosturalForm(request.POST)
        if form_postural.is_valid():
            avaliacao_postural = form_postural.save(commit=False)
            avaliacao_postural.usuario = request.user
            avaliacao_postural.paciente = paciente
            avaliacao_postural.save()
            return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk': pk , 'tab' : 'postural'}) )
        else:
            print ('Os erros: ' + str(form_postural.errors.as_data()))
    return paciente_geral(request, paciente.id, 'postural', 'formulario-avaliacaopostural', None, None, None, form_postural, template_name)
    

@login_required
def avaliacao_postural_update(request, pk, template_name='paciente/paciente_geral.html'):
    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)
    form = AvaliacaoPosturalForm(request.POST or None, instance=avaliacao_postural)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('paciente_geral',kwargs={'pk': avaliacao_postural.paciente.id , 'tab' : 'postural'}) )
        #return paciente_geral(request, avaliacao_postural.paciente.id, 'postural', 'lista-avaliacaopostural', None, None, None, template_name)
        
    return paciente_geral(request, avaliacao_postural.paciente.id, 'postural', 'formulario-avaliacaopostural', None, None, None, form, template_name)


@login_required
def avaliacao_postural_visualizar(request, pk, template_name='avaliacaopostural/visualiza_avaliacao_postural.html'):
    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)
    form_postural = AvaliacaoPosturalForm(instance=avaliacao_postural)
    data = dict()
    context = {'form_postural': form_postural}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)





"""
    VIEWS PARA PARAMETROS GERENCIAIS
"""

@login_required
def parametros(request, template_name='parametros/cria_parametros_form.html'):
    
    parametros = ParametrosGerencial.objects.all()
    
    if(len(parametros) > 0):
        parametro = ParametrosGerencial.objects.filter().first()
        form = ParametrosGerencialForm(request.POST or None, instance=parametro)
    else:
        form = ParametrosGerencialForm(request.POST or None)
    
    if form.is_valid():
        form.save()
        return redirect('parametros')
    return render(request, template_name, {'form':form})





"""
 NOTIFICACOES
"""

def inserir_notificacao(paciente, data_modelo, modelo):
    
    
    dias_aviso = None
    
    if(data_modelo != None):
    
        if(modelo == 'AVF'):
            try:
                NotificacaoAvaliacao.objects.filter(paciente=paciente,descricao_modelo='AVF').delete()
            except:
                pass
        else:
            if(modelo == 'AVC'):
                try:
                    NotificacaoAvaliacao.objects.filter(paciente=paciente,descricao_modelo='AVC').delete()
                except:
                    pass
            else:
                if(modelo == 'EXA'):
                    try:
                        NotificacaoAvaliacao.objects.filter(paciente=paciente,descricao_modelo='EXA').delete()
                    except:
                        pass
                else:
                    if(modelo == 'CON'):
                        try:
                            NotificacaoAvaliacao.objects.filter(paciente=paciente,descricao_modelo='CON').delete()
                        except:
                            pass
        
        notificacao = NotificacaoAvaliacao(data_agendamento=data_modelo,descricao_modelo=modelo,paciente=paciente)
        notificacao.save()