from django.shortcuts import render

# Create your views here.
from django.conf.urls import url, include
from django.contrib import admin
from cadastro import views


urlpatterns = [
    #url(r'^$', views.home, name='home'),
    url(r'^$', views.home, name='paciente_list_card'),
    url(r'^dashboard$', views.dashboard, name='dashboard'),

    # URLs Convenio
    url(r'^convenio$', views.convenio_list, name='convenio_list'),
    url(r'^convenio/create/$', views.convenio_create, name='convenio_create'),
    url(r'^convenio/update/(?P<pk>\d+)$', views.convenio_update, name='convenio_update'),
    url(r'^convenio/delete/(?P<pk>\d+)$', views.convenio_delete, name='convenio_delete'),

    # URLs EMPRESAS
    url(r'^empresa$', views.empresa_list, name='empresa_list'),
    url(r'^empresa/create/$', views.empresa_create, name='empresa_create'),
    url(r'^empresa/update/(?P<pk>\d+)$', views.empresa_update, name='empresa_update'),
    url(r'^empresa/delete/(?P<pk>\d+)$', views.empresa_delete, name='empresa_delete'),

    # URLs Medico
    url(r'^medico$', views.medico_list, name='medico_list'),
    url(r'^medico/create/$', views.medico_create, name='medico_create'),
    url(r'^medico/update/(?P<pk>\d+)$', views.medico_update, name='medico_update'),
    url(r'^medico/delete/(?P<pk>\d+)$', views.medico_delete, name='medico_delete'),

    
    # URLs Professor
    url(r'^professor$', views.professor_list, name='professor_list'),
    url(r'^professor/create/$', views.professor_create, name='professor_create'),
    url(r'^professor/update/(?P<pk>\d+)$', views.professor_update, name='professor_update'),
    url(r'^professor/delete/(?P<pk>\d+)$', views.professor_delete, name='professor_delete'),
    
    
    # URLs Plano
    url(r'^plano$', views.plano_list, name='plano_list'),
    url(r'^plano/create/$', views.plano_create, name='plano_create'),
    url(r'^plano/update/(?P<pk>\d+)$', views.plano_update, name='plano_update'),
    url(r'^plano/delete/(?P<pk>\d+)$', views.plano_delete, name='plano_delete'),

    # URLs PROSPECTS
    url(r'^prospect$', views.prospect_list, name='prospect_list'),
    url(r'^prospect/create/$', views.prospect_create, name='prospect_create'),
    url(r'^prospect/update/(?P<pk>\d+)$', views.prospect_update, name='prospect_update'),
    url(r'^prospect/delete/(?P<pk>\d+)$', views.prospect_delete, name='prospect_delete'),
    
    # URLs Paciente
    url(r'^paciente$', views.paciente_list, name='paciente_list'),
    url(r'^paciente/create/$', views.paciente_create, name='paciente_create'),
    url(r'^paciente/update/(?P<pk>\d+)$', views.paciente_update, name='paciente_update'),
    url(r'^paciente/delete/(?P<pk>\d+)$', views.paciente_delete, name='paciente_delete'),
    url(r'^paciente/menu/(?P<pk>\d+)$', views.paciente_menu, name='paciente_menu'),
    url(r'^paciente/geral/(?P<tab>\w+)/(?P<pk>\d+)$', views.paciente_geral, name='paciente_geral'),
    url(r'^paciente/dashboard/(?P<pk>\d+)$', views.paciente_dashboard, name='paciente_dashboard'),
    url(r'^paciente/servicos/$', views.paciente_list_servicos, name='paciente_list_servicos'),
    
    # URLs Hospital
    url(r'^hospital$', views.hospital_list, name='hospital_list'),
    url(r'^hospital/create/$', views.hospital_create, name='hospital_create'),
    url(r'^hospital/update/(?P<pk>\d+)$', views.hospital_update, name='hospital_update'),
    url(r'^hospital/delete/(?P<pk>\d+)$', views.hospital_delete, name='hospital_delete'),

     # URLs Avaliacao Parametros
    url(r'^parametros$', views.parametros, name='parametros'),
]
