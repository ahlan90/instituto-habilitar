from django.apps import AppConfig


class CadastroConfig(AppConfig):
    name = 'instituto_habilitar.cadastro'
