from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from django import forms
from django.forms import ModelForm
from .models import *
from cadastro.models import Plano
from django_select2.forms import Select2MultipleWidget, Select2Widget, ModelSelect2MultipleWidget
from django.contrib.auth.models import User

class TesteCaminhada6Form(ModelForm):

    class Meta:
        model = TesteCaminhada6
        exclude = ['paciente']
        widgets = {
            'data': forms.TextInput(attrs={'type': 'date', 'id': 'data_caminhada6'})
        }

    def __init__(self, *args, **kwargs):
        super(TesteCaminhada6Form, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control textinput textInput'
        self.fields['pa_repouso'].widget.attrs['class'] = 'form-control textinput textInput pressao-arterial'
        self.fields['pa_recuperacao'].widget.attrs['class'] = 'form-control textinput textInput pressao-arterial'
        self.fields['pa_pos_esforco'].widget.attrs['class'] = 'form-control textinput textInput pressao-arterial'
        self.fields['voltas_1'].widget.attrs['class'] = 'form-control textinput textInput volta'
        self.fields['voltas_2'].widget.attrs['class'] = 'form-control textinput textInput volta'
        self.fields['voltas_3'].widget.attrs['class'] = 'form-control textinput textInput volta'
        self.fields['voltas_4'].widget.attrs['class'] = 'form-control textinput textInput volta'
        self.fields['voltas_5'].widget.attrs['class'] = 'form-control textinput textInput volta'
        self.fields['voltas_6'].widget.attrs['class'] = 'form-control textinput textInput volta'




class AvaliacaoPosturalForm(ModelForm):
    class Meta:
        model = AvaliacaoPostural
        fields = '__all__'
        exclude = ['paciente']
        widgets = {
            'data': forms.TextInput(attrs={'type': 'date', 'id': 'data_postural'})
        }

    def __init__(self, *args, **kwargs):
        super(AvaliacaoPosturalForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'filled-in chk-col-orange form-control textinput textInput'


class PreReavaliacaoForm(ModelForm):
    class Meta:
        model = PreReavaliacao
        fields = '__all__'
        exclude = ['paciente', 'data_proxima_avaliacao']
        widgets = {
            'plano': Select2Widget(attrs={'class': 'form-control textinput textInput'}),
            'parecer_professor': forms.Textarea(attrs={'class': 'form-control form-control textinput textInput'}),
            'data_criacao': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),
        }


class AvaliacaoClinicaForm(ModelForm):

    usuario = forms.ModelChoiceField(queryset=User.objects.all(), widget=Select2Widget)

    data = forms.DateField(localize=False, widget=forms.DateInput(
                format=('%Y-%m-%d'),
                attrs={'type': 'date', 'class': 'form-control textinput textInput', 'id': 'data_clinica'}))
    class Meta:
        model = AvaliacaoClinica
        fields = '__all__'
        widgets = {

            'risco_cardiovascular': Select2Widget,
            'plano': Select2MultipleWidget,
            'data': forms.DateInput(
                format=('%Y-%m-%d'),
                attrs={'type': 'date', 'class': 'form-control textinput textInput', 'id': 'data_clinica'}),
            'historia_clinica': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),
            'fatores_risco_dac': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),
            'co_morbidades': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),
            'medicacoes': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),
            'funcao_ventricular_esquerda': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pratica_exercicio_fisico': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),

            'paciente': Select2Widget(attrs={'class': 'form-control textinput textInput'}),
            'acv': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'abdomen': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'carotidas': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'ar': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'locomotor_neurologico': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'osteo_muscular': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'oftalmologico': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'geral': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pressao_arterial': forms.TextInput(attrs={'class': 'form-control textinput textInput pressao-arterial'}),
            'frequencia_cardiaca': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'saturacao_oxigenio': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'triglicerideos': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'colesterol': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),

            'coracao': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'cirurgia_cardiaca': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'caterizacao_cardiaca': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'angioplastia_coronariana': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'marcapasso_cardiaco': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'disturbio_ritmo': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'doenca_valvular_cardiaca': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'insuficiencia_cardiaca': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'transplante_coracao': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'cardiaca_congenita': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),
            'exercicio_regular': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-red'}),

            'ataque_cardiaco': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'luxacoes': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'artrite_pernas_bracos': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'desmaios': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'asma': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'anormalidades_radiograficas_torax': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'febre_reumatica': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'arteriosclerose': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'bronquite': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'epilepsia': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'anemia': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'sopro_cardiaco': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'problema_nervoso_emocional': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'veias_varicosas': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'tonteira': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'acidente_cerebral': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'tireoide': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),
            'pneumonia': forms.CheckboxInput(attrs={'class': 'filled-in chk-col-orange'}),

            # 'conferencia_programa' : forms.CheckboxInput(attrs={'class' : 'checkboxinput form-check-input'}),
            'frequencia': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),

            'texto': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),

            'data_entrega': forms.TextInput(attrs={'type': 'date', 'class': 'form-control textinput textInput'}),

            'texto_prescricao': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),

            'autonomia': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'treinamento_realizado': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'sintomas': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'queixas': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'limitacoes': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'parecer_professor': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),

            # FORMULARIO EXAMES COMPLEMENTARES
            'eco': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'cate': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'ecg': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'te_tcp': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'bioquimico': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'outros': forms.Textarea(attrs={'class': 'form-control textinput textInput'}),

            # FORMULARIO TC6
            # 'conferencia_tc6' : forms.CheckboxInput(attrs={'class' : 'checkboxinput form-check-input'}),
            'distancia_percorrida': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'escala_borg': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pa_inicial': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pa_recuperacao': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pa_final': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'fc_max': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'sat_02': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),

            # FORMULARIO TE
            # 'conferencia_te' : forms.CheckboxInput(attrs={'class' : 'checkboxinput form-check-input'}),
            'sintomas': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'fc_max_prevista': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'fc_max_atingida': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'vo2_max': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'la': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pcr': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'vo2_fc': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'pa': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),
            'evidencia_isquemia': forms.TextInput(attrs={'class': 'form-control textinput textInput'}),

            'pressao_parcial_co2': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'pressao_parcial_o2': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'pressao_arterial_media': forms.TextInput(attrs={'class': 'form-control textinput textInput pressao-arterial'}),
            'ph_capilar': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'debito_cardiaco': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'viscosidade_sanguinea': forms.NumberInput(attrs={ 'step': '0.01', 'max': '1000'}),
            'hematocrito': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'pressao_o2': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'pressao_co2': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'volume_sistolico': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'hemoglobina_glicada': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),

            'pressao_arterial_cnoga': forms.TextInput(attrs={'class': 'form-control textinput textInput pressao-arterial'}),
            'saturacao_o2': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),
            'frequencia_cardiaca_cnoga': forms.NumberInput(attrs={'class': 'form-control textinput textInput', 'step': '0.01', 'max': '1000'}),

        }

    def __init__(self, *args, **kwargs):
        super(AvaliacaoClinicaForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field(AppendedText('viscosidade_sanguinea', '$', active=True))
        )


VALIDACAO_CLINICA=[
    (0, 'Nenhum'),
    (1, 'Máximo'),
    (2, 'Submáximo')
]

class TesteCardiopulmonarForm(ModelForm):

    tc_maximo_submaximo = forms.ChoiceField(label="Máximo/Submáximo", choices=VALIDACAO_CLINICA)

    class Meta:
        model = TesteCardioPulmonar
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TesteCardiopulmonarForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field(AppendedText('tc_fc_max', 'xxx', active=True))
        )


class TesteErgometricoForm(ModelForm):
    te_maximo_submaximo = forms.ChoiceField(label="Máximo/Submáximo", choices=VALIDACAO_CLINICA)

    class Meta:
        model = TesteErgometrico
        fields = '__all__'


class ExamesBioquimicosForm(ModelForm):
    
    class Meta:
        model = ExamesBioquimicos
        fields = '__all__'


class AvaliacaoNutricionalForm(ModelForm):
    
    class Meta:
        model = AvaliacaoNutricional
        fields = '__all__'
        widgets = {
            'data_nutricional': forms.TextInput(attrs={'type': 'date', 'id': 'id_data_nutricional'}),
            'paciente': forms.HiddenInput(),
        }
