from django.conf.urls import url
from . import views

urlpatterns = [
    # URLs Teste caminhada
    url(r'^teste_caminhada6/(?P<pk>\d+)$', views.teste_caminhada6_list, name='teste_caminhada6_list'),
    url(r'^teste_caminhada6/criar/(?P<pk>\d+)$', views.teste_caminhada6_create, name='teste_caminhada6_create'),
    url(r'^teste_caminhada6/atualizar/(?P<pk>\d+)$', views.teste_caminhada6_update, name='teste_caminhada6_update'),
    url(r'^teste_caminhada6/deletar/(?P<pk>\d+)$', views.teste_caminhada6_delete, name='teste_caminhada6_delete'),
    url(r'^teste_caminhada6/visualizar/(?P<pk>\d+)$', views.teste_caminhada6_visualizar,
        name='teste_caminhada6_visualizar'),

    # URLs Avaliacao Postural
    url(r'^avaliacaopostural/(?P<pk>\d+)$', views.avaliacao_postural_list, name='avaliacao_postural_list'),
    url(r'^avaliacaopostural/criar/(?P<pk>\d+)$', views.avaliacao_postural_create, name='avaliacao_postural_create'),
    url(r'^avaliacaopostural/atualizar/(?P<pk>\d+)$', views.avaliacao_postural_update,
        name='avaliacao_postural_update'),
    url(r'^avaliacaopostural/deletar/(?P<pk>\d+)$', views.avaliacao_postural_delete, name='avaliacao_postural_delete'),
    url(r'^avaliacaopostural/visualizar/(?P<pk>\d+)$', views.avaliacao_postural_visualizar,
        name='avaliacao_postural_visualizar'),

    # URLs PreAvaliacao
    url(r'^prereavaliacao/(?P<pk>\d+)$', views.prereavaliacao_list, name='prereavaliacao_list'),
    url(r'^prereavaliacao/criar/(?P<pk>\d+)$', views.prereavaliacao_create, name='prereavaliacao_create'),
    url(r'^prereavaliacao/atualizar/(?P<pk>\d+)$', views.prereavaliacao_update, name='prereavaliacao_update'),
    url(r'^prereavaliacao/deletar/(?P<pk>\d+)$', views.prereavaliacao_delete, name='prereavaliacao_delete'),
    url(r'^prereavaliacao/visualizar/(?P<pk>\d+)$', views.prereavaliacao_visualizar, name='prereavaliacao_visualizar'),

    # URLs Avaliacao Clinica
    url(r'^avaliacaoclinica/(?P<pk>\d+)$', views.avaliacao_clinica_list, name='avaliacao_clinica_list'),
    url(r'^avaliacaoclinica/criar/(?P<pk>\d+)$', views.avaliacao_clinica_create, name='avaliacao_clinica_create'),
    url(r'^avaliacaoclinica/atualizar/(?P<pk>\d+)$', views.avaliacao_clinica_update, name='avaliacao_clinica_update'),
    url(r'^avaliacaoclinica/deletar/(?P<pk>\d+)$', views.avaliacao_clinica_delete, name='avaliacao_clinica_delete'),
    url(r'^avaliacaoclinica/visualizar/(?P<pk>\d+)$', views.avaliacao_clinica_visualizar,
        name='avaliacao_clinica_visualizar'),
    url(r'^avaliacaoclinica/imprimir/(?P<pk>\d+)$', views.avaliacao_clinica_print, name='avaliacao_clinica_print'),
    
    
        # URLs Avaliacao Clinica
    url(r'^avaliacaonutricional/(?P<pk>\d+)$', views.avaliacao_nutricional_list, name='avaliacao_nutricional_list'),
    url(r'^avaliacaonutricional/criar/(?P<pk>\d+)$', views.avaliacao_nutricional_create, name='avaliacao_nutricional_create'),
    url(r'^avaliacaonutricional/atualizar/(?P<pk>\d+)$', views.avaliacao_nutricional_update, name='avaliacao_nutricional_update'),
    url(r'^avaliacaonutricional/deletar/(?P<pk>\d+)$', views.avaliacao_nutricional_delete, name='avaliacao_nutricional_delete'),
    # url(r'^avaliacaonutricional/visualizar/(?P<pk>\d+)$', views.avaliacao_nutricional_visualizar,
    #     name='avaliacao_nutricional_visualizar'),
    # url(r'^avaliacaonutricional/imprimir/(?P<pk>\d+)$', views.avaliacao_nutricional_print, name='avaliacao_nutricional_print'),
]
