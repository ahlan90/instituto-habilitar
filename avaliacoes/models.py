from decimal import Decimal
from enum import Enum

from django.conf import settings
from django.db import models
from reportlab.graphics.renderPM import save

from cadastro.models import Paciente, Plano
from django.core.validators import MaxValueValidator, MinValueValidator
import datetime
from ckeditor.fields import RichTextField


class Avaliacoes(Enum):

    AVF = "Avaliação Física"
    AVC = "Avaliação Clínica"
    AVN = "Avaliação Nutricional"
    PRA = "Pre Reavaliação"

class TesteCaminhada6(models.Model):
    paciente = models.ForeignKey(Paciente)
    data = models.DateTimeField()

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL)

    pa_repouso = models.CharField(max_length=200, blank=True, null=True)
    fc_repouso = models.CharField(max_length=200, blank=True, null=True)
    sp02_repouso = models.CharField(max_length=200, blank=True, null=True)
    borg_repouso = models.CharField(max_length=200, blank=True, null=True)

    pa_pos_esforco = models.CharField(max_length=200, blank=True, null=True)
    fc_pos_esforco = models.CharField(max_length=200, blank=True, null=True)
    sp02_pos_esforco = models.CharField(max_length=200, blank=True, null=True)
    borg_pos_esforco = models.CharField(max_length=200, blank=True, null=True)

    pa_recuperacao = models.CharField(max_length=200, blank=True, null=True)
    fc_recuperacao = models.CharField(max_length=200, blank=True, null=True)
    sp02_recuperacao = models.CharField(max_length=200, blank=True, null=True)
    borg_recuperacao = models.CharField(max_length=200, blank=True, null=True)

    fc_1 = models.CharField(max_length=200, blank=True, null=True)
    sp02_1 = models.CharField(max_length=200, blank=True, null=True)
    borg_1 = models.CharField(max_length=200, blank=True, null=True)
    voltas_1 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_1 = models.CharField(max_length=250, blank=True, null=True)

    fc_2 = models.CharField(max_length=200, blank=True, null=True)
    sp02_2 = models.CharField(max_length=200, blank=True, null=True)
    borg_2 = models.CharField(max_length=200, blank=True, null=True)
    voltas_2 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_2 = models.CharField(max_length=250, blank=True, null=True)

    fc_3 = models.CharField(max_length=200, blank=True, null=True)
    sp02_3 = models.CharField(max_length=200, blank=True, null=True)
    borg_3 = models.CharField(max_length=200, blank=True, null=True)
    voltas_3 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_3 = models.CharField(max_length=250, blank=True, null=True)

    fc_4 = models.CharField(max_length=200, blank=True, null=True)
    sp02_4 = models.CharField(max_length=200, blank=True, null=True)
    borg_4 = models.CharField(max_length=200, blank=True, null=True)
    voltas_4 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_4 = models.CharField(max_length=250, blank=True, null=True)

    fc_5 = models.CharField(max_length=200, blank=True, null=True)
    sp02_5 = models.CharField(max_length=200, blank=True, null=True)
    borg_5 = models.CharField(max_length=200, blank=True, null=True)
    voltas_5 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_5 = models.CharField(max_length=250, blank=True, null=True)

    fc_6 = models.CharField(max_length=200, blank=True, null=True)
    sp02_6 = models.CharField(max_length=200, blank=True, null=True)
    borg_6 = models.CharField(max_length=200, blank=True, null=True)
    voltas_6 = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    sinais_sintomas_6 = models.CharField(max_length=250, blank=True, null=True)

    total_voltas = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, default=Decimal('0.00'))
    distancia_percorrida = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True,
                                               default=Decimal('0.00'))


class AvaliacaoPostural(models.Model):
    paciente = models.ForeignKey(Paciente)
    data = models.DateTimeField()

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL)

    rot_lat_cerv_direita = models.BooleanField()
    rot_lat_cerv_esquerda = models.BooleanField()
    inc_lat_cerv_direita = models.BooleanField()
    inc_lat_cerv_esquerda = models.BooleanField()
    elev_ombro_direito = models.BooleanField()
    elev_ombro_esquerdo = models.BooleanField()
    rot_med_punho_esquerdo = models.BooleanField()
    rot_med_punho_direito = models.BooleanField()
    desalinhamento_mamilos = models.BooleanField()
    desequilibrio_quadril = models.BooleanField()
    assimetria_membro_inferior = models.BooleanField()
    joelho_varo = models.BooleanField()
    joelho_valgo = models.BooleanField()
    rot_int_joelho = models.BooleanField()
    rot_ext_joelho = models.BooleanField()
    pe_eversao = models.BooleanField()
    pe_inversao = models.BooleanField()
    elev_quadril_esquerdo = models.BooleanField()
    elev_quadril_direito = models.BooleanField()
    pe_aduto = models.BooleanField()
    pe_abduto = models.BooleanField()
    desalinhamento = models.BooleanField()
    pe_equino = models.BooleanField()
    aparentemente_saudavel_ant = models.BooleanField()

    le_hiperlordose_cervical = models.BooleanField()
    le_hipercifose_toracica = models.BooleanField()
    le_hiperlordose_lombar = models.BooleanField()
    le_costa_plana = models.BooleanField()
    le_geno_recurvato = models.BooleanField()
    le_geno_flexo = models.BooleanField()
    le_atituto_cifotica_torax = models.BooleanField()
    le_lordose_lombar = models.BooleanField()
    le_aparentemente_saudavel_lat = models.BooleanField()
    le_ombros_protusos = models.BooleanField()
    le_rot_int_ombros = models.BooleanField()

    ld_hiperlordose_cervical = models.BooleanField()
    ld_hipercifose_toracica = models.BooleanField()
    ld_hiperlordose_lombar = models.BooleanField()
    ld_costa_plana = models.BooleanField()
    ld_geno_recurvato = models.BooleanField()
    ld_geno_flexo = models.BooleanField()
    ld_atituto_cifotica_torax = models.BooleanField()
    ld_lordose_lombar = models.BooleanField()
    ld_aparentemente_saudavel_lat = models.BooleanField()
    ld_ombros_protusos = models.BooleanField()
    ld_rot_int_ombros = models.BooleanField()

    escoliose_cervical = models.BooleanField()
    escoliose_toracica = models.BooleanField()
    escoliose_lombar = models.BooleanField()
    desalinhamento_escapula = models.BooleanField()
    escapulas_abduzidas = models.BooleanField()
    escapulas_aduzidas = models.BooleanField()
    desalinhamento_dobra_axilar = models.BooleanField()
    desalinhamento_quadril = models.BooleanField()
    dobra_poplitea = models.BooleanField()
    rot_med_punhos = models.BooleanField()
    aparentemente_saudavel_post = models.BooleanField()

    fa_escoliose_cervical = models.BooleanField()
    fa_escoliose_toracica = models.BooleanField()
    fa_escoliose_lombar = models.BooleanField()
    fa_desalinhamento_escapula = models.BooleanField()
    fa_aparentemente_saudavel_post = models.BooleanField()


class PreReavaliacao(models.Model):

    paciente = models.ForeignKey(Paciente)
    data_criacao = models.DateTimeField("Data de Criação")

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL)

    plano = models.ForeignKey(Plano)
    treinamento_realizado = models.CharField(max_length=200, null=True, blank=True)
    sintomas = models.CharField(max_length=200, null=True, blank=True)
    queixas = models.CharField(max_length=200, null=True, blank=True)
    limitacoes = models.CharField("Limitações", max_length=200, null=True, blank=True)
    intercorrencias = models.CharField("Intercorrências", max_length=200, null=True, blank=True)
    parecer_professor = models.CharField(max_length=1000, null=True)

    data_proxima_avaliacao = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.treinamento_realizado


class TesteErgometrico(models.Model):

    te_maximo_submaximo = models.IntegerField("Máximo/Submáximo", null=True, blank=True)
    te_fc_max = models.CharField( "FC Máx", max_length=20,null=True, blank=True)
    te_sintomas = models.CharField("Sintomas", max_length=200, null=True, blank=True)
    te_pa = models.CharField("Pressão Arterial", max_length=20, null=True, blank=True)
    te_isquemia = models.CharField("Isquemia", max_length=200, null=True, blank=True)
    te_mets = models.CharField("METS", max_length=20, null=True, blank=True)
    te_arritmia = models.CharField("Arritmia", max_length=200, null=True, blank=True)


class TesteCardioPulmonar(models.Model):

    tc_maximo_submaximo = models.IntegerField("Máximo/Submáximo", null=True, blank=True)
    tc_fc_max = models.IntegerField("FC Máx", null=True, blank=True)
    tc_sintomas = models.CharField("Sintomas", max_length=200, null=True, blank=True)
    tc_pa_repouso = models.CharField("PA Repouso", max_length=20, null=True, blank=True)
    tc_pa_max = models.CharField("PA Máx", max_length=20, null=True, blank=True)
    tc_isquemia = models.CharField("Isquemia", max_length=200, null=True, blank=True)
    tc_arritmia = models.CharField("Arritmia", max_length=200, null=True, blank=True)
    tc_vo2_max = models.CharField( "VO2 Máx", max_length=20,null=True, blank=True)
    tc_rer = models.CharField("RER", max_length=20, null=True, blank=True)
    tc_vo2_fc = models.CharField("FC V02 Máx", max_length=20, null=True, blank=True)
    tc_vo2_vco2 = models.CharField("VO2/VCO2", max_length=20, null=True, blank=True)
    tc_limiar_anaerobio = models.CharField("Limiar Anaeróbico (LA)", max_length=20, null=True, blank=True)
    tc_pcr = models.CharField("Ponto de Compensação Respiratória (PCR)", max_length=20, null=True, blank=True)
    tc_reserva_ventilatoria = models.CharField("Reserva Ventilatória", max_length=20, null=True, blank=True)
    tc_observacao = models.CharField("Observação", max_length=200, null=True, blank=True)


class ExamesBioquimicos(models.Model):

    eb_glicose = models.CharField("Glicose", max_length=200, blank=True, null=True)
    hba1c = models.CharField(max_length=200, blank=True, null=True)
    hb_hc = models.CharField("Hb/Hc", max_length=200, blank=True, null=True)
    eb_creatinina = models.CharField("Creatinina", max_length=200, blank=True, null=True)
    k_mais = models.CharField("K+", max_length=200, blank=True, null=True)
    colesterol_total = models.CharField(max_length=200, blank=True, null=True)
    colesterol_hdl = models.CharField("Colesterol HDL", max_length=200, blank=True, null=True)
    colesterol_ldl = models.CharField("Colesterol LDL", max_length=200, blank=True, null=True)
    eb_triglicerideos = models.CharField("Triglicerideos", max_length=200, blank=True, null=True)
    eb_insulina = models.CharField("Insulina", max_length=200, blank=True, null=True)
    tsh = models.CharField("TSH", max_length=200, blank=True, null=True)
    vitamina_d = models.CharField("Vitamina D", max_length=200, blank=True, null=True)
    testosterona = models.CharField(max_length=200, blank=True, null=True)
    vitamina_b12 = models.CharField("Vitamina B12", max_length=200, blank=True, null=True)
    eb_acido_folico = models.CharField("Ácido Fólico", max_length=200, blank=True, null=True)


class AvaliacaoClinica(models.Model):
    paciente = models.ForeignKey(Paciente)
    data = models.DateTimeField(null=True, blank=True)

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)

    historia_clinica = models.CharField(max_length=2000, blank=True, null=True)
    pratica_exercicio_fisico = models.CharField("Prática de Exercícios Físicos", max_length=200, blank=True, null=True)
    fatores_risco_dac = models.CharField("Fatores de Risco de DAC", max_length=1000, blank=True, null=True)
    co_morbidades = models.CharField("Comorbidades", max_length=1000, blank=True, null=True)
    medicacoes = models.CharField("Medicações", max_length=1000, blank=True, null=True)
    funcao_ventricular_esquerda = models.CharField("Função Ventricular Esquerda", max_length=200, blank=True, null=True)

    acv = models.CharField("Acidente Cardio Vascular", max_length=200, blank=True, null=True)
    carotidas = models.CharField(max_length=200, blank=True, null=True)
    ar = models.CharField(max_length=200, blank=True, null=True)
    locomotor_neurologico = models.CharField(max_length=200, blank=True, null=True)
    oftalmologico = models.CharField(max_length=200, blank=True, null=True)
    geral = models.CharField(max_length=200, blank=True, null=True)
    pressao_arterial = models.CharField("Pressão Arterial", max_length=200, blank=True, null=True)
    frequencia_cardiaca = models.IntegerField(blank=True, null=True)
    saturacao_oxigenio = models.IntegerField("Saturação de Oxigênio", blank=True, null=True)
    triglicerideos = models.IntegerField(blank=True, null=True)
    colesterol = models.IntegerField(blank=True, null=True)
    abdomen = models.CharField("Abdômen", max_length=200, blank=True, null=True)
    osteo_muscular = models.CharField(max_length=200, blank=True, null=True)
    cintilografia = models.CharField(max_length=200, blank=True, null=True)

    PARAMETRO_RISCO_CARDIOVASCULAR = (
        ('BX', 'Baixo'),
        ('MD', 'Moderado'),
        ('AT', 'Alto'),
    )

    risco_cardiovascular = models.CharField(max_length=2, choices=PARAMETRO_RISCO_CARDIOVASCULAR, blank=True, null=True)

    coracao = models.BooleanField("Ataque Cardíaco", default=False)
    cirurgia_cardiaca = models.BooleanField("Cirurgia Cardíaca", default=False)
    caterizacao_cardiaca = models.BooleanField("Cateterização Cardíaca", default=False)
    angioplastia_coronariana = models.BooleanField(default=False)
    marcapasso_cardiaco = models.BooleanField("Marcapasso Cardíaco", default=False)
    disturbio_ritmo = models.BooleanField(default=False)
    doenca_valvular_cardiaca = models.BooleanField("Doença Valvular Cardíaca", default=False)
    insuficiencia_cardiaca = models.BooleanField("Insuficiência Cardíaca", default=False)
    transplante_coracao = models.BooleanField("Transplante de Coração", default=False)
    cardiaca_congenita = models.BooleanField("Doença Cardíaca Congênita", default=False)
    exercicio_regular = models.BooleanField("Exercício Regular", default=False)

    ataque_cardiaco = models.BooleanField("Ataque Cardíaco", default=False)
    luxacoes = models.BooleanField("Luxações", default=False)
    artrite_pernas_bracos = models.BooleanField("Artrite Pernas e Braços", default=False)
    desmaios = models.BooleanField(default=False)
    asma = models.BooleanField(default=False)
    anormalidades_radiograficas_torax = models.BooleanField("Anormalidades Radiograficas no Toráx", default=False)
    febre_reumatica = models.BooleanField("Febre Reumática", default=False)
    arteriosclerose = models.BooleanField(default=False)
    bronquite = models.BooleanField(default=False)
    epilepsia = models.BooleanField(default=False)
    anemia = models.BooleanField(default=False)
    problema_nervoso_emocional = models.BooleanField(default=False)
    sopro_cardiaco = models.BooleanField("Sobro Cardíaco", default=False)
    veias_varicosas = models.BooleanField(default=False)
    tonteira = models.BooleanField(default=False)
    acidente_cerebral = models.BooleanField(default=False)
    tireoide = models.BooleanField(default=False)
    pneumonia = models.BooleanField(default=False)

    plano = models.ManyToManyField(Plano, blank=True)
    # frequencia = models.IntegerField(default=1, validators=[MaxValueValidator(100), MinValueValidator(1)], blank=True,null=True)

    texto_prescricao = models.CharField(max_length=1000, blank=True, null=True)

    texto = models.CharField("Exames", max_length=1000, blank=True, null=True)
    data_entrega = models.DateField(blank=True, null=True, )

    eco = models.CharField(max_length=200, blank=True, null=True)
    ecg = models.CharField(max_length=200, blank=True, null=True)
    te_tcp = models.CharField(max_length=200, blank=True, null=True)
    cate = models.CharField(max_length=200, blank=True, null=True)
    bioquimico = models.CharField(max_length=200, blank=True, null=True)
    outros = models.CharField(max_length=2000, blank=True, null=True)

    distancia_percorrida = models.CharField("Distância Percorrida", max_length=200, blank=True, null=True)
    escala_borg = models.CharField("Escala de BORG", max_length=200, blank=True, null=True)
    pa_inicial = models.CharField("P.A. inicial", max_length=200, blank=True, null=True)
    pa_recuperacao = models.CharField("P.A. recuperação", max_length=200, blank=True, null=True)
    pa_final = models.CharField("P.A. final", max_length=200, blank=True, null=True)
    fc_max = models.CharField("Frequência Cardíaca Máxima", max_length=200, blank=True, null=True)
    sat_02 = models.CharField("Saturação de Oxigênio", max_length=200, blank=True, null=True)

    #Antigo TCP/TE
    sintomas = models.CharField(max_length=200, blank=True)
    fc_max_prevista = models.CharField(max_length=200, blank=True, null=True)
    fc_max_atingida = models.CharField(max_length=200, blank=True, null=True)
    vo2_max = models.CharField(max_length=200, blank=True, null=True)
    la = models.CharField(max_length=200, blank=True, null=True)
    pcr = models.CharField(max_length=200, blank=True, null=True)
    vo2_fc = models.CharField(max_length=200, blank=True, null=True)
    pa = models.CharField(max_length=200, blank=True, null=True)
    evidencia_isquemia = models.CharField(max_length=200, blank=True, null=True)


    pressao_parcial_co2 = models.CharField("Pressão parcial de CO2 (PC02)", max_length=20, null=True, blank=True)
    pressao_parcial_o2 = models.CharField("Pressão parcial de O2 (PO2)", max_length=20, null=True, blank=True)
    pressao_arterial_media = models.CharField("Pressão arterial média (MAP)", max_length=200, null=True, blank=True)
    ph_capilar = models.CharField("PH de capilar (PH)", max_length=20, null=True, blank=True)
    debito_cardiaco = models.CharField("Débito cardíaco (CO)", max_length=20, null=True, blank=True)
    viscosidade_sanguinea = models.CharField("Viscosidade sanguínea", max_length=20, null=True, blank=True)
    hematocrito = models.CharField("Hematócrito", max_length=20, null=True, blank=True)
    pressao_o2 = models.CharField("Pressão O2", max_length=20, null=True, blank=True)
    pressao_co2 = models.CharField("Pressão de (CO2)", max_length=20, null=True, blank=True)
    volume_sistolico = models.CharField("Volume sistólico", max_length=20, blank=True, null=True)
    hemoglobina_glicada = models.CharField("Hemoglobina glicada (Hb)", max_length=20, null=True, blank=True)

    # Parei aqui
    pressao_arterial_cnoga = models.CharField("Pressão Arterial (PA)", max_length=20, blank=True, null=True)
    saturacao_o2 = models.CharField("Saturação de Oxigênio(SPO2)", max_length=20, null=True, blank=True)
    frequencia_cardiaca_cnoga = models.CharField("Frequência cardíaca", max_length=20, blank=True, null=True)

    teste_ergometrico = models.OneToOneField(TesteErgometrico, models.CASCADE, null=True, blank=True)
    teste_cardiopulmonar = models.OneToOneField(TesteCardioPulmonar, models.CASCADE, null=True, blank=True)
    bioquimicos = models.OneToOneField(ExamesBioquimicos, models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.historia_clinica


class AvaliacaoNutricional(models.Model):
    
    paciente = models.ForeignKey(Paciente)

    data_nutricional = models.DateTimeField("Data da Avaliação", null=True, blank=True)

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Avaliador", null=True, blank=True)
    
    dieta_praticada = RichTextField("Dieta praticada pelo Aluno", blank=True, null=True)
    dieta_prescrita = RichTextField("Dieta prescrita", blank=True, null=True)
    suplementacao_prescrita = RichTextField("Suplementação prescrita", blank=True, null=True)
    prescricao_alimentar = RichTextField("Prescrição alimentar para o exercício", blank=True, null=True)

