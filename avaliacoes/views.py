from copy import deepcopy

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.forms import model_to_dict
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from easy_pdf.rendering import render_to_pdf_response

from avaliacao_fisica.models import *
from django.http import HttpResponseRedirect
from avaliacoes.models import *
from avaliacoes.forms import *
from cadastro.models import ParametrosGerencial
from notificacoes.models import NotificacaoAvaliacao
from django.utils import formats



"""
  VIEWS PARA TESTE CAMINHADA 6 MINUTOS
"""
@login_required
def teste_caminhada6_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    parametros = ParametrosGerencial.objects.first()

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_caminhada6 = TesteCaminhada6Form(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_caminhada6': form_caminhada6,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'caminhada',
        'subtab': 'lista-testecaminhada6',
    }

    return render(request, 'teste_caminhada6/cria_teste_caminhada.html', context)


@login_required
def teste_caminhada6_delete(request, pk):
    teste_caminhada6 = get_object_or_404(TesteCaminhada6, pk=pk)

    paciente = teste_caminhada6.paciente

    data = dict()

    if request.method == 'POST':
        teste_caminhada6.delete()
        data['form_is_valid'] = True
        testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
        data['html_teste_caminhada6_list'] = render_to_string('teste_caminhada6/lista_teste_caminhada_parcial.html', {
            'testes_caminhada': testes_caminhada
        })
    else:
        context = {'teste_caminhada': teste_caminhada6}
        data['html_form'] = render_to_string('teste_caminhada6/teste_caminhada_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def teste_caminhada6_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    parametros = ParametrosGerencial.objects.first()

    if request.method == 'POST':
        form_caminhada6 = TesteCaminhada6Form(request.POST)
        if form_caminhada6.is_valid():
            teste_caminhada6 = form_caminhada6.save(commit=False)
            teste_caminhada6.paciente = paciente
            teste_caminhada6.total_voltas = teste_caminhada6.voltas_1 + teste_caminhada6.voltas_2 + teste_caminhada6.voltas_3 + teste_caminhada6.voltas_4 + teste_caminhada6.voltas_5 + teste_caminhada6.voltas_6
            teste_caminhada6.distancia_percorrida = teste_caminhada6.total_voltas * parametros.tamanho_volta_teste_caminhada
            teste_caminhada6.save()
            return teste_caminhada6_list(request, paciente.id)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_caminhada6 = TesteCaminhada6Form(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_caminhada6': form_caminhada6,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'testes_caminhada': testes_caminhada,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'tab': 'caminhada',
        'subtab': 'formulario-testecaminhada6',
    }

    return render(request, 'teste_caminhada6/cria_teste_caminhada.html', context)


@login_required
def teste_caminhada6_update(request, pk, template_name='paciente/paciente_geral.html'):

    teste_caminhada = get_object_or_404(TesteCaminhada6, pk=pk)
    paciente = teste_caminhada.paciente

    parametros = ParametrosGerencial.objects.first()

    if request.method == 'POST':
        form_caminhada6 = TesteCaminhada6Form(request.POST, instance=teste_caminhada)
        if form_caminhada6.is_valid():
            teste_caminhada6 = form_caminhada6.save(commit=False)
            teste_caminhada6.paciente = paciente
            teste_caminhada6.total_voltas = teste_caminhada6.voltas_1 + teste_caminhada6.voltas_2 + teste_caminhada6.voltas_3 + teste_caminhada6.voltas_4 + teste_caminhada6.voltas_5 + teste_caminhada6.voltas_6
            teste_caminhada6.distancia_percorrida = teste_caminhada6.total_voltas * parametros.tamanho_volta_teste_caminhada
            teste_caminhada6.save()
            return teste_caminhada6_list(request, paciente.id)
    else:
        form_caminhada6 = TesteCaminhada6Form(instance=teste_caminhada)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)


    context = {
        'paciente': paciente,
        'form_caminhada6': form_caminhada6,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'testes_caminhada': testes_caminhada,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'tab': 'caminhada',
        'subtab': 'formulario-testecaminhada6',
    }

    return render(request, 'teste_caminhada6/cria_teste_caminhada.html', context)


@login_required
def teste_caminhada6_visualizar(request, pk, template_name='teste_caminhada6/visualiza_teste_caminhada.html'):
    teste_caminhada6 = get_object_or_404(TesteCaminhada6, pk=pk)
    form_teste_caminhada = TesteCaminhada6Form(instance=teste_caminhada6)
    data = dict()
    context = {'form_caminhada6_vis': form_teste_caminhada}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



"""
  VIEWS PARA AVALIACAO POSTURAL
"""
@login_required
def avaliacao_postural_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_postural = AvaliacaoPosturalForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_postural': form_postural,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'postural',
        'subtab': 'lista-avaliacaopostural',
    }

    return render(request, 'avaliacaopostural/cria_avaliacao_postural.html', context)


@login_required
def avaliacao_postural_delete(request, pk):
    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)

    paciente = avaliacao_postural.paciente

    data = dict()
    if request.method == 'POST':
        avaliacao_postural.delete()
        data['form_is_valid'] = True
        avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
        data['html_avaliacao_postural_list'] = render_to_string(
            'avaliacaopostural/lista_avaliacao_postural_parcial.html', {
                'avaliacoes_postural': avaliacoes_postural
            })
    else:
        context = {'avaliacao_postural': avaliacao_postural}
        data['html_form'] = render_to_string('avaliacaopostural/avaliacao_postural_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def avaliacao_postural_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    if request.method == 'POST':
        form_postural = AvaliacaoPosturalForm(request.POST)
        if form_postural.is_valid():
            avaliacao_postural = form_postural.save(commit=False)
            avaliacao_postural.paciente = paciente
            avaliacao_postural.save()
            return avaliacao_postural_list(request, pk)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form_postural = AvaliacaoPosturalForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_postural': form_postural,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'postural',
        'subtab': 'formulario-avaliacaopostural',
    }

    return render(request, 'avaliacaopostural/cria_avaliacao_postural.html', context)


@login_required
def avaliacao_postural_update(request, pk, template_name='paciente/paciente_geral.html'):

    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)

    paciente = avaliacao_postural.paciente

    if request.method == 'POST':
        form_postural = AvaliacaoPosturalForm(request.POST, instance=avaliacao_postural)
        if form_postural.is_valid():
            avaliacao_postural = form_postural.save(commit=False)
            avaliacao_postural.paciente = paciente
            avaliacao_postural.save()
            return avaliacao_postural_list(request, paciente.id)
    else:
        form_postural = AvaliacaoPosturalForm(instance=avaliacao_postural)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'form_postural': form_postural,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'postural',
        'subtab': 'formulario-avaliacaopostural',
    }

    return render(request, 'avaliacaopostural/cria_avaliacao_postural.html', context)


@login_required
def avaliacao_postural_visualizar(request, pk, template_name='avaliacaopostural/visualiza_avaliacao_postural.html'):
    avaliacao_postural = get_object_or_404(AvaliacaoPostural, pk=pk)
    form_postural = AvaliacaoPosturalForm(instance=avaliacao_postural)
    data = dict()
    context = {'form_postural': form_postural}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



"""
        VIEWS PARA PRE REAVALIACAO
"""
@login_required
def prereavaliacao_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form = PreReavaliacaoForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form': form,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'prereavaliacao',
        'subtab': 'lista-prereavaliacao',
    }

    return render(request, 'prereavaliacao/cria_prereavaliacao.html', context)


@login_required
def prereavaliacao_delete(request, pk):
    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)

    paciente = prereavaliacao.paciente

    data = dict()

    if request.method == 'POST':
        prereavaliacao.delete()
        data['form_is_valid'] = True
        prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
        # return paciente_geral(request, pk, 'prereavaliacao', 'paciente/paciente_geral.html')
        data['html_prereavaliacao_list'] = render_to_string('prereavaliacao/lista_prereavaliacao_parcial.html', {
            'prereavaliacoes': prereavaliacoes
        })
    else:
        context = {'prereavaliacao': prereavaliacao}
        data['html_form'] = render_to_string('prereavaliacao/prereavaliacao_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)

@login_required
def prereavaliacao_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    if request.method == 'POST':
        form = PreReavaliacaoForm(request.POST)
        if form.is_valid():
            preavaliacao = form.save(commit=False)
            preavaliacao.paciente = paciente
            preavaliacao.save()
            return prereavaliacao_list(request, pk)

        # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    form = PreReavaliacaoForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form': form,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'prereavaliacao',
        'subtab': 'formulario-prereavaliacao',
    }

    return render(request, 'prereavaliacao/cria_prereavaliacao.html', context)

@login_required
def prereavaliacao_update(request, pk):

    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)

    paciente = prereavaliacao.paciente

    if request.method == 'POST':
        form = PreReavaliacaoForm(request.POST, instance=prereavaliacao)
        if form.is_valid():
            form.save()
            return prereavaliacao_create(request, paciente.id)
    else:
        form = PreReavaliacaoForm(instance=prereavaliacao)

    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'form': form,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'prereavaliacao',
        'subtab': 'formulario-prereavaliacao',
    }

    return render(request, 'prereavaliacao/cria_prereavaliacao.html', context)


@login_required
def prereavaliacao_visualizar(request, pk, template_name='prereavaliacao/visualiza_prereavaliacao.html'):
    prereavaliacao = get_object_or_404(PreReavaliacao, pk=pk)
    prereavaliacao.data_criacao = formats.date_format(prereavaliacao.data_criacao, "Y-m-d")
    form_fisica = PreReavaliacaoForm(instance=prereavaliacao)
    data = dict()
    context = {'form_fisica': form_fisica}

    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)



"""
  VIEWS PARA AVALIACAO CLINICA
"""
@login_required
def avaliacao_clinica_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'testes_caminhada': testes_caminhada,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'tab': 'clinica',
        'subtab': 'lista-avaliacaoclinica',
    }

    return render(request, 'avaliacaoclinica/lista_avaliacao_clinica.html', context)


@login_required
def avaliacao_clinica_delete(request, pk):
    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)

    paciente = avaliacao_clinica.paciente

    data = dict()

    if request.method == 'POST':
        avaliacao_clinica.delete()
        data['form_is_valid'] = True
        avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
        data['html_avaliacao_clinica_list'] = render_to_string('avaliacaoclinica/lista_avaliacao_clinica_parcial.html',
                                                               {
                                                                   'avaliacoes_clinica': avaliacoes_clinica
                                                               })
    else:
        context = {'avaliacao_clinica': avaliacao_clinica}
        data['html_form'] = render_to_string('avaliacaoclinica/avaliacao_clinica_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def avaliacao_clinica_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    if request.method == 'POST':

        form_clinica = AvaliacaoClinicaForm(request.POST)
        form_ergometrico = TesteErgometricoForm(request.POST)
        form_cardiopulmonar = TesteCardiopulmonarForm(request.POST)
        form_bioquimicos = ExamesBioquimicosForm(request.POST)

        if form_clinica.is_valid() and form_bioquimicos.is_valid() and form_ergometrico.is_valid() and form_cardiopulmonar.is_valid():

            teste_cardiopulmonar = form_cardiopulmonar.save()
            teste_ergometrico = form_ergometrico.save()
            bioquimicos = form_bioquimicos.save()

            avaliacao_clinica = form_clinica.save(commit=False)
            avaliacao_clinica.pk = None
            data_inicial = avaliacao_clinica.data

            avaliacao_clinica.teste_ergometrico = teste_ergometrico
            avaliacao_clinica.teste_cardiopulmonar = teste_cardiopulmonar
            avaliacao_clinica.bioquimicos = bioquimicos

            avaliacao_clinica.save()
            form_clinica.save_m2m()

            inserir_notificacao(paciente, avaliacao_clinica.data, 'AVC')
            inserir_notificacao(paciente, avaliacao_clinica.data_entrega, 'EXA')

            return avaliacao_clinica_list(request, pk)

    else:

        clinica = AvaliacaoClinica()
        clinica.paciente = paciente
        clinica.usuario = request.user
        clinica.data = datetime.datetime.now()

        ergometrico = TesteErgometrico()
        cardiopulmonar = TesteCardioPulmonar()
        bioquimicos = ExamesBioquimicos()

        clinica.save()
        ergometrico.save()
        cardiopulmonar.save()
        bioquimicos.save()

        form_clinica = AvaliacaoClinicaForm(instance=clinica)
        form_ergometrico = TesteErgometricoForm(instance=ergometrico)
        form_cardiopulmonar = TesteCardiopulmonarForm(instance=cardiopulmonar)
        form_bioquimicos = ExamesBioquimicosForm(instance=bioquimicos)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    # av_clinica_anterior = AvaliacaoClinica.objects.filter(paciente=paciente).last()
    #
    # if av_clinica_anterior:
    #     av_clinica = copia_dados_avaliacao_clinica(av_clinica_anterior)
    #     av_clinica.paciente = paciente
    #     av_clinica.usuario = request.user
    #     form_clinica = AvaliacaoClinicaForm(instance=av_clinica, initial={'plano': [i.id for i in av_clinica_anterior.plano.all()]})
    # else:
    #     form_clinica = AvaliacaoClinicaForm(initial={'paciente': paciente, 'usuario': request.user})
    #
    #
    # form_ergometrico = TesteErgometricoForm()
    # form_cardiopulmonar = TesteCardiopulmonarForm()
    # form_bioquimicos = ExamesBioquimicosForm()


    context = {
        'paciente': paciente,
        'form_clinica': form_clinica,
        'form_ergometrico': form_ergometrico,
        'form_cardiopulmonar': form_cardiopulmonar,
        'form_bioquimicos': form_bioquimicos,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'clinica',
        'subtab': 'formulario-avaliacaoclinica',
    }

    return render(request, 'avaliacaoclinica/cria_avaliacao_clinica.html', context)


@login_required
def avaliacao_clinica_update(request, pk, template_name='paciente/paciente_geral.html'):

    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    paciente = avaliacao_clinica.paciente

    if request.method == 'POST':

        form_clinica = AvaliacaoClinicaForm(request.POST, instance=avaliacao_clinica)
        form_ergometrico = TesteErgometricoForm(request.POST, instance=avaliacao_clinica.teste_ergometrico)
        form_cardiopulmonar = TesteCardiopulmonarForm(request.POST, instance=avaliacao_clinica.teste_cardiopulmonar)
        form_bioquimicos = ExamesBioquimicosForm(request.POST, instance=avaliacao_clinica.bioquimicos)

        if form_clinica.is_valid() and form_cardiopulmonar.is_valid() and form_ergometrico.is_valid() and form_bioquimicos.is_valid():

            teste_cardiopulmonar = form_cardiopulmonar.save()
            teste_ergometrico = form_ergometrico.save()
            avaliacao_clinica = form_clinica.save(commit=False)
            bioquimicos = form_bioquimicos.save()
            
            data_inicial = avaliacao_clinica.data

            avaliacao_clinica.teste_ergometrico = teste_ergometrico
            avaliacao_clinica.teste_cardiopulmonar = teste_cardiopulmonar
            avaliacao_clinica.bioquimicos = bioquimicos

            avaliacao_clinica.save()
            form_clinica.save_m2m()

            inserir_notificacao(paciente, avaliacao_clinica.data, 'AVC')
            inserir_notificacao(paciente, avaliacao_clinica.data_entrega, 'EXA')

            print('Salvou')
            #return avaliacao_clinica_list(request, paciente.id)
            return JsonResponse(({'message': 'Salvo com sucesso!'}))
        else:
            messages.add_message(request, messages.ERROR, 'Error ao salvar')
            print(form_clinica.errors)
    else:
        form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
        form_ergometrico = TesteErgometricoForm(instance=avaliacao_clinica.teste_ergometrico)
        form_cardiopulmonar = TesteCardiopulmonarForm(instance=avaliacao_clinica.teste_cardiopulmonar)
        form_bioquimicos = ExamesBioquimicosForm(instance=avaliacao_clinica.bioquimicos)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'form_clinica': form_clinica,
        'form_ergometrico': form_ergometrico,
        'form_cardiopulmonar': form_cardiopulmonar,
        'form_bioquimicos': form_bioquimicos,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_clinica': avaliacoes_clinica,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'clinica',
        'subtab': 'formulario-avaliacaoclinica',
        'update': 'True'
    }

    return render(request, 'avaliacaoclinica/cria_avaliacao_clinica.html', context)


@login_required
def avaliacao_clinica_visualizar(request, pk, template_name='avaliacaoclinica/visualiza_avaliacao_clinica.html'):
    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
    data = dict()
    context = {'form_clinica': form_clinica}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


"""
 NOTIFICACOES
"""
def inserir_notificacao(paciente, data_modelo, modelo):
    dias_aviso = None

    if (data_modelo != None):

        if (modelo == 'AVF'):
            try:
                NotificacaoAvaliacao.objects.filter(paciente=paciente, descricao_modelo='AVF').delete()
            except:
                pass
        else:
            if (modelo == 'AVC'):
                try:
                    NotificacaoAvaliacao.objects.filter(paciente=paciente, descricao_modelo='AVC').delete()
                except:
                    pass
            else:
                if (modelo == 'EXA'):
                    try:
                        NotificacaoAvaliacao.objects.filter(paciente=paciente, descricao_modelo='EXA').delete()
                    except:
                        pass
                else:
                    if (modelo == 'CON'):
                        try:
                            NotificacaoAvaliacao.objects.filter(paciente=paciente, descricao_modelo='CON').delete()
                        except:
                            pass

        notificacao = NotificacaoAvaliacao(data_agendamento=data_modelo, descricao_modelo=modelo, paciente=paciente)
        notificacao.save()


# @login_required
# def paciente_geral(request, pk, tab=None, subtab=None, formulario=None, formulario_fisica=None, formulario_clinica=None,
#                    formulario_postural=None, formulario_caminhada6=None, template_name='paciente/paciente_geral.html'):
#     paciente = get_object_or_404(Paciente, pk=pk)
#
#     # Puxando as listas
#     prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
#     avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
#     avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
#     avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
#     testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
#
#     # Formularios
#     if formulario == None:
#         form = PreReavaliacaoForm(initial={'paciente': paciente})
#     else:
#         form = formulario
#
#     if formulario_fisica == None:
#         form_fisica = AvaliacaoFisicaForm(initial={'paciente': paciente})
#     else:
#         form_fisica = formulario_fisica
#
#     if formulario_clinica == None:
#         form_clinica = AvaliacaoClinicaForm(initial={'paciente': paciente, 'usuario': request.user})
#     else:
#         form_clinica = formulario_clinica
#
#     if formulario_postural == None:
#         form_postural = AvaliacaoPosturalForm(initial={'paciente': paciente})
#     else:
#         form_postural = formulario_postural
#
#     if formulario_caminhada6 == None:
#         form_caminhada6 = TesteCaminhada6Form(initial={'paciente': paciente})
#     else:
#         form_caminhada6 = formulario_caminhada6
#
#     # Navegacao Default
#     if tab == None:
#         tab = 'info'
#
#     if subtab == None:
#         subtab = 'lista'
#
#     context = {
#         'paciente': paciente,
#         'form_fisica': form_fisica,
#         'form_clinica': form_clinica,
#         'form_postural': form_postural,
#         'form_caminhada6': form_caminhada6,
#         'form': form,
#         'prereavaliacoes': prereavaliacoes,
#         'avaliacoes_fisica': avaliacoes_fisica,
#         'avaliacoes_clinica': avaliacoes_clinica,
#         'avaliacoes_postural': avaliacoes_postural,
#         'testes_caminhada': testes_caminhada,
#         'tab': tab,
#         'subtab': subtab,
#     }
#
#     return render(request, template_name, context)


def copia_dados_avaliacao_clinica(av_clinica_anterior):

    retorno = AvaliacaoClinica()

    retorno.coracao = av_clinica_anterior.coracao
    retorno.cirurgia_cardiaca = av_clinica_anterior.cirurgia_cardiaca
    retorno.caterizacao_cardiaca = av_clinica_anterior.caterizacao_cardiaca
    retorno.angioplastia_coronariana = av_clinica_anterior.angioplastia_coronariana
    retorno.marcapasso_cardiaco = av_clinica_anterior.marcapasso_cardiaco
    retorno.disturbio_ritmo = av_clinica_anterior.disturbio_ritmo
    retorno.doenca_valvular_cardiaca = av_clinica_anterior.doenca_valvular_cardiaca
    retorno.insuficiencia_cardiaca = av_clinica_anterior.insuficiencia_cardiaca
    retorno.transplante_coracao = av_clinica_anterior.transplante_coracao
    retorno.cardiaca_congenita = av_clinica_anterior.cardiaca_congenita
    retorno.exercicio_regular = av_clinica_anterior.exercicio_regular

    retorno.ataque_cardiaco = av_clinica_anterior.ataque_cardiaco
    retorno.luxacoes = av_clinica_anterior.luxacoes
    retorno.artrite_pernas_bracos = av_clinica_anterior.artrite_pernas_bracos
    retorno.desmaios = av_clinica_anterior.desmaios
    retorno.asma = av_clinica_anterior.asma
    retorno.anormalidades_radiograficas_torax = av_clinica_anterior.anormalidades_radiograficas_torax
    retorno.febre_reumatica = av_clinica_anterior.febre_reumatica
    retorno.arteriosclerose = av_clinica_anterior.arteriosclerose
    retorno.bronquite = av_clinica_anterior.bronquite
    retorno.epilepsia = av_clinica_anterior.epilepsia
    retorno.anemia = av_clinica_anterior.anemia
    retorno.problema_nervoso_emocional = av_clinica_anterior.problema_nervoso_emocional
    retorno.sopro_cardiaco = av_clinica_anterior.sopro_cardiaco
    retorno.veias_varicosas = av_clinica_anterior.veias_varicosas
    retorno.tonteira = av_clinica_anterior.tonteira
    retorno.acidente_cerebral = av_clinica_anterior.acidente_cerebral
    retorno.tireoide = av_clinica_anterior.tireoide
    retorno.pneumonia = av_clinica_anterior.pneumonia

    retorno.historia_clinica = av_clinica_anterior.historia_clinica
    retorno.pratica_exercicio_fisico = av_clinica_anterior.pratica_exercicio_fisico
    retorno.fatores_risco_dac = av_clinica_anterior.fatores_risco_dac
    retorno.co_morbidades = av_clinica_anterior.co_morbidades
    retorno.medicacoes = av_clinica_anterior.medicacoes
    retorno.funcao_ventricular_esquerda = av_clinica_anterior.funcao_ventricular_esquerda

    return retorno






def avaliacao_clinica_print(request, pk):

    avaliacao_clinica = get_object_or_404(AvaliacaoClinica, pk=pk)
    paciente = avaliacao_clinica.paciente

    form_clinica = AvaliacaoClinicaForm(instance=avaliacao_clinica)
    form_ergometrico = TesteErgometricoForm(instance=avaliacao_clinica.teste_ergometrico)
    form_cardiopulmonar = TesteCardiopulmonarForm(instance=avaliacao_clinica.teste_cardiopulmonar)
    form_bioquimicos = ExamesBioquimicosForm(instance=avaliacao_clinica.bioquimicos)


    anamnse = False

    for field in form_clinica.visible_fields()[3:9]:
        if field.value() != None:
            anamnse = True
            break


    examesfisicos = []
    # Construindo a logica para tabela
    for field in form_clinica.visible_fields()[9:23]:
        if field.value() != None:
            examesfisicos.append(field)


    historico = []
    #Construindo a logica para tabela
    i = 1
    for field in form_clinica.visible_fields()[24:35]:
        if i == 3:
            i = 1
        if field.value() == True:
            row = [i, field.label]
            historico.append(row)
            i+=1


    antecedentes = []

    #Construindo a logica para tabela
    i = 1
    for field in form_clinica.visible_fields()[36:53]:
        if i == 3:
            i = 1
        if field.value() == True:
            row = [i, field.label]
            antecedentes.append(row)
            i+=1


    tc6 = []
    # Construindo a logica para tabela
    for field in form_clinica.visible_fields()[63:70]:
        if field.value() != None:
            tc6.append(field)

    biomedicos = []
    # Construindo a logica para tabela
    for field in form_clinica.visible_fields()[79:93]:
        if field.value() != None:
            biomedicos.append(field)

    ergometrico = False

    #Construindo a logica para tabela
    for field in form_ergometrico:
        if field.value() != None:
            ergometrico = True
            break


    cardiopulmonar = False

    #Construindo a logica para tabela
    for field in form_cardiopulmonar:
        if field.value() != None:
            cardiopulmonar = True
            break

    bioquimicos = False

    # Construindo a logica para tabela
    for field in form_bioquimicos:
        if field.value() != None:
            bioquimicos = True
            break

    data = {
        'paciente': paciente,
        'form_clinica': form_clinica,
        'form_ergometrico': form_ergometrico,
        'form_cardiopulmonar': form_cardiopulmonar,
        'form_bioquimicos': form_bioquimicos,
        'historico': historico,
        'antecedentes': antecedentes,
        'anamnse': anamnse,
        'ergometrico': ergometrico,
        'cardiopulmonar': cardiopulmonar,
        'bioquimicos': bioquimicos,
        'examesfisicos': examesfisicos,
        'tc6': tc6,
        'biomedicos': biomedicos
    }

    return render_to_pdf_response(request, 'print/avaliacao_clinica_print.html', data)






"""
  VIEWS PARA AVALIACAO NUTRICIONAL
"""
@login_required
def avaliacao_nutricional_list(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)
    
    form_nutricional = AvaliacaoNutricionalForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_nutricional': form_nutricional,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'avaliacoes_postural': avaliacoes_postural,
        'testes_caminhada': testes_caminhada,
        'avaliacoes_clinica': avaliacoes_clinica,
        'tab': 'nutricional',
        'subtab': 'lista-avaliacaonutricional',
    }

    return render(request, 'avaliacaonutricional/cria_avaliacao_nutricional.html', context)


@login_required
def avaliacao_nutricional_delete(request, pk):
    print('Entrou')
    avaliacao_nutricional = get_object_or_404(AvaliacaoNutricional, pk=pk)
    paciente = avaliacao_nutricional.paciente

    data = dict()

    if request.method == 'POST':
        avaliacao_nutricional.delete()
        data['form_is_valid'] = True
        avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)
        data['html_avaliacao_nutricional_list'] = render_to_string('avaliacaonutricional/lista_avaliacao_nutricional_parcial.html',
                                                              {
                                                                  'avaliacoes_nutricional': avaliacoes_nutricional
                                                              })
    else:
        context = {'avaliacao_nutricional': avaliacao_nutricional}
        data['html_form'] = render_to_string('avaliacaonutricional/avaliacao_nutricional_confirma_delete.html', context,
                                             request=request)
    return JsonResponse(data)


@login_required
def avaliacao_nutricional_create(request, pk):

    paciente = get_object_or_404(Paciente, pk=pk)

    if request.method == 'POST':

        form_nutricional = AvaliacaoNutricionalForm(request.POST)

        if form_nutricional.is_valid():
            
            form_nutricional.save()

            return avaliacao_nutricional_list(request, pk)
        else:
            print(form_nutricional.errors)
             
    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)

    form_nutricional = AvaliacaoNutricionalForm(initial={'paciente': paciente, 'usuario': request.user})

    context = {
        'paciente': paciente,
        'form_nutricional': form_nutricional,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'nutricional',
        'subtab': 'formulario-avaliacaonutricional',
    }

    return render(request, 'avaliacaonutricional/cria_avaliacao_nutricional.html', context)


@login_required
def avaliacao_nutricional_update(request, pk, template_name='paciente/paciente_geral.html'):

    avaliacao_nutricional = get_object_or_404(AvaliacaoNutricional, pk=pk)
    paciente = avaliacao_nutricional.paciente

    if request.method == 'POST':

        form_nutricional = AvaliacaoNutricionalForm(request.POST, instance=avaliacao_nutricional)

        if form_nutricional.is_valid():

            form_nutricional.save()

            return avaliacao_nutricional_list(request, paciente.id)
        else:
            print(form_nutricional.errors)
    else:
        form_nutricional = AvaliacaoNutricionalForm(instance=avaliacao_nutricional)

    # Puxando as listas
    prereavaliacoes = PreReavaliacao.objects.filter(paciente=paciente)
    avaliacoes_fisica = AvaliacaoFisica.objects.filter(paciente=paciente)
    avaliacoes_nutricional = AvaliacaoNutricional.objects.filter(paciente=paciente)
    avaliacoes_postural = AvaliacaoPostural.objects.filter(paciente=paciente)
    testes_caminhada = TesteCaminhada6.objects.filter(paciente=paciente)
    avaliacoes_clinica = AvaliacaoClinica.objects.filter(paciente=paciente)

    context = {
        'paciente': paciente,
        'form_nutricional': form_nutricional,
        'prereavaliacoes': prereavaliacoes,
        'avaliacoes_fisica': avaliacoes_fisica,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'avaliacoes_postural': avaliacoes_postural,
        'avaliacoes_nutricional': avaliacoes_nutricional,
        'testes_caminhada': testes_caminhada,
        'tab': 'nutricional',
        'subtab': 'formulario-avaliacaonutricional',
        'update': 'True'
    }

    return render(request, 'avaliacaonutricional/cria_avaliacao_nutricional.html', context)