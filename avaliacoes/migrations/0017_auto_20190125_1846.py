# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-01-25 20:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacoes', '0016_auto_20190125_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avaliacaoclinica',
            name='pressao_parcial_co2',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
