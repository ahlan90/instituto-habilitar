# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-01-04 17:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacoes', '0004_avaliacaoclinica'),
    ]

    operations = [
        migrations.CreateModel(
            name='TesteCardioPulmonar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('maximo_submaximo', models.IntegerField(blank=True, null=True)),
                ('fc_max', models.IntegerField(blank=True, null=True)),
                ('sintomas', models.IntegerField(blank=True, null=True)),
                ('pa', models.IntegerField(blank=True, null=True)),
                ('isquemia', models.IntegerField(blank=True, null=True)),
                ('arritmia', models.IntegerField(blank=True, null=True)),
                ('vo2_max', models.IntegerField(blank=True, null=True)),
                ('rer', models.IntegerField(blank=True, null=True)),
                ('vo2_fc', models.IntegerField(blank=True, null=True)),
                ('vo2_vco2', models.IntegerField(blank=True, null=True)),
                ('la', models.IntegerField(blank=True, null=True)),
                ('pcr', models.IntegerField(blank=True, null=True)),
                ('reserva_ventilatoria', models.IntegerField(blank=True, null=True)),
                ('observacao', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TesteErgometrico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('maximo_submaximo', models.IntegerField(blank=True, null=True)),
                ('sintomas', models.IntegerField(blank=True, null=True)),
                ('pa', models.IntegerField(blank=True, null=True)),
                ('isquemia', models.IntegerField(blank=True, null=True)),
                ('mets', models.IntegerField(blank=True, null=True)),
                ('arritmia', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='avaliacaoclinica',
            name='frequencia',
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='abdomen',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='bioquimico',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='cintilografia',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='ecg',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='funcao_ventricular_esquerda',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='osteo_muscular',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='pratica_exercicio_fisico',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='risco_cardiovascular',
            field=models.CharField(blank=True, choices=[('BX', 'Baixo'), ('MD', 'Moderado'), ('AT', 'Alto')], max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='te_tcp',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='teste_cardiopulmonar',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='avaliacoes.TesteCardioPulmonar'),
        ),
        migrations.AddField(
            model_name='avaliacaoclinica',
            name='teste_ergometrico',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='avaliacoes.TesteErgometrico'),
        ),
    ]
