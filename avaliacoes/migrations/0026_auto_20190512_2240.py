# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-05-13 01:40
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacoes', '0025_auto_20190511_1403'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='avaliacaoclinica',
            name='data_proxima_avaliacao',
        ),
        migrations.RemoveField(
            model_name='avaliacaonutricional',
            name='data_proxima_avaliacao',
        ),
    ]
