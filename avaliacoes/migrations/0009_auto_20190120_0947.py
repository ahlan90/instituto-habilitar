# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-01-20 11:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('avaliacoes', '0008_auto_20190119_1845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testecardiopulmonar',
            name='vo2_fc',
            field=models.IntegerField(blank=True, null=True, verbose_name='FC V02 Máx'),
        ),
        migrations.AlterField(
            model_name='testecardiopulmonar',
            name='vo2_max',
            field=models.IntegerField(blank=True, null=True, verbose_name='VO2 Máx'),
        ),
    ]
